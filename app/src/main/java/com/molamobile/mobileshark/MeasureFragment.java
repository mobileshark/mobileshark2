package com.molamobile.mobileshark;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.app.INotificationSideChannel;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityNr;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoNr;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthNr;
import android.telephony.CellSignalStrengthWcdma;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.molamobile.mobileshark.adapter.CellDisplayAdapter;
import com.molamobile.mobileshark.models.CellDisplayAdapterModel;
import com.molamobile.mobileshark.utils.CellUtility;
import com.molamobile.mobileshark.utils.CommonUtility;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

/*Created By Ajish Dharman on 28-July-2020
 *
 *
 */
public class MeasureFragment extends Fragment {

    Activity thisActvity;
    TelephonyManager tm;
    TelephonyManager tmsub1;
    TelephonyManager tmsub2 = null;
    BottomNavigationView bottomNavigation;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    boolean permissionGranted = false;



    boolean isOnRefresh = false;

    TextView txt_battery;
    TextView txt_wifi_connec;
    TextView txt_ping;
    TextView txt_operator;
    TextView txt_roaming;
    TextView txt_mcc_mnc;
    TextView txt_sim_data_nw;
    TextView txt_cell_name;

    TextView txt_sim_state;
    TextView txt_sim_data_connected;

    ConnectivityManager cm;
    HashMap<String, String[][]> cellMap;
    List<CellDisplayAdapterModel> lstDisplay;
    RecyclerView rcvServingCells;

    List<CellInfoGsm> gsmCells;
    List<CellInfoLte> lteCells;
    List<CellInfoWcdma> wCdmaCells;
    List<CellInfoNr> cellInfoNrs;

    private Runnable runnableCode;
    Handler handler;

    static int counter =0;
    boolean threadStart = false;

   // ScheduledThreadPoolExecutor threadExec;


    CellDisplayAdapter cellDisplayAdapter;
    Timer timer;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_measure_two, null);

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        initialize();
        if(permissionGranted) {
            if (Build.VERSION.SDK_INT >= 29) {

                getSimParamApi29();

            }
            else{
                getSIMParamOtherVersionApI();
            }
        }
    }

    private void initialize(){

        thisActvity = getActivity();
       // timer = new Timer();

       tm = (TelephonyManager) thisActvity.getSystemService(Context.TELEPHONY_SERVICE);
       cm = (ConnectivityManager) thisActvity.getSystemService(Context.CONNECTIVITY_SERVICE);

       if(checkAndRequestPermissionsForMesure(thisActvity)){

           permissionGranted = true;
       }
       else{
           permissionGranted = false;
       }


       txt_battery = (TextView) getView().findViewById(R.id.txt_battery);
       //txt_wifi_connec_stats = (TextView) findViewById(R.id.txt_wifi_connec_stats);
       txt_ping = (TextView) getView().findViewById(R.id.txt_ping);
       txt_operator = (TextView) getView().findViewById(R.id.txt_operator);
       txt_mcc_mnc = (TextView)getView(). findViewById(R.id.txt_mcc_mnc);
       //  txt_sim_state_val = (TextView) findViewById(R.id.txt_sim_state_val);
       txt_roaming = (TextView)getView(). findViewById(R.id.txt_roaming);
       txt_sim_data_nw = (TextView)getView(). findViewById(R.id.txt_sim_data_nw);
       //  txt_sim_data_connected_val = (TextView)findViewById(R.id.txt_sim_data_connected_val);
       txt_cell_name = (TextView) getView().findViewById(R.id.txt_cell_name);
       rcvServingCells = (RecyclerView)getView(). findViewById(R.id.rcvServingCell) ;
       txt_wifi_connec = (TextView)getView().findViewById(R.id.txt_wifi_connec) ;

       txt_sim_state = (TextView)getView().findViewById(R.id.txt_sim_state);
       txt_sim_data_connected = (TextView)getView().findViewById(R.id.txt_sim_data_connected);

      // int itemViewType = 0;
     // rcvServingCells.getRecycledViewPool().setMaxRecycledViews(itemViewType, 0);
        int itemViewType = 0;
        rcvServingCells.getRecycledViewPool().setMaxRecycledViews(itemViewType, 0);




       lstDisplay = new ArrayList<CellDisplayAdapterModel>();
       cellDisplayAdapter = new CellDisplayAdapter(lstDisplay,thisActvity);

       gsmCells = new ArrayList<CellInfoGsm>();
       lteCells = new ArrayList<CellInfoLte>();
       wCdmaCells = new ArrayList<CellInfoWcdma>();
       cellInfoNrs = new ArrayList<CellInfoNr>();

       //NeighBour
       RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(thisActvity.getApplicationContext());
       ((LinearLayoutManager) mLayoutManager2).setOrientation(RecyclerView.VERTICAL);
       rcvServingCells.setAdapter(cellDisplayAdapter);
       rcvServingCells.setLayoutManager(mLayoutManager2);

   }

    public  boolean checkAndRequestPermissionsForMesure(Activity thisCtx) {
        int phone = ContextCompat.checkSelfPermission(thisCtx, Manifest.permission.READ_PHONE_STATE);
        int storage = ContextCompat.checkSelfPermission(thisCtx, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int loc = ContextCompat.checkSelfPermission(thisCtx, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int loc2 = ContextCompat.checkSelfPermission(thisCtx, android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (phone != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (loc2 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (loc != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty())
        {
            //ActivityCompat.requestPermissions(thisCtx,listPermissionsNeeded.toArray
                   // (new String[listPermissionsNeeded.size()]), CommonUtility.REQUEST_ID_MULTIPLE_PERMISSIONS);

            requestPermissions(listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]),CommonUtility.REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;


        }
        return true;
    }


    private void setUpCells(){
        try {
            List<CellInfo> cellInfos = tm.getAllCellInfo();
           List<CellInfo> primaryCells = collectPrimaryServingCell(cellInfos);
            collectMeasuresForPrimaryServingCell(primaryCells);
            collectNeighbouringCells(cellInfos);
            groupMeasuresofNeighbouringCellsOrderd();
           // cellDisplayAdapter.addCellInfoList(lstDisplay);
           // cellDisplayAdapter.notifyDataSetChanged();


        }
        catch (SecurityException ex){

        }
    }




    // This function is called when user accept or decline the permission.
    // Request Code is used to check which permission called this function.
    // This request code is provided when user is prompt for permission.
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super
                .onRequestPermissionsResult(requestCode,
                        permissions,
                        grantResults);

        if (requestCode == REQUEST_ID_MULTIPLE_PERMISSIONS) {

            // Checking whether user granted the permission or not.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED) {

                permissionGranted = true;
                if (Build.VERSION.SDK_INT >= 29) {

                    getSimParamApi29();

                }
                else{
                    getSIMParamOtherVersionApI();
                }

            } else {

                permissionGranted = false;
            }
        }
    }//OnRequestPermissionResult


    @TargetApi(29)
    private void getSimParamApi29() throws SecurityException {

        if (permissionGranted) {

            SubscriptionManager subscriptionManager =
                    (SubscriptionManager) thisActvity.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);

            try {

                // hasdual = false;
                List<SubscriptionInfo> subscriptionInfoList = subscriptionManager.getActiveSubscriptionInfoList();

                if (subscriptionInfoList != null) {
                    if (subscriptionInfoList.size() > 0) {

                        int sz = subscriptionInfoList.size();
                        //subid1 = subscriptionInfoList.get(0).getSubscriptionId();
                        // tmsub1 = tm.createForSubscriptionId(subid1);
                        if (sz > 1) {
                            //Dual Sim get the Details for the First 1 - Later will take it from settings
                            // simSlot1.setText("SIM 1");
                            int subid1 = subscriptionInfoList.get(0).getSubscriptionId();
                            tmsub1 = tm.createForSubscriptionId(subid1);

                        } else {
                            int subid1 = subscriptionInfoList.get(0).getSubscriptionId();
                            tmsub1 = tm.createForSubscriptionId(subid1);

                        }
                        String simState = CellUtility.simState(tmsub1.getSimState()); //SimState

                        if (simState.equals("Ready")) {
                            txt_sim_state.setText("Sim State: "+"Ready");
                        } else {
                            txt_sim_state.setText("Sim State: "+simState);

                        }
                        String nw = CellUtility.getRatFromInt(tmsub1.getDataNetworkType());
                        txt_sim_data_nw.setText("Data N/W: "+nw);

                        String mccmnc = tmsub1.getNetworkOperator()+"".trim();
                     //   txt_mcc_mnc.append(" "+ mccmnc.substring(0,2)+ " "+mccmnc.substring(3,mccmnc.length()-1));
                        txt_mcc_mnc.setText("MCC MNC: "+ mccmnc.substring(0,3) +" "+mccmnc.substring(3,mccmnc.length()));

                       // txt_mcc_mnc.setText(txt_mcc_mnc.getText()+" "+tmsub1.getNetworkOperator());

                        String opname = tmsub1.getSimOperatorName();
                        if (opname.length() > 10) {
                            txt_operator.setText("Operator: "+opname.substring(0,10) + "...");
                        } else {
                            //  txt_operator_val.setText(opname);
                            txt_operator.setText("Operator: " +opname);

                        }
                        boolean roaming = tmsub1.isNetworkRoaming();
                        // txt_roaming_val.setText(roaming == true ? "Yes" : "No");
                        if(roaming) {
                            txt_roaming.setText("Roaming: "+"Yes");
                        }
                        else{
                            txt_roaming.setText("Roaming: "+"No");
                        }


                        getWifi_Battery_SimParams();
                        setUpCells();
                        if(cellDisplayAdapter.getItemCount()>0){

                            startRefreshThread();
                        }

                    }
                }

            } catch (Exception ex) {

                String s = ex.getMessage();
                String s3 = "9939393";
            }//catch
        }
    }

    private void getSIMParamOtherVersionApI() {

        try {

            int sz = 0;
            if (permissionGranted) {

                SubscriptionManager subscriptionManager =
                        (SubscriptionManager) thisActvity.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
                try {

                    int subid1 = 0;
                    int simslot=0;
                    List<SubscriptionInfo> subscriptionInfoList = subscriptionManager.getActiveSubscriptionInfoList();
                    if (subscriptionInfoList != null) {


                        sz = subscriptionInfoList.size();
                        subid1 = subscriptionInfoList.get(0).getSubscriptionId();


                        if (sz > 1) {

                            subid1 = subscriptionInfoList.get(0).getSubscriptionId();


                        } else if (sz == 1)

                            subid1 = subscriptionInfoList.get(0).getSubscriptionId();
                        simslot =       subscriptionInfoList.get(0).getSimSlotIndex();
                    } else if (sz == 0) {

                    }
                    String nwt;



                    String simState = CellUtility.simState(CellUtility.getSIMStateBySlot(thisActvity, "getSimState", simslot));
                    nwt = CellUtility.getRatFromInt(CellUtility.getNetWork(thisActvity, "getNetworkType", subid1));

                    int nwtOp = CellUtility.getNetworkOperator(thisActvity, "getNetworkOperator", subid1);
                    String netOpname = CellUtility.getNetworkOperatorName(thisActvity, "getNetworkOperatorName", subid1);
                    boolean roaming = CellUtility.isNetworkRoaming(thisActvity, "isNetworkRoaming", subid1);


                    if (netOpname.length() > 9) {
                        txt_operator.setText("Operator: "+netOpname.substring(0,10) + "...");
                    } else {
                        txt_operator.setText("Operator: "+netOpname);

                    }
                    //txt_sim_state_val.setText(simState);
                    //  txt_roaming_val.setText(roaming == true ? "Yes" : "No");
                    if(roaming) {
                        txt_roaming.setText("Roaming: "+"Yes");
                    }
                    else{
                        txt_roaming.setText("Roaming: "+"No");
                    }

                    if (simState.equals("Ready")) {
                        txt_sim_state.setText("Sim State: "+"Ready");
                    } else {
                        txt_sim_state.setText("Sim State: "+simState);

                    }

                    String mccmnc = nwtOp+"".trim();
                //    String mcc = mccmnc.substring(0,3);
                //    String mnc = mccmnc.substring(3,mccmnc.length());
                    txt_mcc_mnc.setText("MCC MNC: "+ mccmnc.substring(0,3) +" "+mccmnc.substring(3,mccmnc.length()));

                    txt_sim_data_nw.setText("Data N/W: "+nwt);

                    getWifi_Battery_SimParams();
                    setUpCells();

                    if(cellDisplayAdapter.getItemCount()>0){

                        startRefreshThread();
                    }

                } catch (SecurityException ex) {

                    String s = ex.getMessage();

                } catch (Exception ex) {

                    String s = ex.getMessage();

                }

            }//PermissionGranted
        } catch (Exception ex) {

            String s = ex.getMessage();
        }
    }//setSIMValuesOtherVersionApI


    private void getWifi_Battery_SimParams(){

        int sid;

        String simdata="";

        try {

          /*  sid = CellUtility.getDefault(thisActvity);
            if (sid == 1) {


              //  txt_sim_data_connected.setText("SIM Data:"+" "+CellUtility.getDataStateName(tm.getDataState()));
                simdata = CellUtility.getDataStateName(tm.getDataState());
                txt_sim_data_connected.setText("SIM Data: "+simdata);
            } else if (sid == 2) {


               // txt_sim_data_connected.setText("SIM Data:"+" "+CellUtility.getDataStateName(tm.getDataState()));
              //  txt_sim_data_connected.setText("SIM Data: "+" "+CellUtility.getDataStateName(tm.getDataState()));
                simdata = CellUtility.getDataStateName(tm.getDataState());
                txt_sim_data_connected.setText("SIM Data: "+simdata);
            } else {
                sid = CellUtility.getDefault(thisActvity);
                if (sid == 1) {
                   // txt_sim_data_connected.setText("SIM Data:"+" "+CellUtility.getDataStateName(tm.getDataState()));
                    simdata = CellUtility.getDataStateName(tm.getDataState());
                    txt_sim_data_connected.setText("SIM Data: "+simdata);
                }
                else if(sid==3){
                    simdata = CellUtility.getDataStateName(tm.getDataState());
                    txt_sim_data_connected.setText("SIM Data: "+simdata);
                }
            }*/

            sid = CellUtility.getDefault(thisActvity);
            simdata = CellUtility.getDataStateName(tm.getDataState());
            txt_sim_data_connected.setText("SIM Data: "+simdata);




        }
        catch(Exception ex){

            String s = ex.getMessage();
        }

        try {

            // this.registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

            txt_battery.setText("Battery: "+
                    CellUtility.getBatteryPercentage(thisActvity)+"");
            String source = txt_battery.getText().toString();
            int start= source.indexOf(':') + 2;
            int ent = source.length();
            Spannable  wordtoSpan = new SpannableString(
                    txt_battery.getText().toString());
            wordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#3CB64A")),
                    start,ent, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            txt_battery.setText(wordtoSpan);

            if (CellUtility.isWifi(thisActvity)) {
                // txt_wifi_connec_stats.setText("Yes");
                String wifis = "Wifi Connected: Yes";
                start = wifis.indexOf(":") +1;
                ent = wifis.length();
                wordtoSpan = new SpannableString(wifis);
                ForegroundColorSpan foregroundSpan = new ForegroundColorSpan(Color.RED);
                    wordtoSpan.setSpan(foregroundSpan, start,ent, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                txt_wifi_connec.setText(wordtoSpan);

            } else {

                String wifis = "Wifi Connected: No";
                start = wifis.indexOf(":") +1;
                ent = wifis.length();
                wordtoSpan = new SpannableString(wifis);
                ForegroundColorSpan foregroundSpan = new ForegroundColorSpan(Color.parseColor("#3CB64A"));
                wordtoSpan.setSpan(foregroundSpan, start,ent, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                txt_wifi_connec.setText(wordtoSpan);

            }


            if(Build.VERSION.SDK_INT<30) {
                AsyncTaskExample asy = new AsyncTaskExample();
                asy.execute();
            }

        }
        catch (SecurityException ex){

            String s = ex.getLocalizedMessage();
        }
        catch(Exception ex){
            String s = ex.getMessage();
        }

    }





    public static boolean isHostAvailable() {



        try {
            HttpURLConnection.setFollowRedirects(false);
            // note : you may also need
            // HttpURLConnection.setInstanceFollowRedirects(false)
            HttpURLConnection con = (HttpURLConnection) new URL("http://www.google.com")
                    .openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private class AsyncTaskExample extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            return(isHostAvailable());
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                txt_ping.setText("Ping: Success");
                //pingstatus.setTextColor(Color.GREEN);
                String source =  txt_ping.getText().toString();
                int start = source.indexOf(':') + 2;
                int ent = source.length();
                Spannable wordtoSpan = new SpannableString(
                        txt_ping.getText().toString());


                wordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#3CB64A")),
                        start,ent, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                txt_ping.setText(wordtoSpan);
            }
            else{
                txt_ping.setText("Ping: Failed");
                //pingstatus.setTextColor(Color.RED);
            }
        }
    }

    public static final Spannable getColoredString(Context context, CharSequence text,String color) {
        Spannable spannable = new SpannableString(text);
        spannable.setSpan(new ForegroundColorSpan(Color.parseColor(color)), 0, spannable.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }


    ///////////////////////////Refresh Timer Methods////////////////////////////////////////////////////////////////////////////////////////////
    private void refreshCellInfo(){

        if (cellDisplayAdapter.getItemCount() > 0) {

            //Recycler view has items
            //So start your refresh
            Timer t = new Timer();
            //Set the schedule function and rate
            t.scheduleAtFixedRate(new TimerTask() {

                   @Override
                    public void run() {
                       //Called each time when 1000
                         // milliseconds (1 second) (the period parameter)
                      // populateCellInfo();
                        getWifi_Battery_SimParams();
                        refreshMeasuresForPrimaryServingCell_Old();

                       //setUpCellList();
                       }
                    },
            //Set how long before to start calling the TimerTask (in milliseconds)
           0,
            //Set the amount of time between each execution (in milliseconds)
           2000);
       }
    }//refreshCellInfo



    @Override
    public void onStop() {
        super.onStop();
        try {
/*          if (timer != null) {
                timer.cancel();
            }*/
            if(handler!=null){
                handler.removeCallbacks(runnableCode);
                threadStart =false;
            }
         // threadExec.shutdownNow();

        }
        catch (Exception ex){

        }
    }


    private void refreshMeasuresForPrimaryServingCell_Old() {

        try {
            List<CellInfo> cellInfos = tm.getAllCellInfo();
            for (int i = 0; i < cellInfos.size(); i++) {
                CellInfo info = cellInfos.get(i);
                if (info != null) {

                    if (info instanceof CellInfoGsm) { //GSM

                        if (info.isRegistered()) {


                            // ARFCN, BSIC,CID,DBM,level and the rest.
                            String[] servCellParms = {"ARFCN", "BSIC", "CID", "DBM", "LEVEL",
                                    "LAC", "MNC", "MCC", "ASU", "RSSI","TIMINGADV"};

                            int rowSize = 2;
                            int columnSize = servCellParms.length;
                            String key = "G_S";
                            String[][] gsmTable = new String[rowSize][columnSize];
                            for (int k = 0; k < columnSize; k++) {
                                gsmTable[0][k] = servCellParms[k];
                            }//k

                            final CellSignalStrengthGsm gsmSignal = ((CellInfoGsm) info).getCellSignalStrength();
                            final CellIdentityGsm identityGsm = ((CellInfoGsm) info).getCellIdentity();

                            if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                                gsmTable[1][0] = identityGsm.getArfcn() == Integer.MAX_VALUE ? "-" : identityGsm.getArfcn() +"";
                                gsmTable[1][1] = identityGsm.getBsic() == Integer.MAX_VALUE ? "-" :identityGsm.getBsic() + "";
                            }
                            else{
                                gsmTable[1][0] = "-";
                                gsmTable[1][1] = "-";
                            }
                            gsmTable[1][2] = identityGsm.getCid() == Integer.MAX_VALUE ? "-" : identityGsm.getCid()+"";

                            gsmTable[1][3] = gsmSignal.getDbm() == Integer.MAX_VALUE ? "-" : gsmSignal.getDbm()+"";

                            gsmTable[1][4] = gsmSignal.getLevel() == Integer.MAX_VALUE ? "-" : gsmSignal.getLevel() +"";

                            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {

                                gsmTable[1][2] = gsmSignal.getTimingAdvance() + "";
                            }
                            gsmTable[1][5] = identityGsm.getLac() == Integer.MAX_VALUE ? "-" : identityGsm.getLac()+"";
                            if(Build.VERSION.SDK_INT >=28) {
                                gsmTable[1][6] = identityGsm.getMncString() == null ? "-" : identityGsm.getMncString();
                            }
                            else{
                                gsmTable[1][6] = identityGsm.getMnc()==Integer.MAX_VALUE  ? "-" :  identityGsm.getMnc()+"";
                            }
                            if(Build.VERSION.SDK_INT >=28) {
                                gsmTable[1][7] = identityGsm.getMccString() == null ? "-" : identityGsm.getMccString();
                            }
                            else{
                                gsmTable[1][7] = identityGsm.getMcc()==Integer.MAX_VALUE  ? "-" :  identityGsm.getMcc()+"";
                            }
                            gsmTable[1][8] = gsmSignal.getAsuLevel()==Integer.MAX_VALUE ? "-" : gsmSignal.getAsuLevel()+"";
                            if(Build.VERSION.SDK_INT >=30) {
                                gsmTable[1][9] = gsmSignal.getRssi() == Integer.MAX_VALUE ? "-" : gsmSignal.getRssi() + "";
                            }
                            else{
                                gsmTable[1][10] = "-";
                            }
                            if(Build.VERSION.SDK_INT >=26) {
                                gsmTable[1][10] = gsmSignal.getTimingAdvance() == Integer.MAX_VALUE ? "-" : gsmSignal.getTimingAdvance() + "";
                            }
                            else{
                                gsmTable[1][10] = "-";
                            }
                            CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                            cellMap = new HashMap<String, String[][]>();
                            cellMap.put(key, gsmTable);
                            model.setCellType("G_S");
                            model.setCellTypeName("Primary Serving Cell");
                            model.setRows(rowSize);
                            model.setColumns(columnSize);
                            model.setCellMap(cellMap);
                            // lstDisplay.add(model);
                            if(lstDisplay.size()>0) {
                                lstDisplay.set(0, model);
                                cellDisplayAdapter.notifyItemChanged(0);
                            }

                        } else {
                            //gsmCells.add((CellInfoGsm)info);
                        }

                    }//if
                    else if (info instanceof CellInfoLte) {

                        if (info.isRegistered()) {
                            //LTE
                           // EARFCN, PCI, RSRP, RSRQ, ASU,dBM,Level,cqi,rssnr,timingadv,MNC,MCC,CI,tac, bandwidth.
                            String[] servCellParms = {"EARFCN", "PCI", "RSRP", "RSRQ", "ASU", "DBM",
                                    "LEVEL", "CQI", "RSSNR", "TIMINGADV", "MNC", "MCC", "CI", "TAC", "BANDWIDTH"};

                            int rowSize = 2;
                            int columnSize = servCellParms.length;
                            String key = "L_S";
                            String[][] lteTable = new String[rowSize][columnSize];
                            for (int j = 0; j < columnSize; j++) {
                                lteTable[0][j] = servCellParms[j];
                            }//k
                            final CellSignalStrengthLte lteSignal = ((CellInfoLte) info).getCellSignalStrength();
                            final CellIdentityLte lteIdentity = ((CellInfoLte) info).getCellIdentity();

                            if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {

                                lteTable[1][0] = lteIdentity.getEarfcn()==Integer.MAX_VALUE?"-":lteIdentity.getEarfcn() + "";
                            }
                            else{
                                lteTable[1][0] = "-"; //1
                            }

                            //2
                            lteTable[1][1] =   lteIdentity.getPci()==Integer.MAX_VALUE ?"-"  :   lteIdentity.getPci() + "";

                            //3
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                                lteTable[1][2] =  lteSignal.getRsrp() == Integer.MAX_VALUE ?"-":  lteSignal.getRsrp() + ""; //3
                                lteTable[1][3] = lteSignal.getRsrq()==Integer.MAX_VALUE ? "-" : lteSignal.getRsrq() + ""; //4
                            }
                            else{
                                lteTable[1][2] = "-";
                                lteTable[1][3] =  "-";

                            }

                            lteTable[1][4] =  lteSignal.getAsuLevel()==Integer.MAX_VALUE ?"-" : lteSignal.getAsuLevel() + ""; // 5 ASU
                            lteTable[1][5] = lteSignal.getDbm()==Integer.MAX_VALUE ?"-": lteSignal.getDbm() + ""; // 6 DBM

                            lteTable[1][6] = lteSignal.getLevel()==Integer.MAX_VALUE ?"-" : lteSignal.getLevel() + ""; //7 LEVEL

                            if(Build.VERSION.SDK_INT >= 28) {
                                lteTable[1][7] = lteSignal.getCqi()==Integer.MAX_VALUE ? "-" : lteSignal.getCqi() + ""; //8 CQI
                            }
                            else{
                                lteTable[1][7] = "-"; //8 CQI
                            }

                            if(Build.VERSION.SDK_INT >= 28) {
                                lteTable[1][8] =  lteSignal.getRssnr()==Integer.MAX_VALUE ?"-" :lteSignal.getRssnr()+""; //8
                            }
                            else{
                                lteTable[1][8] = "-"; //8
                            }

                            lteTable[1][9] = lteSignal.getTimingAdvance()==
                                    Integer.MAX_VALUE ?"-" : lteSignal.getTimingAdvance() + ""; //11 TIMING
                            if (Build.VERSION.SDK_INT >= 28) {
                                lteTable[1][10] = lteIdentity.getMncString()==null ?"-" : lteIdentity.getMncString() + ""; //11 MNC
                                lteTable[1][11] = lteIdentity.getMccString() ==null ?"-" :lteIdentity.getMccString()+""; //12//MCC

                            }
                            else{

                                lteTable[1][10] = lteIdentity.getMnc()==Integer.MAX_VALUE ?"-": lteIdentity.getMnc() + ""; //11 MNC
                                lteTable[1][11] = lteIdentity.getMnc()==Integer.MAX_VALUE ?"-": lteIdentity.getMnc()+""; //12//MCC
                            }

                            lteTable[1][12] = lteIdentity.getCi()==Integer.MAX_VALUE ?"-" :lteIdentity.getCi()+ ""; //13 CI
                            lteTable[1][13] = lteIdentity.getTac()==Integer.MAX_VALUE ? "-" : lteIdentity.getTac()+"" ; //14 TAC
                            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                                lteTable[1][14] = lteIdentity.getBandwidth()==Integer.MAX_VALUE ? "-" :
                                        lteIdentity.getBandwidth() + ""; //15 BANDWIDTH
                            }
                            else{
                                lteTable[1][14] = "-";
                            }

                            CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                            cellMap = new HashMap<String, String[][]>();
                            cellMap.put(key, lteTable);
                            model.setCellType("L_S");
                            model.setCellTypeName("Primary Serving Cell");
                            model.setRows(rowSize);
                            model.setColumns(columnSize);
                            model.setCellMap(cellMap);
                            //lstDisplay.add(model);
                            if(lstDisplay.size()>0) {
                                lstDisplay.set(0, model);
                               // cellDisplayAdapter.updatePrimaryCellMeasureAtIndex(model.getCellType(),model);
                            }
                        } else {
                            //  lteCells.add((CellInfoLte)info);
                        }


                    } else if (info instanceof CellInfoWcdma) {

                        //3G
                        if (info.isRegistered()) {


                        //    RSCP = in asu
                            String[] servCellParms = {"UARFCN", "PSC", "RSCP", "ECNO", "DBM","LEVEL","CID","LAC","MNC","MCC"};
                            int rowSize = 2;
                            int columnSize = servCellParms.length;
                            String key = "W_S";
                            String[][] wcdmaTable = new String[rowSize][columnSize];
                            for (int j = 0; j < columnSize; j++) {
                                wcdmaTable[0][j] = servCellParms[j];
                            }//k
                            final CellSignalStrengthWcdma wcdmaSignal = ((CellInfoWcdma) info).getCellSignalStrength();
                            final CellIdentityWcdma wcdmaIdentity = ((CellInfoWcdma) info).getCellIdentity();

                            if (Build.VERSION.SDK_INT >= 24) {
                                wcdmaTable[1][0] = wcdmaIdentity.getUarfcn() == Integer.MAX_VALUE ? "-" : wcdmaIdentity.getUarfcn() + "";
                            }
                            else{
                                wcdmaTable[1][0]="-";
                            }
                            wcdmaTable[1][1] =wcdmaIdentity.getPsc()==Integer.MAX_VALUE ? "-" : wcdmaIdentity.getPsc()+"";
                            wcdmaTable[1][2] =wcdmaSignal.getAsuLevel()==Integer.MAX_VALUE ? "-" : wcdmaSignal.getAsuLevel()+"";
                           // RSCP in asu
                            if(Build.VERSION.SDK_INT >= 30){
                                wcdmaTable[1][3] = wcdmaSignal.getEcNo() == Integer.MAX_VALUE ? "-" : wcdmaSignal.getEcNo() + "";
                            }
                            else{
                                wcdmaTable[1][3]="-";
                            }

                            wcdmaTable[1][4] = wcdmaSignal.getDbm()== Integer.MAX_VALUE ?"-" :wcdmaSignal.getDbm()+"";

                            wcdmaTable[1][5] =wcdmaSignal.getLevel() == Integer.MAX_VALUE ? "-" : wcdmaSignal.getLevel() +"";

                            wcdmaTable[1][6] =wcdmaIdentity.getCid() == Integer.MAX_VALUE ? "-" : wcdmaIdentity.getCid() + "";

                            wcdmaTable[1][7] =wcdmaIdentity.getLac() == Integer.MAX_VALUE ? "-" : wcdmaIdentity.getLac() + "";

                            if (Build.VERSION.SDK_INT >= 28) {
                                wcdmaTable[1][8] = wcdmaIdentity.getMncString() + "";
                                wcdmaTable[1][9] = wcdmaIdentity.getMccString() + "";
                            } else {
                                wcdmaTable[1][9] = wcdmaIdentity.getMnc() + "";
                                wcdmaTable[1][9] = wcdmaIdentity.getMcc() + "";
                            }
                            CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                            cellMap = new HashMap<String, String[][]>();
                            cellMap.put(key, wcdmaTable);
                            model.setCellType("W_S");
                            model.setCellTypeName("Primary Serving Cell");
                            model.setRows(rowSize);
                            model.setColumns(columnSize);
                            model.setCellMap(cellMap);
                            // lstDisplay.add(model);
                            if(lstDisplay.size()>0) {
                                lstDisplay.set(0, model);
                                cellDisplayAdapter.notifyItemChanged(0);
                            }
                        } else {

                            //wCdmaCells.add((CellInfoWcdma)info);
                        }


                    }//cdma 3G
                    else if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

                        if (info instanceof CellInfoNr) {

                            if (info.isRegistered()) {

                                String[] servCellParms = {"MCC", "MNC", "NCI", "NRARFCN", "PCI",
                                        "CSIRSRP", "CSISINR", "DBM", "LEVEL", "SSRSRP", "SSRSRQ", "SSSINR"};
                                int rowSize = 2;
                                int columnSize = servCellParms.length;
                                String key = "5_S";
                                String[][] fiveGTable = new String[rowSize][columnSize];
                                for (int j = 0; j < columnSize; j++) {
                                    fiveGTable[0][j] = servCellParms[j];
                                }//k

                                final CellIdentityNr fiveGIdentity = (CellIdentityNr) ((CellInfoNr) info).getCellIdentity();
                                final CellSignalStrengthNr fiveGISignal = (CellSignalStrengthNr) ((CellInfoNr) info).getCellSignalStrength();

                                fiveGTable[1][0] = fiveGIdentity.getMccString() + "";
                                fiveGTable[1][1] = fiveGIdentity.getMncString() + "";
                                fiveGTable[1][2] = fiveGIdentity.getNci() + "";
                                fiveGTable[1][3] = fiveGIdentity.getNrarfcn() + "";
                                fiveGTable[1][4] = fiveGIdentity.getPci() + "";

                                fiveGTable[1][5] = fiveGISignal.getCsiRsrp() + "";
                                fiveGTable[1][6] = fiveGISignal.getCsiSinr() + "";
                                fiveGTable[1][7] = fiveGISignal.getDbm() + "";
                                fiveGTable[1][8] = fiveGISignal.getLevel() + "";

                                fiveGTable[1][9] = fiveGISignal.getSsRsrp() + "";
                                fiveGTable[1][10] = fiveGISignal.getSsRsrq() + "";
                                fiveGTable[1][11] = fiveGISignal.getSsSinr() + "";
                                CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                                cellMap = new HashMap<String, String[][]>();
                                cellMap.put(key, fiveGTable);
                                model.setCellType("5_S");
                                model.setCellTypeName("Primary Serving Cell");
                                model.setRows(rowSize);
                                model.setColumns(columnSize);
                                model.setCellMap(cellMap);

                                if(lstDisplay.size()>0) {
                                    lstDisplay.set(0, model);
                                    cellDisplayAdapter.notifyItemChanged(0);
                                }
                            } else {

                                // cellInfoNrs.add((CellInfoNr)info);
                            }
                        }
                    }

                }//if null
            }//for

        } catch (SecurityException ex) {

        } catch (Exception ex) {

        }
    }//refreshPrimaryCellINfo


    private List<CellInfo> collectPrimaryServingCell(List<CellInfo> cellInfos){

        List<CellInfo> primaryInfo;
        try {

            primaryInfo = new ArrayList<CellInfo>();

            for (CellInfo cinfo : cellInfos) {

                if (cinfo.isRegistered()) {

                    primaryInfo.add(cinfo);

                }
            }
            return primaryInfo;
        }
        catch (Exception ex){

            String s = ex.getMessage();
            return null;
        }
    }

    private void collectMeasuresForPrimaryServingCell(List<CellInfo> primaryCells) {

        try {
            if(primaryCells!=null) {

                for (int i = 0; i < primaryCells.size(); i++) {
                    CellInfo info = primaryCells.get(i);
                    if (info != null) {

                        if (info instanceof CellInfoGsm) { //GSM
                            int maxlevel=0;

                            // ARFCN, BSIC,CID,DBM,level and the rest.
                            String[] servCellParms = new String[]{"ARFCN", "BSIC", "CID", "DBM", "LEVEL",
                                    "LAC", "MNC", "MCC", "ASU", "RSSI", "TIMINGADV"};

                            int rowSize = 2;
                            int columnSize = servCellParms.length;
                            String key = "G_S";
                            String[][] gsmTable = new String[rowSize][columnSize];
                            for (int k = 0; k < columnSize; k++) {
                                gsmTable[0][k] = servCellParms[k];
                            }//k



                            final CellSignalStrengthGsm gsmSignal = ((CellInfoGsm) info).getCellSignalStrength();
                            final CellIdentityGsm identityGsm = ((CellInfoGsm) info).getCellIdentity();

                            if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                                gsmTable[1][0] = identityGsm.getArfcn() == Integer.MAX_VALUE ? "-" : identityGsm.getArfcn() + "";
                                gsmTable[1][1] = identityGsm.getBsic() == Integer.MAX_VALUE ? "-" : identityGsm.getBsic() + "";
                            } else {
                                gsmTable[1][0] = "-";
                                gsmTable[1][1] = "-";
                            }
                            gsmTable[1][2] = identityGsm.getCid() == Integer.MAX_VALUE ? "-" : identityGsm.getCid() + "";

                            gsmTable[1][3] = gsmSignal.getDbm() == Integer.MAX_VALUE ? "-" : gsmSignal.getDbm() + "";

                            gsmTable[1][4] = gsmSignal.getLevel() == Integer.MAX_VALUE ? "-" : gsmSignal.getLevel() + "";

                            if (gsmSignal.getLevel() != Integer.MAX_VALUE) {
                                if (gsmSignal.getLevel() > maxlevel) {
                                    maxlevel = gsmSignal.getLevel();
                                }
                            }



                            gsmTable[1][5] = identityGsm.getLac() == Integer.MAX_VALUE ? "-" : identityGsm.getLac() + "";
                            if (Build.VERSION.SDK_INT >= 28) {
                                gsmTable[1][6] = identityGsm.getMncString() == null ? "-" : identityGsm.getMncString();
                            } else {
                                gsmTable[1][6] = identityGsm.getMnc() == Integer.MAX_VALUE ? "-" : identityGsm.getMnc() + "";
                            }
                            if (Build.VERSION.SDK_INT >= 28) {
                                gsmTable[1][7] = identityGsm.getMccString() == null ? "-" : identityGsm.getMccString();
                            } else {
                                gsmTable[1][7] = identityGsm.getMcc() == Integer.MAX_VALUE ? "-" : identityGsm.getMcc() + "";
                            }
                            gsmTable[1][8] = gsmSignal.getAsuLevel() == Integer.MAX_VALUE ? "-" : gsmSignal.getAsuLevel() + "";
                            if (Build.VERSION.SDK_INT >= 30) {
                                gsmTable[1][9] = gsmSignal.getRssi() == Integer.MAX_VALUE ? "-" : gsmSignal.getRssi() + "";
                            } else {
                                gsmTable[1][10] = "-";
                            }
                            if (Build.VERSION.SDK_INT >= 26) {
                                gsmTable[1][10] = gsmSignal.getTimingAdvance() == Integer.MAX_VALUE ? "-" : gsmSignal.getTimingAdvance() + "";
                            } else {
                                gsmTable[1][10] = "-";
                            }
                            CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                            cellMap = new HashMap<String, String[][]>();
                            cellMap.put(key, gsmTable);
                            model.setCellType("G_S");
                            model.setCellTypeName("Primary Serving Cell");
                            model.setRows(rowSize);
                            model.setColumns(columnSize);
                            model.setCellMap(cellMap);
                            model.setMaxlevel(maxlevel);

                           // lstDisplay.add(model);
                            cellDisplayAdapter.insertCellMeasure(model);


                        }//if
                        else if (info instanceof CellInfoLte) {

                            int maxlevel=0;
                            //LTE
                            // EARFCN, PCI, RSRP, RSRQ, ASU,dBM,Level,cqi,rssnr,timingadv,MNC,MCC,CI,tac, bandwidth.
                            String[] servCellParms =new String[]{"EARFCN", "PCI", "RSRP", "RSRQ", "ASU", "DBM",
                                    "LEVEL", "CQI", "RSSNR", "TIMINGADV", "MNC", "MCC", "CI", "TAC", "BANDWIDTH"};

                            int rowSize = 2;
                            int columnSize = servCellParms.length;
                            String key = "L_S";
                            String[][] lteTable = new String[rowSize][columnSize];
                            for (int j = 0; j < columnSize; j++) {
                                lteTable[0][j] = servCellParms[j];
                            }//k
                            final CellSignalStrengthLte lteSignal = ((CellInfoLte) info).getCellSignalStrength();
                            final CellIdentityLte lteIdentity = ((CellInfoLte) info).getCellIdentity();

                            if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {

                                lteTable[1][0] = lteIdentity.getEarfcn() == Integer.MAX_VALUE ? "-" : lteIdentity.getEarfcn() + "";
                            } else {
                                lteTable[1][0] = "-"; //1
                            }

                            //2
                            lteTable[1][1] = lteIdentity.getPci() == Integer.MAX_VALUE ? "-" : lteIdentity.getPci() + "";

                            //3
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                                lteTable[1][2] = lteSignal.getRsrp() == Integer.MAX_VALUE ? "-" : lteSignal.getRsrp() + ""; //3
                                lteTable[1][3] = lteSignal.getRsrq() == Integer.MAX_VALUE ? "-" : lteSignal.getRsrq() + ""; //4
                            } else {
                                lteTable[1][2] = "-";
                                lteTable[1][3] = "-";

                            }

                            lteTable[1][4] = lteSignal.getAsuLevel() == Integer.MAX_VALUE ? "-" : lteSignal.getAsuLevel() + ""; // 5 ASU
                            lteTable[1][5] = lteSignal.getDbm() == Integer.MAX_VALUE ? "-" : lteSignal.getDbm() + ""; // 6 DBM

                            lteTable[1][6] = lteSignal.getLevel() == Integer.MAX_VALUE ? "-" : lteSignal.getLevel() + ""; //7 LEVEL

                            if (lteSignal.getLevel() != Integer.MAX_VALUE) {
                                if (lteSignal.getLevel() > maxlevel) {
                                    maxlevel = lteSignal.getLevel();
                                }
                            }

                            if (Build.VERSION.SDK_INT >= 28) {
                                lteTable[1][7] = lteSignal.getCqi() == Integer.MAX_VALUE ? "-" : lteSignal.getCqi() + ""; //8 CQI
                            } else {
                                lteTable[1][7] = "-"; //8 CQI
                            }

                            if (Build.VERSION.SDK_INT >= 28) {
                                lteTable[1][8] = lteSignal.getRssnr() == Integer.MAX_VALUE ? "-" : lteSignal.getRssnr() + ""; //8
                            } else {
                                lteTable[1][8] = "-"; //8
                            }

                            lteTable[1][9] = lteSignal.getTimingAdvance() ==
                                    Integer.MAX_VALUE ? "-" : lteSignal.getTimingAdvance() + ""; //11 TIMING
                            if (Build.VERSION.SDK_INT >= 28) {
                                lteTable[1][10] = lteIdentity.getMncString() == null ? "-" : lteIdentity.getMncString() + ""; //11 MNC
                                lteTable[1][11] = lteIdentity.getMccString() == null ? "-" : lteIdentity.getMccString() + ""; //12//MCC

                            } else {

                                lteTable[1][10] = lteIdentity.getMnc() == Integer.MAX_VALUE ? "-" : lteIdentity.getMnc() + ""; //11 MNC
                                lteTable[1][11] = lteIdentity.getMnc() == Integer.MAX_VALUE ? "-" : lteIdentity.getMnc() + ""; //12//MCC
                            }

                            lteTable[1][12] = lteIdentity.getCi() == Integer.MAX_VALUE ? "-" : lteIdentity.getCi() + ""; //13 CI
                            lteTable[1][13] = lteIdentity.getTac() == Integer.MAX_VALUE ? "-" : lteIdentity.getTac() + ""; //14 TAC
                            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                                lteTable[1][14] = lteIdentity.getBandwidth() == Integer.MAX_VALUE ? "-" :
                                        lteIdentity.getBandwidth() + ""; //15 BANDWIDTH
                            } else {
                                lteTable[1][14] = "-";
                            }

                            CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                            cellMap = new HashMap<String, String[][]>();
                            cellMap.put(key, lteTable);
                            model.setCellType("L_S");
                            model.setCellTypeName("Primary Serving Cell");
                            model.setRows(rowSize);
                            model.setColumns(columnSize);
                            model.setCellMap(cellMap);

                           // lstDisplay.add(model);
                            model.setMaxlevel(maxlevel);
                            maxlevel=0;
                            cellDisplayAdapter.insertCellMeasure(model);


                        } else if (info instanceof CellInfoWcdma) {

                            //3G
                            //    RSCP = in asu
                            int maxlevel=0;
                            String[] servCellParms = new String[]{"UARFCN", "PSC", "ASU", "ECNO",
                                    "DBM", "LEVEL", "CID", "LAC", "MNC", "MCC"};
                            int rowSize = 2;
                            int columnSize = servCellParms.length;
                            String key = "W_S";
                            String[][] wcdmaTable = new String[rowSize][columnSize];
                            for (int j = 0; j < columnSize; j++) {
                                wcdmaTable[0][j] = servCellParms[j];
                            }//k
                            final CellSignalStrengthWcdma wcdmaSignal = ((CellInfoWcdma) info).getCellSignalStrength();
                            final CellIdentityWcdma wcdmaIdentity = ((CellInfoWcdma) info).getCellIdentity();

                            if (Build.VERSION.SDK_INT >= 24) {
                                wcdmaTable[1][0] = wcdmaIdentity.getUarfcn() == Integer.MAX_VALUE ? "-" : wcdmaIdentity.getUarfcn() + "";
                            } else {
                                wcdmaTable[1][0] = "-";
                            }
                            wcdmaTable[1][1] = wcdmaIdentity.getPsc() == Integer.MAX_VALUE ? "-" : wcdmaIdentity.getPsc() + "";
                            wcdmaTable[1][2] = wcdmaSignal.getAsuLevel() == Integer.MAX_VALUE ? "-" : wcdmaSignal.getAsuLevel() + "";
                            // RSCP in asu
                            if (Build.VERSION.SDK_INT >= 30) {
                                wcdmaTable[1][3] = wcdmaSignal.getEcNo() == Integer.MAX_VALUE ? "-" : wcdmaSignal.getEcNo() + "";
                            } else {
                                wcdmaTable[1][3] = "-";
                            }

                            wcdmaTable[1][4] = wcdmaSignal.getDbm() == Integer.MAX_VALUE ? "-" : wcdmaSignal.getDbm() + "";

                            wcdmaTable[1][5] = wcdmaSignal.getLevel() == Integer.MAX_VALUE ? "-" : wcdmaSignal.getLevel() + "";

                            if (wcdmaSignal.getLevel() != Integer.MAX_VALUE) {
                                if (wcdmaSignal.getLevel() > maxlevel) {
                                    maxlevel = wcdmaSignal.getLevel();
                                }
                            }

                            wcdmaTable[1][6] = wcdmaIdentity.getCid() == Integer.MAX_VALUE ? "-" : wcdmaIdentity.getCid() + "";

                            wcdmaTable[1][7] = wcdmaIdentity.getLac() == Integer.MAX_VALUE ? "-" : wcdmaIdentity.getLac() + "";

                            if (Build.VERSION.SDK_INT >= 28) {
                                wcdmaTable[1][8] = wcdmaIdentity.getMncString() + "";
                                wcdmaTable[1][9] = wcdmaIdentity.getMccString() + "";
                            } else {
                                wcdmaTable[1][8] = wcdmaIdentity.getMnc() + "";
                                wcdmaTable[1][9] = wcdmaIdentity.getMcc() + "";
                            }
                            CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                            cellMap = new HashMap<String, String[][]>();
                            cellMap.put(key, wcdmaTable);
                            model.setCellType("W_S");
                            model.setCellTypeName("Primary Serving Cell");
                            model.setRows(rowSize);
                            model.setColumns(columnSize);
                            model.setCellMap(cellMap);
                            //lstDisplay.add(model);
                            model.setMaxlevel(maxlevel);
                            maxlevel=0;
                            cellDisplayAdapter.insertCellMeasure(model);


                        }//cdma 3G
                        else if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

                            if (info instanceof CellInfoNr) {
                                int maxlevel=0;
                                String[] servCellParms = new String[]  {"NRARFCN", "PCI", "NCI", "DBM", "LEVEL", "CSIRSRP",
                                        "CSISINR", "SSRSRP", "SSRSRQ", "SSSINR", "MNC", "MCC"};

                                int rowSize = 2;
                                int columnSize = servCellParms.length;
                                String key = "5_S";
                                String[][] fiveGTable = new String[rowSize][columnSize];
                                for (int j = 0; j < columnSize; j++) {
                                    fiveGTable[0][j] = servCellParms[j];
                                }//k

                                final CellIdentityNr fiveGIdentity = (CellIdentityNr) ((CellInfoNr) info).getCellIdentity();
                                final CellSignalStrengthNr fiveGISignal = (CellSignalStrengthNr) ((CellInfoNr) info).getCellSignalStrength();

                                fiveGTable[1][0] = fiveGIdentity.getNrarfcn()==Integer.MAX_VALUE ? "-":
                                        fiveGIdentity.getNrarfcn() + "";

                                fiveGTable[1][1] = fiveGIdentity.getPci()==Integer.MAX_VALUE ? "-":
                                        fiveGIdentity.getPci() + "";

                                fiveGTable[1][2] = fiveGIdentity.getNci()==Integer.MAX_VALUE ? "-":
                                        fiveGIdentity.getNci() + "";

                                fiveGTable[1][3] = fiveGISignal.getDbm()==Integer.MAX_VALUE ? "-":
                                        fiveGISignal.getDbm() + "";

                                if(fiveGISignal.getLevel()!=Integer.MAX_VALUE){
                                    fiveGTable[1][4] = fiveGISignal.getLevel()+"";
                                    if(fiveGISignal.getLevel() > maxlevel){
                                        maxlevel = fiveGISignal.getLevel();
                                    }
                                }
                                else{
                                    fiveGTable[1][4] = "-";
                                }

                                fiveGTable[1][5] = fiveGISignal.getCsiRsrp()==Integer.MAX_VALUE ? "-" :  fiveGISignal.getCsiRsrp()+"";

                                fiveGTable[1][6] = fiveGISignal.getCsiSinr()== Integer.MAX_VALUE ?"-" : fiveGISignal.getCsiSinr()+"";

                                fiveGTable[1][7] = fiveGISignal.getSsRsrp() ==Integer.MAX_VALUE ?"-" :fiveGISignal.getSsRsrp()+"";
                                fiveGTable[1][8] = fiveGISignal.getSsRsrq() ==Integer.MAX_VALUE ?"-" :fiveGISignal.getSsRsrq()+"";
                                fiveGTable[1][9] = fiveGISignal.getSsSinr() ==Integer.MAX_VALUE?"-" :fiveGISignal.getSsSinr()+"";
                                fiveGTable[1][10] = fiveGIdentity.getMncString() ==null?"-" :fiveGIdentity.getMncString();
                                fiveGTable[1][11] = fiveGIdentity.getMccString() ==null?"-" :fiveGIdentity.getMccString();

                                CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                                cellMap = new HashMap<String, String[][]>();
                                cellMap.put(key, fiveGTable);
                                model.setCellType("5_S");
                                model.setCellTypeName("Primary Serving Cell");
                                model.setRows(rowSize);
                                model.setColumns(columnSize);
                                model.setCellMap(cellMap);
                               // lstDisplay.add(model);
                                model.setMaxlevel(maxlevel);
                                maxlevel=0;
                                cellDisplayAdapter.insertCellMeasure(model);

                            }
                        }

                    }//if null
                }//for
            }

        } catch (SecurityException ex) {

        } catch (Exception ex) {

        }
    }//refreshPrimaryCellINfo


    private void collectNeighbouringCells(List<CellInfo> cellInfos){
        if(gsmCells.size()>0){
            gsmCells.clear();
        }
        if(cellInfoNrs.size()>0){
            cellInfoNrs.clear();
        }
        if(wCdmaCells.size()>0){
            wCdmaCells.clear();
        }
        if(lteCells.size()>0){
            lteCells.clear();
        }
        try {
            for (CellInfo cinfo : cellInfos) {

                if (!cinfo.isRegistered()) {

                    if (cinfo instanceof CellInfoGsm) {

                        gsmCells.add((CellInfoGsm) cinfo);
                        if (gsmCells.size() > 6) {
                            for (int i = 7; i < gsmCells.size(); i++) {
                                gsmCells.remove(i);
                            }
                        }
                    } else if (cinfo instanceof CellInfoLte) {

                        lteCells.add((CellInfoLte) cinfo);
                        if (lteCells.size() > 6) {
                            for (int i = 7; i < lteCells.size(); i++) {
                                lteCells.remove(i);
                            }
                        }
                    } else if (cinfo instanceof CellInfoWcdma) {

                        wCdmaCells.add((CellInfoWcdma) cinfo);
                        if (wCdmaCells.size() > 6) {
                            for (int i = 7; i < wCdmaCells.size(); i++) {
                                wCdmaCells.remove(i);
                            }
                        }
                    }else if(Build.VERSION.SDK_INT >= 29) {
                        if (cinfo instanceof CellInfoNr) {

                            cellInfoNrs.add((CellInfoNr) cinfo);
                            if (cellInfoNrs.size() > 6) {
                                for (int i = 7; i < cellInfoNrs.size(); i++) {
                                    cellInfoNrs.remove(i);
                                }
                            }
                        }//elseif
                    }
                }
            }
        }
        catch (Exception ex){

            String s = ex.getMessage();
        }
    }


     private void groupMeasuresofNeighbouringCellsOrderd(){

        //5G 4G 3G 2G
        try{
           //5G

                if (cellInfoNrs != null ) {
                    int maxlevel=0;
                    String[] FivGParams = new String[]{"NRARFCN", "PCI", "NCI", "DBM", "LEVEL", "CSIRSRP",
                            "CSISINR", "SSRSRP", "SSRSRQ", "SSSINR", "MNC", "MCC"};
                    int rowSize = cellInfoNrs.size() + 1;
                    int columnSize = 6;
                    String key = "5_N";
                    String[][] fiveGTable = new String[rowSize][columnSize];
                    for (int i = 0; i < columnSize; i++) {
                        fiveGTable[0][i] = FivGParams[i];
                    }//i
                    for (int fiveGIndex = 0; fiveGIndex < cellInfoNrs.size(); fiveGIndex++) {
                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                            final CellIdentityNr fiveGIdentity =
                                    (CellIdentityNr) ((CellInfoNr) cellInfoNrs.get(fiveGIndex)).getCellIdentity();
                            final CellSignalStrengthNr fiveGISignal =
                                    (CellSignalStrengthNr) ((CellInfoNr) cellInfoNrs.get(fiveGIndex)).getCellSignalStrength();

                            if(fiveGIdentity.getNrarfcn()!=Integer.MAX_VALUE) {
                                fiveGTable[fiveGIndex + 1][0] = fiveGIdentity.getNrarfcn() + "";
                            }
                            else{
                                fiveGTable[fiveGIndex + 1][0] ="-";
                            }

                            if(fiveGIdentity.getPci()!=Integer.MAX_VALUE) {
                                fiveGTable[fiveGIndex + 1][1] = fiveGIdentity.getPci() + "";
                            }
                            else{
                                fiveGTable[fiveGIndex + 1][1] = "-";
                            }
                            if(fiveGIdentity.getNci()!=Integer.MAX_VALUE) {
                                fiveGTable[fiveGIndex + 1][2] = fiveGIdentity.getNci() + "";
                            }
                            else{
                                fiveGTable[fiveGIndex + 1][2] ="-";
                            }
                            if(fiveGISignal.getDbm()!=Integer.MAX_VALUE){
                                fiveGTable[fiveGIndex + 1][3] = fiveGISignal.getDbm() + "";
                            }
                            else{
                                fiveGTable[fiveGIndex + 1][3] = "-";
                            }
                            if( fiveGISignal.getLevel()!=Integer.MAX_VALUE){

                                if(fiveGISignal.getLevel()> maxlevel){
                                    maxlevel = fiveGISignal.getLevel();
                                }
                                fiveGTable[fiveGIndex + 1][4] = fiveGISignal.getLevel()+"";
                            }
                            else{
                                fiveGTable[fiveGIndex + 1][4] = "-";
                            }

                            if( fiveGISignal.getCsiRsrp()!=Integer.MAX_VALUE){
                                fiveGTable[fiveGIndex + 1][5] = fiveGISignal.getCsiRsrp() + "";
                            }
                            else{
                                fiveGTable[fiveGIndex + 1][5] = "-";
                            }
                           if(fiveGISignal.getCsiSinr()!=Integer.MAX_VALUE){
                               fiveGTable[fiveGIndex + 1][6] = fiveGISignal.getCsiSinr() + "";
                           }
                           else{
                               fiveGTable[fiveGIndex + 1][6] ="-";
                           }
                           if(fiveGISignal.getSsRsrp()!=Integer.MAX_VALUE){
                               fiveGTable[fiveGIndex + 1][7] = fiveGISignal.getSsRsrp() + "";
                           }
                           else{
                               fiveGTable[fiveGIndex + 1][7] = "-";
                           }

                            if(fiveGISignal.getSsRsrq()!=Integer.MAX_VALUE){
                                fiveGTable[fiveGIndex + 1][8] = fiveGISignal.getSsRsrq() + "";
                            }
                            else{
                                fiveGTable[fiveGIndex + 1][8] = "-";
                            }
                           if(fiveGISignal.getSsSinr() !=Integer.MAX_VALUE){
                               fiveGTable[fiveGIndex + 1][9] = fiveGISignal.getSsSinr() + "";
                           }
                           else{
                               fiveGTable[fiveGIndex + 1][9] = "-";
                           }
                            if(fiveGIdentity.getMncString()!=null){
                                fiveGTable[fiveGIndex + 1][10] = fiveGIdentity.getMncString() + "";
                            }
                            else{
                                fiveGTable[fiveGIndex + 1][10] = "-";
                            }
                            if(fiveGIdentity.getMccString()!=null){
                                fiveGTable[fiveGIndex + 1][11] = fiveGIdentity.getMccString() + "";
                            }else{
                                fiveGTable[fiveGIndex + 1][11] = "-";
                            }


                        }

                    }//for loop

                    CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                    cellMap = new HashMap<String, String[][]>();
                    cellMap.put(key, fiveGTable);
                    model.setCellType("5_N");
                    model.setCellTypeName("5G Measurements");
                    model.setRows(rowSize);
                    model.setColumns(columnSize);
                    model.setCellMap(cellMap);
                    if(cellInfoNrs.size()==0){
                        model.setGroupEmpty(true);
                    }
                    else{
                        model.setGroupEmpty(false);
                    }
                    //lstDisplay.add(model);
                    model.setMaxlevel(maxlevel);
                    maxlevel=0;
                    cellDisplayAdapter.insertCellMeasure(model);
                    if(!cellInfoNrs.isEmpty()){
                        cellInfoNrs.clear();
                    }
                }


            //4G Grouping
            if(lteCells!=null ){

                int maxlevel=0;
                String[] lteParams =new String[] {"EARFCN", "PCI", "RSRP", "RSRQ", "ASU", "DBM",
                        "LEVEL", "CQI", "RSSNR", "TIMINGADV", "MNC", "MCC", "CI", "TAC", "BANDWIDTH"};

                //LTE
                int rowSize = lteCells.size()+1;
                int columnSize = lteParams.length;
                String key = "L_N";
                String[][] lteTable = new String[rowSize][columnSize];
                for(int i =0;i<columnSize;i++){
                    lteTable[0][i]  = lteParams[i];
                }//i
                for(int lteIndex =0;lteIndex<lteCells.size();lteIndex++) {

                    final CellSignalStrengthLte lteSignal = ((CellInfoLte) lteCells.get(lteIndex)).getCellSignalStrength();
                    final CellIdentityLte lteIdentity = ((CellInfoLte) lteCells.get(lteIndex)).getCellIdentity();


                    if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {

                        lteTable[lteIndex + 1][0] = lteIdentity.getEarfcn()==Integer.MAX_VALUE?"-":lteIdentity.getEarfcn() + "";
                    }
                    else{
                        lteTable[lteIndex + 1][0] = "-"; //1
                    }

                    //2
                    lteTable[lteIndex + 1][1] =   lteIdentity.getPci()==Integer.MAX_VALUE ?"-"  :   lteIdentity.getPci() + "";

                    //3
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                        lteTable[lteIndex + 1][2] =  lteSignal.getRsrp() == Integer.MAX_VALUE ?"-":  lteSignal.getRsrp() + ""; //3
                        lteTable[lteIndex + 1][3] = lteSignal.getRsrq()==Integer.MAX_VALUE ? "-" : lteSignal.getRsrq() + ""; //4
                    }
                    else{
                        lteTable[lteIndex + 1][2] = "-";
                        lteTable[lteIndex + 1][3] =  "-";

                    }

                    lteTable[lteIndex + 1][4] =  lteSignal.getAsuLevel()==Integer.MAX_VALUE ?"-" : lteSignal.getAsuLevel() + ""; // 5 ASU
                    lteTable[lteIndex + 1][5] = lteSignal.getDbm()==Integer.MAX_VALUE ?"-": lteSignal.getDbm() + ""; // 6 DBM

                    lteTable[lteIndex + 1][6] = lteSignal.getLevel()==Integer.MAX_VALUE ?"-" : lteSignal.getLevel() + ""; //7 LEVEL

                    if(lteSignal.getLevel()!=Integer.MAX_VALUE){
                        if(lteSignal.getLevel() > maxlevel){
                            maxlevel = lteSignal.getLevel();
                        }
                    }

                    if(Build.VERSION.SDK_INT >= 28) {
                        lteTable[lteIndex + 1][7] = lteSignal.getCqi()==Integer.MAX_VALUE ? "-" : lteSignal.getCqi() + ""; //8 CQI
                    }
                    else{
                        lteTable[lteIndex + 1][7] = "-"; //8 CQI
                    }

                    if(Build.VERSION.SDK_INT >= 28) {
                        lteTable[lteIndex + 1][8] =  lteSignal.getRssnr()==Integer.MAX_VALUE ?"-" :lteSignal.getRssnr()+""; //8
                    }
                    else{
                        lteTable[lteIndex + 1][8] = "-"; //8
                    }

                    lteTable[lteIndex + 1][9] = lteSignal.getTimingAdvance()==
                            Integer.MAX_VALUE ?"-" : lteSignal.getTimingAdvance() + ""; //11 TIMING
                    if (Build.VERSION.SDK_INT >= 28) {
                        lteTable[lteIndex + 1][10] = lteIdentity.getMncString()==null ?"-" : lteIdentity.getMncString() + ""; //11 MNC
                        lteTable[lteIndex + 1][11] = lteIdentity.getMccString() ==null ?"-" :lteIdentity.getMccString()+""; //12//MCC

                    }
                    else{

                        lteTable[lteIndex + 1][10] = lteIdentity.getMnc()==Integer.MAX_VALUE ?"-": lteIdentity.getMnc() + ""; //11 MNC
                        lteTable[lteIndex + 1][11] = lteIdentity.getMnc()==Integer.MAX_VALUE ?"-": lteIdentity.getMnc()+""; //12//MCC
                    }

                    lteTable[lteIndex + 1][12] = lteIdentity.getCi()==Integer.MAX_VALUE ?"-" :lteIdentity.getCi()+ ""; //13 CI
                    lteTable[lteIndex + 1][13] = lteIdentity.getTac()==Integer.MAX_VALUE ? "-" : lteIdentity.getTac()+"" ; //14 TAC
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        lteTable[lteIndex + 1][14] = lteIdentity.getBandwidth()==Integer.MAX_VALUE ? "-" :
                                lteIdentity.getBandwidth() + ""; //15 BANDWIDTH
                    }
                    else{
                        lteTable[lteIndex + 1][14] = "-";
                    }


                }//for loop

                CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                for(int i=0;i<rowSize;i++){
                    for(int j=0;j<columnSize;j++){

                      System.out.print(lteTable[i][j]+"   ");
                    }
                    System.out.println("\n");
                }
                cellMap = new HashMap<String,String[][]>();
                cellMap.put(key,lteTable);
                model.setCellType(key);
                model.setCellTypeName("4G Measurements");
                model.setRows(rowSize);
                model.setColumns(columnSize);
                model.setCellMap(cellMap);
                if(lteCells.size() ==0){
                    model.setGroupEmpty(true);
                }
                else{
                    model.setGroupEmpty(false);
                }
               // lstDisplay.add(model);
                model.setMaxlevel(maxlevel);
                maxlevel=0;
               cellDisplayAdapter.insertCellMeasure(model);
                if(!lteCells.isEmpty()) {
                    lteCells.clear();
                }
            }
            //3G Grouping
            if(wCdmaCells!=null ){
                // String[] wCdmaParams = new String[]{"CID","LAC","MCC","MNC", "PSC","UARFCN"};
                int maxlevel=0;
                String[] wCdmaParams =new String[] {"UARFCN", "PSC", "ASU", "ECNO", "DBM","LEVEL","CID","LAC","MNC","MCC"};
                int rowSize = wCdmaCells.size()+1;
                int columnSize =wCdmaParams.length;
                String key = "W_N";
                String[][] wcdmaTable = new String[rowSize][columnSize];
                for(int i =0;i<columnSize;i++){
                    wcdmaTable[0][i]  = wCdmaParams[i];
                }//i

                for(int cdMaIndex =0;cdMaIndex<wCdmaCells.size();cdMaIndex++){

                    final CellSignalStrengthWcdma wcdmaSignal = ((CellInfoWcdma) wCdmaCells.get(cdMaIndex)).getCellSignalStrength();
                    final CellIdentityWcdma wcdmaIdentity = ((CellInfoWcdma)  wCdmaCells.get(cdMaIndex)).getCellIdentity();


                    if (Build.VERSION.SDK_INT >= 24) {
                        wcdmaTable[cdMaIndex+1][0] = wcdmaIdentity.getUarfcn() == Integer.MAX_VALUE ? "-" : wcdmaIdentity.getUarfcn() + "";
                    }
                    else{
                        wcdmaTable[cdMaIndex+1][0]="-";
                    }
                    wcdmaTable[cdMaIndex+1][1] =wcdmaIdentity.getPsc()==Integer.MAX_VALUE ? "-" : wcdmaIdentity.getPsc()+"";
                    wcdmaTable[cdMaIndex+1][2] =wcdmaSignal.getAsuLevel()==Integer.MAX_VALUE ? "-" : wcdmaSignal.getAsuLevel()+"";
                    // RSCP in asu
                    if(Build.VERSION.SDK_INT >= 30){
                        wcdmaTable[cdMaIndex+1][3] = wcdmaSignal.getEcNo() == Integer.MAX_VALUE ? "-" : wcdmaSignal.getEcNo() + "";
                    }
                    else{
                        wcdmaTable[cdMaIndex+1][3]="-";
                    }

                    wcdmaTable[cdMaIndex+1][4] = wcdmaSignal.getDbm()== Integer.MAX_VALUE ?"-" :wcdmaSignal.getDbm()+"";

                    wcdmaTable[cdMaIndex+1][5] =wcdmaSignal.getLevel() == Integer.MAX_VALUE ? "-" : wcdmaSignal.getLevel() +"";

                    if(wcdmaSignal.getLevel()!=Integer.MAX_VALUE){
                        if(wcdmaSignal.getLevel() > maxlevel){
                            maxlevel = wcdmaSignal.getLevel();
                        }
                    }

                    wcdmaTable[cdMaIndex+1][6] =wcdmaIdentity.getCid() == Integer.MAX_VALUE ? "-" : wcdmaIdentity.getCid() + "";

                    wcdmaTable[cdMaIndex+1][7] =wcdmaIdentity.getLac() == Integer.MAX_VALUE ? "-" : wcdmaIdentity.getLac() + "";

                    if (Build.VERSION.SDK_INT >= 28) {
                        wcdmaTable[cdMaIndex+1][8] = wcdmaIdentity.getMncString() + "";
                        wcdmaTable[cdMaIndex+1][9] = wcdmaIdentity.getMccString() + "";
                    } else {
                        wcdmaTable[cdMaIndex+1][9] = wcdmaIdentity.getMnc() + "";
                        wcdmaTable[cdMaIndex+1][9] = wcdmaIdentity.getMcc() + "";
                    }

                }//for Loop

                CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                cellMap = new HashMap<String,String[][]>();
                cellMap.put(key,wcdmaTable);
                model.setCellType(key);
                model.setCellTypeName("3G Measurements");
                model.setRows(rowSize);
                model.setColumns(columnSize);
                model.setCellMap(cellMap);
                if(wCdmaCells.size()==0){
                    model.setGroupEmpty(true);
                }else{
                    model.setGroupEmpty(false);
                }
               // lstDisplay.add(model);
                model.setMaxlevel(maxlevel);
                maxlevel=0;
                cellDisplayAdapter.insertCellMeasure(model);
               if(!wCdmaCells.isEmpty()){
                   wCdmaCells.clear();
               }
            }

            //2G Grouping
            if (gsmCells != null ) {

                int maxlevel=0;
                String[] gmsParams = new String[]{"ARFCN", "BSIC", "CID", "DBM", "LEVEL",
                        "LAC", "MNC", "MCC", "ASU", "RSSI","TIMINGADV"};
                String key="G_N";
                int rowSize = gsmCells.size()+1;
                int columnSize = gmsParams.length;
                String[][] gsmTable = new String[rowSize][columnSize];
                for(int i =0;i<columnSize;i++){
                    gsmTable[0][i]  = gmsParams[i];
                }//i

                for(int gsmIndex = 0; gsmIndex<gsmCells.size();gsmIndex++){
                    final CellSignalStrengthGsm gsmSignal = ((CellInfoGsm) gsmCells.get(gsmIndex)).getCellSignalStrength();
                    final CellIdentityGsm identityGsm = ((CellInfoGsm) gsmCells.get(gsmIndex)).getCellIdentity();

                    if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        gsmTable[gsmIndex +1][0] = identityGsm.getArfcn() == Integer.MAX_VALUE ? "-" : identityGsm.getArfcn() +"";
                        gsmTable[gsmIndex +1][1] = identityGsm.getBsic() == Integer.MAX_VALUE ? "-" :identityGsm.getBsic() + "";
                    }
                    else{
                        gsmTable[gsmIndex +1][0] = "-";
                        gsmTable[gsmIndex +1][1] = "-";
                    }
                    gsmTable[gsmIndex +1][2] = identityGsm.getCid() == Integer.MAX_VALUE ? "-" : identityGsm.getCid()+"";

                    gsmTable[gsmIndex +1][3] = gsmSignal.getDbm() == Integer.MAX_VALUE ? "-" : gsmSignal.getDbm()+"";

                    gsmTable[gsmIndex +1][4] = gsmSignal.getLevel() == Integer.MAX_VALUE ? "-" : gsmSignal.getLevel() +"";

                    if(gsmSignal.getLevel()!=Integer.MAX_VALUE){

                        if(gsmSignal.getLevel()>maxlevel){
                            maxlevel=gsmSignal.getLevel();
                        }
                    }

                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {

                        gsmTable[gsmIndex +1][2] = gsmSignal.getTimingAdvance() + "";
                    }
                    gsmTable[gsmIndex +1][5] = identityGsm.getLac() == Integer.MAX_VALUE ? "-" : identityGsm.getLac()+"";
                    if(Build.VERSION.SDK_INT >=28) {
                        gsmTable[gsmIndex +1][6] = identityGsm.getMncString() == null ? "-" : identityGsm.getMncString();
                    }
                    else{
                        gsmTable[gsmIndex +1][6] = identityGsm.getMnc()==Integer.MAX_VALUE  ? "-" :  identityGsm.getMnc()+"";
                    }
                    if(Build.VERSION.SDK_INT >=28) {
                        gsmTable[gsmIndex +1][7] = identityGsm.getMccString() == null ? "-" : identityGsm.getMccString();
                    }
                    else{
                        gsmTable[gsmIndex +1][7] = identityGsm.getMcc()==Integer.MAX_VALUE  ? "-" :  identityGsm.getMcc()+"";
                    }
                    gsmTable[gsmIndex +1][8] = gsmSignal.getAsuLevel()==Integer.MAX_VALUE ? "-" : gsmSignal.getAsuLevel()+"";
                    if(Build.VERSION.SDK_INT >=30) {
                        gsmTable[gsmIndex +1][9] = gsmSignal.getRssi() == Integer.MAX_VALUE ? "-" : gsmSignal.getRssi() + "";
                    }
                    else{
                        gsmTable[gsmIndex +1][10] = "-";
                    }
                    if(Build.VERSION.SDK_INT >=26) {
                        gsmTable[gsmIndex +1][10] = gsmSignal.getTimingAdvance() == Integer.MAX_VALUE ? "-" : gsmSignal.getTimingAdvance() + "";
                    }
                    else{
                        gsmTable[gsmIndex +1][10] = "-";
                    }
                }//Iterate through gsm cells

                CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                cellMap = new HashMap<String,String[][]>();
                cellMap.put(key,gsmTable);
                model.setCellType(key);
                model.setCellTypeName("2G Measurements");
                model.setRows(rowSize);
                model.setColumns(columnSize);
                model.setCellMap(cellMap);
                if(gsmCells.size() ==0){
                   model.setGroupEmpty(true);
                 }
                else{
                    model.setGroupEmpty(false);
                }
               // lstDisplay.add(model);
                model.setMaxlevel(maxlevel);
                maxlevel=0;
                cellDisplayAdapter.insertCellMeasure(model);
                if(!gsmCells.isEmpty()){
                    gsmCells.clear();;
                }
            }//gsmFor

        }//Try
        catch(Exception ex){

            String s = ex.getMessage();
        }
  }



  private void refreshThreadProc(){

          try {
              if(isOnRefresh){
                  return;
              }
              getWifi_Battery_SimParams();
              List<CellInfo> cellInfos = tm.getAllCellInfo();
              List<CellInfo> primaryCells = collectPrimaryServingCell(cellInfos);
              refreshMeasuresForPrimaryServingCell(primaryCells);
              collectNeighbouringCells(cellInfos);
              refreshMeasuresofNeighbouringCellsOrderd();
              isOnRefresh=false;


          }
          catch (SecurityException ex){

              isOnRefresh=false;
          }

  }//refreshThreadProc

    private void refreshMeasuresForPrimaryServingCell(List<CellInfo> primaryCells) {

        try {
            if(primaryCells!=null) {
              //  isOnRefresh = true;
                for (int i = 0; i < primaryCells.size(); i++) {
                    CellInfo info = primaryCells.get(i);
                    if (info != null) {

                        if (info instanceof CellInfoGsm) { //GSM

                            int maxlevel =0;
                            // ARFCN, BSIC,CID,DBM,level and the rest.
                            String[] servCellParms = new String[]{"ARFCN", "BSIC", "CID", "DBM", "LEVEL",
                                    "LAC", "MNC", "MCC", "ASU", "RSSI", "TIMINGADV"};

                            int rowSize = 2;
                            int columnSize = servCellParms.length;
                            String key = "G_S";
                            String[][] gsmTable = new String[rowSize][columnSize];
                            for (int k = 0; k < columnSize; k++) {
                                gsmTable[0][k] = servCellParms[k];
                            }//k

                            final CellSignalStrengthGsm gsmSignal = ((CellInfoGsm) info).getCellSignalStrength();
                            final CellIdentityGsm identityGsm = ((CellInfoGsm) info).getCellIdentity();

                            if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                                gsmTable[1][0] = identityGsm.getArfcn() == Integer.MAX_VALUE ? "-" : identityGsm.getArfcn() + "";
                                gsmTable[1][1] = identityGsm.getBsic() == Integer.MAX_VALUE ? "-" : identityGsm.getBsic() + "";
                            } else {
                                gsmTable[1][0] = "-";
                                gsmTable[1][1] = "-";
                            }
                            gsmTable[1][2] = identityGsm.getCid() == Integer.MAX_VALUE ? "-" : identityGsm.getCid() + "";

                            gsmTable[1][3] = gsmSignal.getDbm() == Integer.MAX_VALUE ? "-" : gsmSignal.getDbm() + "";

                            gsmTable[1][4] = gsmSignal.getLevel() == Integer.MAX_VALUE ? "-" : gsmSignal.getLevel() + "";

                            if(gsmSignal.getLevel()!=Integer.MAX_VALUE){
                                if(gsmSignal.getLevel() >maxlevel){
                                    maxlevel = gsmSignal.getLevel();
                                }
                            }


                            gsmTable[1][5] = identityGsm.getLac() == Integer.MAX_VALUE ? "-" : identityGsm.getLac() + "";
                            if (Build.VERSION.SDK_INT >= 28) {
                                gsmTable[1][6] = identityGsm.getMncString() == null ? "-" : identityGsm.getMncString();
                            } else {
                                gsmTable[1][6] = identityGsm.getMnc() == Integer.MAX_VALUE ? "-" : identityGsm.getMnc() + "";
                            }
                            if (Build.VERSION.SDK_INT >= 28) {
                                gsmTable[1][7] = identityGsm.getMccString() == null ? "-" : identityGsm.getMccString();
                            } else {
                                gsmTable[1][7] = identityGsm.getMcc() == Integer.MAX_VALUE ? "-" : identityGsm.getMcc() + "";
                            }
                            gsmTable[1][8] = gsmSignal.getAsuLevel() == Integer.MAX_VALUE ? "-" : gsmSignal.getAsuLevel() + "";
                            if (Build.VERSION.SDK_INT >= 30) {
                                gsmTable[1][9] = gsmSignal.getRssi() == Integer.MAX_VALUE ? "-" : gsmSignal.getRssi() + "";
                            } else {
                                gsmTable[1][10] = "-";
                            }
                            if (Build.VERSION.SDK_INT >= 26) {
                                gsmTable[1][10] = gsmSignal.getTimingAdvance() == Integer.MAX_VALUE ? "-" : gsmSignal.getTimingAdvance() + "";
                            } else {
                                gsmTable[1][10] = "-";
                            }
                            CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                            cellMap = new HashMap<String, String[][]>();
                            cellMap.put(key, gsmTable);
                            model.setCellType("G_S");
                            model.setCellTypeName("Primary Serving Cell");
                            model.setRows(rowSize);
                            model.setColumns(columnSize);
                            model.setCellMap(cellMap);
                            model.setMaxlevel(maxlevel);

                            if(lstDisplay.size()>0) {
                                cellDisplayAdapter.updateCellMeasure(model);
                            }


                        }//if
                        else if (info instanceof CellInfoLte) {


                            //LTE
                            // EARFCN, PCI, RSRP, RSRQ, ASU,dBM,Level,cqi,rssnr,timingadv,MNC,MCC,CI,tac, bandwidth.
                            int maxlevel=0;
                            String[] servCellParms =new String[]{"EARFCN", "PCI", "RSRP", "RSRQ", "ASU", "DBM",
                                    "LEVEL", "CQI", "RSSNR", "TIMINGADV", "MNC", "MCC", "CI", "TAC", "BANDWIDTH"};

                            int rowSize = 2;
                            int columnSize = servCellParms.length;
                            String key = "L_S";
                            String[][] lteTable = new String[rowSize][columnSize];
                            for (int j = 0; j < columnSize; j++) {
                                lteTable[0][j] = servCellParms[j];
                            }//k
                            final CellSignalStrengthLte lteSignal = ((CellInfoLte) info).getCellSignalStrength();
                            final CellIdentityLte lteIdentity = ((CellInfoLte) info).getCellIdentity();

                            if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {

                                lteTable[1][0] = lteIdentity.getEarfcn() == Integer.MAX_VALUE ? "-" : lteIdentity.getEarfcn() + "";
                            } else {
                                lteTable[1][0] = "-"; //1
                            }

                            //2
                            lteTable[1][1] = lteIdentity.getPci() == Integer.MAX_VALUE ? "-" : lteIdentity.getPci() + "";

                            //3
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                                lteTable[1][2] = lteSignal.getRsrp() == Integer.MAX_VALUE ? "-" : lteSignal.getRsrp() + ""; //3
                                lteTable[1][3] = lteSignal.getRsrq() == Integer.MAX_VALUE ? "-" : lteSignal.getRsrq() + ""; //4
                            } else {
                                lteTable[1][2] = "-";
                                lteTable[1][3] = "-";

                            }

                            lteTable[1][4] = lteSignal.getAsuLevel() == Integer.MAX_VALUE ? "-" : lteSignal.getAsuLevel() + ""; // 5 ASU
                            lteTable[1][5] = lteSignal.getDbm() == Integer.MAX_VALUE ? "-" : lteSignal.getDbm() + ""; // 6 DBM

                            lteTable[1][6] = lteSignal.getLevel() == Integer.MAX_VALUE ? "-" : lteSignal.getLevel() + ""; //7 LEVEL

                            if(lteSignal.getLevel()!=Integer.MAX_VALUE){
                                if(lteSignal.getLevel() >maxlevel){
                                    maxlevel = lteSignal.getLevel();
                                }
                            }

                            if (Build.VERSION.SDK_INT >= 28) {
                                lteTable[1][7] = lteSignal.getCqi() == Integer.MAX_VALUE ? "-" : lteSignal.getCqi() + ""; //8 CQI
                            } else {
                                lteTable[1][7] = "-"; //8 CQI
                            }

                            if (Build.VERSION.SDK_INT >= 28) {
                                lteTable[1][8] = lteSignal.getRssnr() == Integer.MAX_VALUE ? "-" : lteSignal.getRssnr() + ""; //8
                            } else {
                                lteTable[1][8] = "-"; //8
                            }

                            lteTable[1][9] = lteSignal.getTimingAdvance() ==
                                    Integer.MAX_VALUE ? "-" : lteSignal.getTimingAdvance() + ""; //11 TIMING
                            if (Build.VERSION.SDK_INT >= 28) {
                                lteTable[1][10] = lteIdentity.getMncString() == null ? "-" : lteIdentity.getMncString() + ""; //11 MNC
                                lteTable[1][11] = lteIdentity.getMccString() == null ? "-" : lteIdentity.getMccString() + ""; //12//MCC

                            } else {

                                lteTable[1][10] = lteIdentity.getMnc() == Integer.MAX_VALUE ? "-" : lteIdentity.getMnc() + ""; //11 MNC
                                lteTable[1][11] = lteIdentity.getMnc() == Integer.MAX_VALUE ? "-" : lteIdentity.getMnc() + ""; //12//MCC
                            }

                            lteTable[1][12] = lteIdentity.getCi() == Integer.MAX_VALUE ? "-" : lteIdentity.getCi() + ""; //13 CI
                            lteTable[1][13] = lteIdentity.getTac() == Integer.MAX_VALUE ? "-" : lteIdentity.getTac() + ""; //14 TAC
                            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                                lteTable[1][14] = lteIdentity.getBandwidth() == Integer.MAX_VALUE ? "-" :
                                        lteIdentity.getBandwidth() + ""; //15 BANDWIDTH
                            } else {
                                lteTable[1][14] = "-";
                            }

                            CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                            cellMap = new HashMap<String, String[][]>();
                            cellMap.put(key, lteTable);
                            model.setCellType("L_S");
                            model.setCellTypeName("Primary Serving Cell");
                            model.setRows(rowSize);
                            model.setColumns(columnSize);
                            model.setCellMap(cellMap);
                            model.setMaxlevel(maxlevel);

                            // lstDisplay.add(model);
                           // cellDisplayAdapter.insertCellMeasure(model);
                            if(lstDisplay.size()>0) {
                                cellDisplayAdapter.updateCellMeasure(model);
                            }


                        } else if (info instanceof CellInfoWcdma) {

                            //3G
                            int maxlevel =0;
                            //    RSCP = in asu
                            String[] servCellParms = new String[]{"UARFCN", "PSC", "ASU", "ECNO",
                                    "DBM", "LEVEL", "CID", "LAC", "MNC", "MCC"};
                            int rowSize = 2;
                            int columnSize = servCellParms.length;
                            String key = "W_S";
                            String[][] wcdmaTable = new String[rowSize][columnSize];
                            for (int j = 0; j < columnSize; j++) {
                                wcdmaTable[0][j] = servCellParms[j];
                            }//k
                            final CellSignalStrengthWcdma wcdmaSignal = ((CellInfoWcdma) info).getCellSignalStrength();
                            final CellIdentityWcdma wcdmaIdentity = ((CellInfoWcdma) info).getCellIdentity();

                            if (Build.VERSION.SDK_INT >= 24) {
                                wcdmaTable[1][0] = wcdmaIdentity.getUarfcn() == Integer.MAX_VALUE ? "-" : wcdmaIdentity.getUarfcn() + "";
                            } else {
                                wcdmaTable[1][0] = "-";
                            }
                            wcdmaTable[1][1] = wcdmaIdentity.getPsc() == Integer.MAX_VALUE ? "-" : wcdmaIdentity.getPsc() + "";
                            wcdmaTable[1][2] = wcdmaSignal.getAsuLevel() == Integer.MAX_VALUE ? "-" : wcdmaSignal.getAsuLevel() + "";
                            // RSCP in asu
                            if (Build.VERSION.SDK_INT >= 30) {
                                wcdmaTable[1][3] = wcdmaSignal.getEcNo() == Integer.MAX_VALUE ? "-" : wcdmaSignal.getEcNo() + "";
                            } else {
                                wcdmaTable[1][3] = "-";
                            }

                            wcdmaTable[1][4] = wcdmaSignal.getDbm() == Integer.MAX_VALUE ? "-" : wcdmaSignal.getDbm() + "";

                            wcdmaTable[1][5] = wcdmaSignal.getLevel() == Integer.MAX_VALUE ? "-" : wcdmaSignal.getLevel() + "";

                           if(wcdmaSignal.getLevel()!=Integer.MAX_VALUE){
                               if(wcdmaSignal.getLevel()>maxlevel){
                                   maxlevel=wcdmaSignal.getLevel();
                               }
                           }

                            wcdmaTable[1][6] = wcdmaIdentity.getCid() == Integer.MAX_VALUE ? "-" : wcdmaIdentity.getCid() + "";

                            wcdmaTable[1][7] = wcdmaIdentity.getLac() == Integer.MAX_VALUE ? "-" : wcdmaIdentity.getLac() + "";

                            if (Build.VERSION.SDK_INT >= 28) {
                                wcdmaTable[1][8] = wcdmaIdentity.getMncString() + "";
                                wcdmaTable[1][9] = wcdmaIdentity.getMccString() + "";
                            } else {
                                wcdmaTable[1][8] = wcdmaIdentity.getMnc() + "";
                                wcdmaTable[1][9] = wcdmaIdentity.getMcc() + "";
                            }
                            CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                            cellMap = new HashMap<String, String[][]>();
                            cellMap.put(key, wcdmaTable);
                            model.setCellType("W_S");
                            model.setCellTypeName("Primary Serving Cell");
                            model.setRows(rowSize);
                            model.setColumns(columnSize);
                            model.setCellMap(cellMap);
                            model.setMaxlevel(maxlevel);
                            //lstDisplay.add(model);
                           // cellDisplayAdapter.insertCellMeasure(model);
                            if(lstDisplay.size()>0) {
                                cellDisplayAdapter.updateCellMeasure(model);
                            }


                        }//cdma 3G
                        else if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

                            if (info instanceof CellInfoNr) {

                                int maxlevel =0;
                                String[] servCellParms = new String[]  {"NRARFCN", "PCI", "NCI", "DBM", "LEVEL", "CSIRSRP",
                                        "CSISINR", "SSRSRP", "SSRSRQ", "SSSINR", "MNC", "MCC"};

                                int rowSize = 2;
                                int columnSize = servCellParms.length;
                                String key = "5_S";
                                String[][] fiveGTable = new String[rowSize][columnSize];
                                for (int j = 0; j < columnSize; j++) {
                                    fiveGTable[0][j] = servCellParms[j];
                                }//k

                                final CellIdentityNr fiveGIdentity = (CellIdentityNr) ((CellInfoNr) info).getCellIdentity();
                                final CellSignalStrengthNr fiveGISignal = (CellSignalStrengthNr) ((CellInfoNr) info).getCellSignalStrength();

                                fiveGTable[1][0] = fiveGIdentity.getNrarfcn()==Integer.MAX_VALUE ? "-":
                                        fiveGIdentity.getNrarfcn() + "";

                                fiveGTable[1][1] = fiveGIdentity.getPci()==Integer.MAX_VALUE ? "-":
                                        fiveGIdentity.getPci() + "";

                                fiveGTable[1][2] = fiveGIdentity.getNci()==Integer.MAX_VALUE ? "-":
                                        fiveGIdentity.getNci() + "";

                                fiveGTable[1][3] = fiveGISignal.getDbm()==Integer.MAX_VALUE ? "-":
                                        fiveGISignal.getDbm() + "";

                                if(fiveGISignal.getLevel()!=Integer.MAX_VALUE){
                                    fiveGTable[1][4] = fiveGISignal.getLevel()+"";
                                    if(fiveGISignal.getLevel() > maxlevel){
                                        maxlevel = fiveGISignal.getLevel();
                                    }
                                }
                                else{
                                    fiveGTable[1][4] = "-";
                                }

                                fiveGTable[1][5] = fiveGISignal.getCsiRsrp()==Integer.MAX_VALUE ? "-" :  fiveGISignal.getCsiRsrp()+"";

                                fiveGTable[1][6] = fiveGISignal.getCsiSinr()== Integer.MAX_VALUE ?"-" : fiveGISignal.getCsiSinr()+"";

                                fiveGTable[1][7] = fiveGISignal.getSsRsrp() ==Integer.MAX_VALUE ?"-" :fiveGISignal.getSsRsrp()+"";
                                fiveGTable[1][8] = fiveGISignal.getSsRsrq() ==Integer.MAX_VALUE ?"-" :fiveGISignal.getSsRsrq()+"";
                                fiveGTable[1][9] = fiveGISignal.getSsSinr() ==Integer.MAX_VALUE?"-" :fiveGISignal.getSsSinr()+"";
                                fiveGTable[1][10] = fiveGIdentity.getMncString() ==null?"-" :fiveGIdentity.getMncString();
                                fiveGTable[1][11] = fiveGIdentity.getMccString() ==null?"-" :fiveGIdentity.getMccString();

                                CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                                cellMap = new HashMap<String, String[][]>();
                                cellMap.put(key, fiveGTable);
                                model.setCellType("5_S");
                                model.setCellTypeName("Primary Serving Cell");
                                model.setRows(rowSize);
                                model.setColumns(columnSize);
                                model.setCellMap(cellMap);
                                model.setMaxlevel(maxlevel);
                                // lstDisplay.add(model);
                               // cellDisplayAdapter.insertCellMeasure(model);
                                if(lstDisplay.size()>0) {
                                    cellDisplayAdapter.updateCellMeasure(model);
                                }

                            }
                        }

                    }//if null
                }//for
            }
            else if(primaryCells.size()==0){

                //Jus for not refreshing if
                if(lstDisplay.size()>0) {
                    CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                    cellMap = new HashMap<String, String[][]>();
                    model.setCellType("G_S");
                    model.setCellTypeName("Primary Serving Cell");
                    model.setCellMap(cellMap);
                    model.setGroupEmpty(true);
                    if(lstDisplay.size()>0) {
                        cellDisplayAdapter.updateCellMeasure(model);
                    }
                }
            }

           // isOnRefresh = false;

        } catch (SecurityException ex) {

        } catch (Exception ex) {

        }
    }

    private void refreshMeasuresofNeighbouringCellsOrderd(){

        //5G 4G 3G 2G
        try{
            //5G
            if (cellInfoNrs != null ) {

                int maxlevel =0;
                String[] FivGParams = new String[]{"NRARFCN", "PCI", "NCI", "DBM", "LEVEL", "CSIRSRP",
                        "CSISINR", "SSRSRP", "SSRSRQ", "SSSINR", "MNC", "MCC"};
                int rowSize = cellInfoNrs.size() + 1;
                int columnSize = 6;
                String key = "5_N";
                String[][] fiveGTable = new String[rowSize][columnSize];
                for (int i = 0; i < columnSize; i++) {
                    fiveGTable[0][i] = FivGParams[i];
                }//i
                for (int fiveGIndex = 0; fiveGIndex < cellInfoNrs.size(); fiveGIndex++) {
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        final CellIdentityNr fiveGIdentity =
                                (CellIdentityNr) ((CellInfoNr) cellInfoNrs.get(fiveGIndex)).getCellIdentity();
                        final CellSignalStrengthNr fiveGISignal =
                                (CellSignalStrengthNr) ((CellInfoNr) cellInfoNrs.get(fiveGIndex)).getCellSignalStrength();

                        if(fiveGIdentity.getNrarfcn()!=Integer.MAX_VALUE) {
                            fiveGTable[fiveGIndex + 1][0] = fiveGIdentity.getNrarfcn() + "";
                        }
                        else{
                            fiveGTable[fiveGIndex + 1][0] ="-";
                        }

                        if(fiveGIdentity.getPci()!=Integer.MAX_VALUE) {
                            fiveGTable[fiveGIndex + 1][1] = fiveGIdentity.getPci() + "";
                        }
                        else{
                            fiveGTable[fiveGIndex + 1][1] = "-";
                        }
                        if(fiveGIdentity.getNci()!=Integer.MAX_VALUE) {
                            fiveGTable[fiveGIndex + 1][2] = fiveGIdentity.getNci() + "";
                        }
                        else{
                            fiveGTable[fiveGIndex + 1][2] ="-";
                        }
                        if(fiveGISignal.getDbm()!=Integer.MAX_VALUE){
                            fiveGTable[fiveGIndex + 1][3] = fiveGISignal.getDbm() + "";
                        }
                        else{
                            fiveGTable[fiveGIndex + 1][3] = "-";
                        }
                        if( fiveGISignal.getLevel()!=Integer.MAX_VALUE){

                            if(fiveGISignal.getLevel()> maxlevel){
                                maxlevel = fiveGISignal.getLevel();
                            }
                            fiveGTable[fiveGIndex + 1][4] = fiveGISignal.getLevel()+"";
                        }
                        else{
                            fiveGTable[fiveGIndex + 1][4] = "-";
                        }

                        if( fiveGISignal.getCsiRsrp()!=Integer.MAX_VALUE){
                            fiveGTable[fiveGIndex + 1][5] = fiveGISignal.getCsiRsrp() + "";
                        }
                        else{
                            fiveGTable[fiveGIndex + 1][5] = "-";
                        }
                        if(fiveGISignal.getCsiSinr()!=Integer.MAX_VALUE){
                            fiveGTable[fiveGIndex + 1][6] = fiveGISignal.getCsiSinr() + "";
                        }
                        else{
                            fiveGTable[fiveGIndex + 1][6] ="-";
                        }
                        if(fiveGISignal.getSsRsrp()!=Integer.MAX_VALUE){
                            fiveGTable[fiveGIndex + 1][7] = fiveGISignal.getSsRsrp() + "";
                        }
                        else{
                            fiveGTable[fiveGIndex + 1][7] = "-";
                        }

                        if(fiveGISignal.getSsRsrq()!=Integer.MAX_VALUE){
                            fiveGTable[fiveGIndex + 1][8] = fiveGISignal.getSsRsrq() + "";
                        }
                        else{
                            fiveGTable[fiveGIndex + 1][8] = "-";
                        }
                        if(fiveGISignal.getSsSinr() !=Integer.MAX_VALUE){
                            fiveGTable[fiveGIndex + 1][9] = fiveGISignal.getSsSinr() + "";
                        }
                        else{
                            fiveGTable[fiveGIndex + 1][9] = "-";
                        }
                        if(fiveGIdentity.getMncString()!=null){
                            fiveGTable[fiveGIndex + 1][10] = fiveGIdentity.getMncString() + "";
                        }
                        else{
                            fiveGTable[fiveGIndex + 1][10] = "-";
                        }
                        if(fiveGIdentity.getMccString()!=null){
                            fiveGTable[fiveGIndex + 1][11] = fiveGIdentity.getMccString() + "";
                        }else{
                            fiveGTable[fiveGIndex + 1][11] = "-";
                        }

                    }

                }//for loop

                CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                cellMap = new HashMap<String, String[][]>();
                cellMap.put(key, fiveGTable);
                model.setCellType("5_N");
                model.setCellTypeName("5G Measurements");
                model.setRows(rowSize);
                model.setColumns(columnSize);
                model.setCellMap(cellMap);
                model.setMaxlevel(maxlevel);
                if(cellInfoNrs.size()==0){
                    model.setGroupEmpty(true);
                }
                else{
                    model.setGroupEmpty(false);
                }
                //lstDisplay.add(model);
                if(lstDisplay.size()>0) {
                    cellDisplayAdapter.updateCellMeasure(model);
                }
                if(!cellInfoNrs.isEmpty()){
                    cellInfoNrs.clear();
                }
            }


            //4G Grouping
            if(lteCells!=null ){

                int maxlevel =0;

                String[] lteParams =new String[] {"EARFCN", "PCI", "RSRP", "RSRQ", "ASU", "DBM",
                        "LEVEL", "CQI", "RSSNR", "TIMINGADV", "MNC", "MCC", "CI", "TAC", "BANDWIDTH"};

                //LTE
                int rowSize = lteCells.size()+1;
                int columnSize = lteParams.length;
                String key = "L_N";
                String[][] lteTable = new String[rowSize][columnSize];
                for(int i =0;i<columnSize;i++){
                    lteTable[0][i]  = lteParams[i];
                }//i
                for(int lteIndex =0;lteIndex<lteCells.size();lteIndex++) {

                    final CellSignalStrengthLte lteSignal = ((CellInfoLte) lteCells.get(lteIndex)).getCellSignalStrength();
                    final CellIdentityLte lteIdentity = ((CellInfoLte) lteCells.get(lteIndex)).getCellIdentity();


                    if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {

                        lteTable[lteIndex + 1][0] = lteIdentity.getEarfcn()==Integer.MAX_VALUE?"-":lteIdentity.getEarfcn() + "";
                    }
                    else{
                        lteTable[lteIndex + 1][0] = "-"; //1
                    }

                    //2
                    lteTable[lteIndex + 1][1] =   lteIdentity.getPci()==Integer.MAX_VALUE ?"-"  :   lteIdentity.getPci() + "";

                    //3
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {

                        lteTable[lteIndex + 1][2] =  lteSignal.getRsrp() == Integer.MAX_VALUE ?"-":  lteSignal.getRsrp() + ""; //3
                        lteTable[lteIndex + 1][3] = lteSignal.getRsrq()==Integer.MAX_VALUE ? "-" : lteSignal.getRsrq() + ""; //4
                    }
                    else{
                        lteTable[lteIndex + 1][2] = "-";
                        lteTable[lteIndex + 1][3] =  "-";

                    }

                    lteTable[lteIndex + 1][4] =  lteSignal.getAsuLevel()==Integer.MAX_VALUE ?"-" : lteSignal.getAsuLevel() + ""; // 5 ASU
                    lteTable[lteIndex + 1][5] = lteSignal.getDbm()==Integer.MAX_VALUE ?"-": lteSignal.getDbm() + ""; // 6 DBM

                    lteTable[lteIndex + 1][6] = lteSignal.getLevel()==Integer.MAX_VALUE ?"-" : lteSignal.getLevel() + ""; //7 LEVEL

                    if(lteSignal.getLevel()!=Integer.MAX_VALUE){
                        if(lteSignal.getLevel() > maxlevel){
                            maxlevel = lteSignal.getLevel();
                        }
                    }

                    if(Build.VERSION.SDK_INT >= 28) {
                        lteTable[lteIndex + 1][7] = lteSignal.getCqi()==Integer.MAX_VALUE ? "-" : lteSignal.getCqi() + ""; //8 CQI
                    }
                    else{
                        lteTable[lteIndex + 1][7] = "-"; //8 CQI
                    }

                    if(Build.VERSION.SDK_INT >= 28) {
                        lteTable[lteIndex + 1][8] =  lteSignal.getRssnr()==Integer.MAX_VALUE ?"-" :lteSignal.getRssnr()+""; //8
                    }
                    else{
                        lteTable[lteIndex + 1][8] = "-"; //8
                    }

                    lteTable[lteIndex + 1][9] = lteSignal.getTimingAdvance()==
                            Integer.MAX_VALUE ?"-" : lteSignal.getTimingAdvance() + ""; //11 TIMING
                    if (Build.VERSION.SDK_INT >= 28) {
                        lteTable[lteIndex + 1][10] = lteIdentity.getMncString()==null ?"-" : lteIdentity.getMncString() + ""; //11 MNC
                        lteTable[lteIndex + 1][11] = lteIdentity.getMccString() ==null ?"-" :lteIdentity.getMccString()+""; //12//MCC

                    }
                    else{

                        lteTable[lteIndex + 1][10] = lteIdentity.getMnc()==Integer.MAX_VALUE ?"-": lteIdentity.getMnc() + ""; //11 MNC
                        lteTable[lteIndex + 1][11] = lteIdentity.getMnc()==Integer.MAX_VALUE ?"-": lteIdentity.getMnc()+""; //12//MCC
                    }

                    lteTable[lteIndex + 1][12] = lteIdentity.getCi()==Integer.MAX_VALUE ?"-" :lteIdentity.getCi()+ ""; //13 CI
                    lteTable[lteIndex + 1][13] = lteIdentity.getTac()==Integer.MAX_VALUE ? "-" : lteIdentity.getTac()+"" ; //14 TAC
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        lteTable[lteIndex + 1][14] = lteIdentity.getBandwidth()==Integer.MAX_VALUE ? "-" :
                                lteIdentity.getBandwidth() + ""; //15 BANDWIDTH
                    }
                    else{
                        lteTable[lteIndex + 1][14] = "-";
                    }


                }//for loop

                CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                for(int i=0;i<rowSize;i++){
                    for(int j=0;j<columnSize;j++){

                        System.out.print(lteTable[i][j]+"   ");
                    }
                    System.out.println("\n");
                }
                cellMap = new HashMap<String,String[][]>();
                cellMap.put(key,lteTable);
                model.setCellType(key);
                model.setCellTypeName("4G Measurements");
                model.setRows(rowSize);
                model.setColumns(columnSize);
                model.setCellMap(cellMap);
                model.setMaxlevel(maxlevel);
                if(lteCells.size() ==0){
                    model.setGroupEmpty(true);
                }
                else{
                    model.setGroupEmpty(false);
                }
                // lstDisplay.add(model);
                if(lstDisplay.size()>0) {
                    cellDisplayAdapter.updateCellMeasure(model);
                }
                if(!lteCells.isEmpty()) {
                    lteCells.clear();
                }
            }
            //3G Grouping
            if(wCdmaCells!=null ){
                // String[] wCdmaParams = new String[]{"CID","LAC","MCC","MNC", "PSC","UARFCN"};
                int maxlevel =0;
                String[] wCdmaParams =new String[] {"UARFCN", "PSC", "ASU", "ECNO", "DBM","LEVEL","CID","LAC","MNC","MCC"};
                int rowSize = wCdmaCells.size()+1;
                int columnSize =wCdmaParams.length;
                String key = "W_N";
                String[][] wcdmaTable = new String[rowSize][columnSize];
                for(int i =0;i<columnSize;i++){
                    wcdmaTable[0][i]  = wCdmaParams[i];
                }//i

                for(int cdMaIndex =0;cdMaIndex<wCdmaCells.size();cdMaIndex++){

                    final CellSignalStrengthWcdma wcdmaSignal = ((CellInfoWcdma) wCdmaCells.get(cdMaIndex)).getCellSignalStrength();
                    final CellIdentityWcdma wcdmaIdentity = ((CellInfoWcdma)  wCdmaCells.get(cdMaIndex)).getCellIdentity();


                    if (Build.VERSION.SDK_INT >= 24) {
                        wcdmaTable[cdMaIndex+1][0] = wcdmaIdentity.getUarfcn() == Integer.MAX_VALUE ? "-" : wcdmaIdentity.getUarfcn() + "";
                    }
                    else{
                        wcdmaTable[cdMaIndex+1][0]="-";
                    }
                    wcdmaTable[cdMaIndex+1][1] =wcdmaIdentity.getPsc()==Integer.MAX_VALUE ? "-" : wcdmaIdentity.getPsc()+"";
                    wcdmaTable[cdMaIndex+1][2] =wcdmaSignal.getAsuLevel()==Integer.MAX_VALUE ? "-" : wcdmaSignal.getAsuLevel()+"";
                    // RSCP in asu
                    if(Build.VERSION.SDK_INT >= 30){
                        wcdmaTable[cdMaIndex+1][3] = wcdmaSignal.getEcNo() == Integer.MAX_VALUE ? "-" : wcdmaSignal.getEcNo() + "";
                    }
                    else{
                        wcdmaTable[cdMaIndex+1][3]="-";
                    }

                    wcdmaTable[cdMaIndex+1][4] = wcdmaSignal.getDbm()== Integer.MAX_VALUE ?"-" :wcdmaSignal.getDbm()+"";

                    wcdmaTable[cdMaIndex+1][5] =wcdmaSignal.getLevel() == Integer.MAX_VALUE ? "-" : wcdmaSignal.getLevel() +"";

                    if(wcdmaSignal.getLevel()!=Integer.MAX_VALUE){
                        if(wcdmaSignal.getLevel() >maxlevel){
                            maxlevel=wcdmaSignal.getLevel();
                        }
                    }

                    wcdmaTable[cdMaIndex+1][6] =wcdmaIdentity.getCid() == Integer.MAX_VALUE ? "-" : wcdmaIdentity.getCid() + "";

                    wcdmaTable[cdMaIndex+1][7] =wcdmaIdentity.getLac() == Integer.MAX_VALUE ? "-" : wcdmaIdentity.getLac() + "";

                    if (Build.VERSION.SDK_INT >= 28) {
                        wcdmaTable[cdMaIndex+1][8] = wcdmaIdentity.getMncString() + "";
                        wcdmaTable[cdMaIndex+1][9] = wcdmaIdentity.getMccString() + "";
                    } else {
                        wcdmaTable[cdMaIndex+1][9] = wcdmaIdentity.getMnc() + "";
                        wcdmaTable[cdMaIndex+1][9] = wcdmaIdentity.getMcc() + "";
                    }

                }//for Loop

                CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                cellMap = new HashMap<String,String[][]>();
                cellMap.put(key,wcdmaTable);
                model.setCellType(key);
                model.setCellTypeName("3G Measurements");
                model.setRows(rowSize);
                model.setColumns(columnSize);
                model.setCellMap(cellMap);
                model.setMaxlevel(maxlevel);
                if(wCdmaCells.size()==0){
                    model.setGroupEmpty(true);
                }else{
                    model.setGroupEmpty(false);
                }
                // lstDisplay.add(model);
                if(lstDisplay.size()>0) {
                    cellDisplayAdapter.updateCellMeasure(model);
                }
                if(!wCdmaCells.isEmpty()){
                    wCdmaCells.clear();
                }
            }

            //2G Grouping
            if (gsmCells != null ) {

                int maxlevel =0;
                String[] gmsParams = new String[]{"ARFCN", "BSIC", "CID", "DBM", "LEVEL",
                        "LAC", "MNC", "MCC", "ASU", "RSSI","TIMINGADV"};
                String key="G_N";
                int rowSize = gsmCells.size()+1;
                int columnSize = gmsParams.length;
                String[][] gsmTable = new String[rowSize][columnSize];
                for(int i =0;i<columnSize;i++){
                    gsmTable[0][i]  = gmsParams[i];
                }//i

                for(int gsmIndex = 0; gsmIndex<gsmCells.size();gsmIndex++){
                    final CellSignalStrengthGsm gsmSignal = ((CellInfoGsm) gsmCells.get(gsmIndex)).getCellSignalStrength();
                    final CellIdentityGsm identityGsm = ((CellInfoGsm) gsmCells.get(gsmIndex)).getCellIdentity();

                    if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        gsmTable[gsmIndex +1][0] = identityGsm.getArfcn() == Integer.MAX_VALUE ? "-" : identityGsm.getArfcn() +"";
                        gsmTable[gsmIndex +1][1] = identityGsm.getBsic() == Integer.MAX_VALUE ? "-" :identityGsm.getBsic() + "";
                    }
                    else{
                        gsmTable[gsmIndex +1][0] = "-";
                        gsmTable[gsmIndex +1][1] = "-";
                    }
                    gsmTable[gsmIndex +1][2] = identityGsm.getCid() == Integer.MAX_VALUE ? "-" : identityGsm.getCid()+"";

                    gsmTable[gsmIndex +1][3] = gsmSignal.getDbm() == Integer.MAX_VALUE ? "-" : gsmSignal.getDbm()+"";

                    gsmTable[gsmIndex +1][4] = gsmSignal.getLevel() == Integer.MAX_VALUE ? "-" : gsmSignal.getLevel() +"";

                    if(gsmSignal.getLevel()!=Integer.MAX_VALUE){
                        if(gsmSignal.getLevel() >maxlevel){
                            maxlevel=gsmSignal.getLevel();
                        }
                    }

                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {

                        gsmTable[gsmIndex +1][2] = gsmSignal.getTimingAdvance() + "";
                    }
                    gsmTable[gsmIndex +1][5] = identityGsm.getLac() == Integer.MAX_VALUE ? "-" : identityGsm.getLac()+"";
                    if(Build.VERSION.SDK_INT >=28) {
                        gsmTable[gsmIndex +1][6] = identityGsm.getMncString() == null ? "-" : identityGsm.getMncString();
                    }
                    else{
                        gsmTable[gsmIndex +1][6] = identityGsm.getMnc()==Integer.MAX_VALUE  ? "-" :  identityGsm.getMnc()+"";
                    }
                    if(Build.VERSION.SDK_INT >=28) {
                        gsmTable[gsmIndex +1][7] = identityGsm.getMccString() == null ? "-" : identityGsm.getMccString();
                    }
                    else{
                        gsmTable[gsmIndex +1][7] = identityGsm.getMcc()==Integer.MAX_VALUE  ? "-" :  identityGsm.getMcc()+"";
                    }
                    gsmTable[gsmIndex +1][8] = gsmSignal.getAsuLevel()==Integer.MAX_VALUE ? "-" : gsmSignal.getAsuLevel()+"";
                    if(Build.VERSION.SDK_INT >=30) {
                        gsmTable[gsmIndex +1][9] = gsmSignal.getRssi() == Integer.MAX_VALUE ? "-" : gsmSignal.getRssi() + "";
                    }
                    else{
                        gsmTable[gsmIndex +1][10] = "-";
                    }
                    if(Build.VERSION.SDK_INT >=26) {
                        gsmTable[gsmIndex +1][10] = gsmSignal.getTimingAdvance() == Integer.MAX_VALUE ? "-" : gsmSignal.getTimingAdvance() + "";
                    }
                    else{
                        gsmTable[gsmIndex +1][10] = "-";
                    }
                }//Iterate through gsm cells

                CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                cellMap = new HashMap<String,String[][]>();
                cellMap.put(key,gsmTable);
                model.setCellType(key);
                model.setCellTypeName("2G Measurements");
                model.setRows(rowSize);
                model.setColumns(columnSize);
                model.setCellMap(cellMap);
                model.setMaxlevel(maxlevel);
                if(gsmCells.size() ==0){
                    model.setGroupEmpty(true);
                }
                else{
                    model.setGroupEmpty(false);
                }
                // lstDisplay.add(model);
                if(lstDisplay.size()>0) {
                    cellDisplayAdapter.updateCellMeasure(model);
                }
                if(!gsmCells.isEmpty()){
                    gsmCells.clear();;
                }
            }//gsmFor

        }//Try
        catch(Exception ex){

            String s = ex.getMessage();
        }
    }

    private void startRefreshThread(){


        handler = new Handler(Looper.myLooper());

        // Define the code block to be executed
        runnableCode = new Runnable() {
            @Override
            public void run() {

                // Do something here on the main threa
                threadStart=true;

                refreshThreadProc();

                Log.d("Handlers", "Called on main thread");
                // Repeat this the same runnable code block again another 2 seconds
                // 'this' is referencing the Runnable object

                handler.postDelayed(this, 2000);

            }
        };

        // Start the initial runnable task by posting through the handler
        handler.post(runnableCode);
       /*try {

           long delay = 2000; //the delay between the termination of one execution and the commencement of the next
           threadExec.scheduleWithFixedDelay(new RefreshTread(), 0, delay, TimeUnit.MILLISECONDS);
       }
       catch(Exception ex){

       }*/



    }


    //ScheduledThreadPoolExecuter

    class RefreshTread implements Runnable {

        @Override
        public void run() {

            refreshThreadProc();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
       // rcvServingCells.refreshDrawableState();
        counter++;
        if(counter > 1){
            if(!threadStart) {
                startRefreshThread();
            }
        }
    }
}//Fragment Class
