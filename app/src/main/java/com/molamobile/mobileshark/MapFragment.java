package com.molamobile.mobileshark;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoNr;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthWcdma;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.molamobile.mobileshark.services.LocationServiceFG;
import com.molamobile.mobileshark.utils.CellUtility;
import com.molamobile.mobileshark.utils.CommonUtility;
import com.molamobile.mobileshark.utils.Constants;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import androidx.recyclerview.widget.RecyclerView;

/*Created By Ajish Dharman on 30-July-2020
 *
 *
 */
public class MapFragment extends Fragment implements OnMapReadyCallback {

    GoogleMap mMap;
    MapView mMapView;

    TextView txt_battery;
    TextView txt_wifi_connec;
    TextView txt_ping;
    TextView txt_operator;
    TextView txt_roaming;
    TextView txt_mcc_mnc;
    TextView txt_sim_data_nw;
    TextView txt_cell_name;

    TextView txt_sim_state;
    TextView txt_sim_data_connected;


    Activity thisActvity;
    TelephonyManager tm;
    boolean permissionGranted = false;
    ConnectivityManager cm;
    TelephonyManager tmsub1;
    TelephonyManager tmsub2 = null;
    Location currentLocation;
    BroadcastReceiver receiver;

    Handler handler;
    Runnable runnable;

    boolean threadStart =false;
    private FusedLocationProviderClient mFusedLocationClient;



    boolean zoomed =false;

    boolean isOnRefresh =false;

    static int counter = 0;

    private void initialize(){
        thisActvity = getActivity();


        tm = (TelephonyManager) thisActvity.getSystemService(Context.TELEPHONY_SERVICE);
        cm = (ConnectivityManager) thisActvity.getSystemService(Context.CONNECTIVITY_SERVICE);

        if(checkAndRequestPermissionsForMesure(thisActvity)){

            permissionGranted = true;
        }
        else{
            permissionGranted = false;
        }


        txt_battery = (TextView) getView().findViewById(R.id.txt_battery);
        //txt_wifi_connec_stats = (TextView) findViewById(R.id.txt_wifi_connec_stats);
        txt_ping = (TextView) getView().findViewById(R.id.txt_ping);
        txt_operator = (TextView) getView().findViewById(R.id.txt_operator);
        txt_mcc_mnc = (TextView)getView(). findViewById(R.id.txt_mcc_mnc);
        //  txt_sim_state_val = (TextView) findViewById(R.id.txt_sim_state_val);
        txt_roaming = (TextView)getView(). findViewById(R.id.txt_roaming);
        txt_sim_data_nw = (TextView)getView(). findViewById(R.id.txt_sim_data_nw);
        //  txt_sim_data_connected_val = (TextView)findViewById(R.id.txt_sim_data_connected_val);
        txt_cell_name = (TextView) getView().findViewById(R.id.txt_cell_name);
        txt_wifi_connec = (TextView)getView().findViewById(R.id.txt_wifi_connec) ;

        txt_sim_state = (TextView)getView().findViewById(R.id.txt_sim_state);
        txt_sim_data_connected = (TextView)getView().findViewById(R.id.txt_sim_data_connected);

        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {

                if(intent.getAction().equals(Constants.INTENT_LOCATION_ACTION)){

                    Location loc = intent.getExtras().getParcelable(Constants.INTENT_LOCATION_ACTION_MSG);
                    //Zoom to that location in map
                    if (loc != null) {
                        currentLocation = loc;
                        plotPrimayServingCell(loc);
                    }

                }

            }
        };//Receiver
    }

    public MapFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.map_fragment, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initialize();
        mMapView = (MapView)view.findViewById(R.id.map);
        if(mMapView!=null){
            mMapView.onCreate(null);
            mMapView.onResume();
            mMapView.getMapAsync(this);
        }
        if(permissionGranted) {
            if (Build.VERSION.SDK_INT >= 29) {

                getSimParamApi29();

            }
            else{
                getSIMParamOtherVersionApI();
            }
        }

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(thisActvity);


    }




    @TargetApi(29)
    private void getSimParamApi29() throws SecurityException {

        if (permissionGranted) {

            SubscriptionManager subscriptionManager =
                    (SubscriptionManager) thisActvity.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);

            try {

                // hasdual = false;
                List<SubscriptionInfo> subscriptionInfoList = subscriptionManager.getActiveSubscriptionInfoList();

                if (subscriptionInfoList != null) {
                    if (subscriptionInfoList.size() > 0) {

                        int sz = subscriptionInfoList.size();
                        //subid1 = subscriptionInfoList.get(0).getSubscriptionId();
                        // tmsub1 = tm.createForSubscriptionId(subid1);
                        if (sz > 1) {
                            //Dual Sim get the Details for the First 1 - Later will take it from settings
                            // simSlot1.setText("SIM 1");
                            int subid1 = subscriptionInfoList.get(0).getSubscriptionId();
                            tmsub1 = tm.createForSubscriptionId(subid1);

                        } else {
                            int subid1 = subscriptionInfoList.get(0).getSubscriptionId();
                            tmsub1 = tm.createForSubscriptionId(subid1);

                        }
                        String simState = CellUtility.simState(tmsub1.getSimState()); //SimState

                        if (simState.equals("Ready")) {
                            txt_sim_state.setText("Sim State: "+"Ready");
                        } else {
                            txt_sim_state.setText("Sim State: "+simState);

                        }
                        String nw = CellUtility.getRatFromInt(tmsub1.getDataNetworkType());
                        txt_sim_data_nw.setText("Data N/W: "+nw);

                        String mccmnc = tmsub1.getNetworkOperator()+"".trim();
                        //   txt_mcc_mnc.append(" "+ mccmnc.substring(0,2)+ " "+mccmnc.substring(3,mccmnc.length()-1));
                        txt_mcc_mnc.setText("MCC MNC: "+ mccmnc.substring(0,3) +" "+mccmnc.substring(3,mccmnc.length()));

                        // txt_mcc_mnc.setText(txt_mcc_mnc.getText()+" "+tmsub1.getNetworkOperator());

                        String opname = tmsub1.getSimOperatorName();
                        if (opname.length() > 10) {
                            txt_operator.setText("Operator: "+opname.substring(0,10) + "...");
                        } else {
                            //  txt_operator_val.setText(opname);
                            txt_operator.setText("Operator: " +opname);

                        }
                        boolean roaming = tmsub1.isNetworkRoaming();
                        // txt_roaming_val.setText(roaming == true ? "Yes" : "No");
                        if(roaming) {
                            txt_roaming.setText("Roaming: "+"Yes");
                        }
                        else{
                            txt_roaming.setText("Roaming: "+"No");
                        }


                        getWifi_Battery_SimParams();
                        startRefreshThread();


                    }
                }

            } catch (Exception ex) {

                String s = ex.getMessage();
                String s3 = "9939393";
            }//catch
        }
    }

    private void getSIMParamOtherVersionApI() {

        try {

            int sz = 0;
            if (permissionGranted) {

                SubscriptionManager subscriptionManager =
                        (SubscriptionManager) thisActvity.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);
                try {

                    int subid1 = 0;
                    int simslot=0;
                    List<SubscriptionInfo> subscriptionInfoList = subscriptionManager.getActiveSubscriptionInfoList();
                    if (subscriptionInfoList != null) {


                        sz = subscriptionInfoList.size();
                        subid1 = subscriptionInfoList.get(0).getSubscriptionId();


                        if (sz > 1) {

                            subid1 = subscriptionInfoList.get(0).getSubscriptionId();


                        } else if (sz == 1)

                            subid1 = subscriptionInfoList.get(0).getSubscriptionId();
                        simslot =       subscriptionInfoList.get(0).getSimSlotIndex();
                    } else if (sz == 0) {

                    }
                    String nwt;



                    String simState = CellUtility.simState(CellUtility.getSIMStateBySlot(thisActvity, "getSimState", simslot));
                    nwt = CellUtility.getRatFromInt(CellUtility.getNetWork(thisActvity, "getNetworkType", subid1));

                    int nwtOp = CellUtility.getNetworkOperator(thisActvity, "getNetworkOperator", subid1);
                    String netOpname = CellUtility.getNetworkOperatorName(thisActvity, "getNetworkOperatorName", subid1);
                    boolean roaming = CellUtility.isNetworkRoaming(thisActvity, "isNetworkRoaming", subid1);


                    if (netOpname.length() > 9) {
                        txt_operator.setText("Operator: "+netOpname.substring(0,10) + "...");
                    } else {
                        txt_operator.setText("Operator: "+netOpname);

                    }
                    //txt_sim_state_val.setText(simState);
                    //  txt_roaming_val.setText(roaming == true ? "Yes" : "No");
                    if(roaming) {
                        txt_roaming.setText("Roaming: "+"Yes");
                    }
                    else{
                        txt_roaming.setText("Roaming: "+"No");
                    }

                    if (simState.equals("Ready")) {
                        txt_sim_state.setText("Sim State: "+"Ready");
                    } else {
                        txt_sim_state.setText("Sim State: "+simState);

                    }

                    String mccmnc = nwtOp+"".trim();
                    //    String mcc = mccmnc.substring(0,3);
                    //    String mnc = mccmnc.substring(3,mccmnc.length());
                    txt_mcc_mnc.setText("MCC MNC: "+ mccmnc.substring(0,3) +" "+mccmnc.substring(3,mccmnc.length()));

                    txt_sim_data_nw.setText("Data N/W: "+nwt);

                    getWifi_Battery_SimParams();

                    startRefreshThread();

                } catch (SecurityException ex) {

                    String s = ex.getMessage();

                } catch (Exception ex) {

                    String s = ex.getMessage();

                }

            }//PermissionGranted
        } catch (Exception ex) {

            String s = ex.getMessage();
        }
    }//setSIMValuesOtherVersionApI


    private void getWifi_Battery_SimParams(){

        int sid;

        String simdata="";

        isOnRefresh =true;

        try {

          /*  sid = CellUtility.getDefault(thisActvity);
            if (sid == 1) {


              //  txt_sim_data_connected.setText("SIM Data:"+" "+CellUtility.getDataStateName(tm.getDataState()));
                simdata = CellUtility.getDataStateName(tm.getDataState());
                txt_sim_data_connected.setText("SIM Data: "+simdata);
            } else if (sid == 2) {


               // txt_sim_data_connected.setText("SIM Data:"+" "+CellUtility.getDataStateName(tm.getDataState()));
              //  txt_sim_data_connected.setText("SIM Data: "+" "+CellUtility.getDataStateName(tm.getDataState()));
                simdata = CellUtility.getDataStateName(tm.getDataState());
                txt_sim_data_connected.setText("SIM Data: "+simdata);
            } else {
                sid = CellUtility.getDefault(thisActvity);
                if (sid == 1) {
                   // txt_sim_data_connected.setText("SIM Data:"+" "+CellUtility.getDataStateName(tm.getDataState()));
                    simdata = CellUtility.getDataStateName(tm.getDataState());
                    txt_sim_data_connected.setText("SIM Data: "+simdata);
                }
                else if(sid==3){
                    simdata = CellUtility.getDataStateName(tm.getDataState());
                    txt_sim_data_connected.setText("SIM Data: "+simdata);
                }
            }*/

            sid = CellUtility.getDefault(thisActvity);
            simdata = CellUtility.getDataStateName(tm.getDataState());
            txt_sim_data_connected.setText("SIM Data: "+simdata);

            isOnRefresh = false;




        }
        catch(Exception ex){

            String s = ex.getMessage();
            isOnRefresh =false;
        }

        try {

            // this.registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

            txt_battery.setText("Battery: "+
                    CellUtility.getBatteryPercentage(thisActvity)+"");
            String source = txt_battery.getText().toString();
            int start= source.indexOf(':') + 2;
            int ent = source.length();
            Spannable wordtoSpan = new SpannableString(
                    txt_battery.getText().toString());
            wordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#3CB64A")),
                    start,ent, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            txt_battery.setText(wordtoSpan);

            if (CellUtility.isWifi(thisActvity)) {
                // txt_wifi_connec_stats.setText("Yes");
                String wifis = "Wifi Connected: Yes";
                start = wifis.indexOf(":") +1;
                ent = wifis.length();
                wordtoSpan = new SpannableString(wifis);
                ForegroundColorSpan foregroundSpan = new ForegroundColorSpan(Color.RED);
                wordtoSpan.setSpan(foregroundSpan, start,ent, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                txt_wifi_connec.setText(wordtoSpan);

            } else {

                String wifis = "Wifi Connected: No";
                start = wifis.indexOf(":") +1;
                ent = wifis.length();
                wordtoSpan = new SpannableString(wifis);
                ForegroundColorSpan foregroundSpan = new ForegroundColorSpan(Color.parseColor("#3CB64A"));
                wordtoSpan.setSpan(foregroundSpan, start,ent, Spannable.SPAN_INCLUSIVE_INCLUSIVE);
                txt_wifi_connec.setText(wordtoSpan);

            }


            if(Build.VERSION.SDK_INT<30) {
                MapFragment.AsyncTaskExample asy = new MapFragment.AsyncTaskExample();
                asy.execute();
            }

        }
        catch (SecurityException ex){

            String s = ex.getLocalizedMessage();
        }
        catch(Exception ex){
            String s = ex.getMessage();
        }

    }



    @Override
    public void onMapReady(GoogleMap googleMap) {

        if(googleMap!=null){
            mMap = googleMap;
        }

    }

    public  boolean checkAndRequestPermissionsForMesure(Activity thisCtx) {
        int phone = ContextCompat.checkSelfPermission(thisCtx, Manifest.permission.READ_PHONE_STATE);
        int storage = ContextCompat.checkSelfPermission(thisCtx, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int loc = ContextCompat.checkSelfPermission(thisCtx, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int loc2 = ContextCompat.checkSelfPermission(thisCtx, android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (phone != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (loc2 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (loc != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty())
        {
            //ActivityCompat.requestPermissions(thisCtx,listPermissionsNeeded.toArray
            // (new String[listPermissionsNeeded.size()]), CommonUtility.REQUEST_ID_MULTIPLE_PERMISSIONS);

            requestPermissions(listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]), CommonUtility.REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;


        }
        return true;
    }


    private class AsyncTaskExample extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            return(isHostAvailable());
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                txt_ping.setText("Ping: Success");
                //pingstatus.setTextColor(Color.GREEN);
                String source =  txt_ping.getText().toString();
                int start = source.indexOf(':') + 2;
                int ent = source.length();
                Spannable wordtoSpan = new SpannableString(
                        txt_ping.getText().toString());


                wordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#3CB64A")),
                        start,ent, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                txt_ping.setText(wordtoSpan);
            }
            else{
                txt_ping.setText("Ping: Failed");
                //pingstatus.setTextColor(Color.RED);
            }
        }
    }

    public static boolean isHostAvailable() {
        
        try {
            HttpURLConnection.setFollowRedirects(false);
            // note : you may also need
            // HttpURLConnection.setInstanceFollowRedirects(false)
            HttpURLConnection con = (HttpURLConnection) new URL("http://www.google.com")
                    .openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    public void onResume() {

        if(isLocationEnabled(thisActvity)){

            if(permissionGranted){

                if(mFusedLocationClient!=null){
                    getLastLocation();
                }
            }
        }
        else {
            ShowAlert(thisActvity,"LOC","Please turn on device location, which uses Google's location service.");
        }
        // rcvServingCells.refreshDrawableState();
        counter++;
        if(counter > 1){
            if(!threadStart) {
                startRefreshThread();
            }
        }
        super.onResume();
    }

    public  boolean isLocationEnabled(Context context) {


        int locationMode = 0;
        String locationProviders;

        boolean locationenabled = false;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

                if( locationMode!=Settings.Secure.LOCATION_MODE_OFF){

                    if(locationMode!=Settings.Secure.LOCATION_MODE_HIGH_ACCURACY)
                    {
                        locationenabled =false;
                    }
                    else if(locationMode==Settings.Secure.LOCATION_MODE_HIGH_ACCURACY){

                        locationenabled =true;
                    }
                }
                else{

                    locationenabled =false;
                }
            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                locationenabled = false;
            }

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(),
                    Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            locationenabled = !TextUtils.isEmpty(locationProviders);
        }

        return  locationenabled;
    }


        private void ShowAlert(Context ctx, final String focus, String msg){

            androidx.appcompat.app.AlertDialog.Builder dlg = null;

            dlg =   new AlertDialog.Builder(ctx,R.style.AlertDialogTheme)
                    .setTitle("")
                    .setMessage(msg)

                    // Specifying a listener allows you to take an action before dismissing the dialog.
                    // The dialog is automatically dismissed when a dialog button is clicked.
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            // finish();
                            if(focus.equals("LOC")){


                                startActivityForResult(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS),
                                        0);
                            }
                            else if(focus.equals("INET")){

                            }


                        }
                    });


            if(dlg!=null){

                dlg.show();
            }
        }//ShowAlert

    @Override
    public void onStart() {

        IntentFilter inf = new IntentFilter();
        inf.addAction(Constants.INTENT_LOCATION_ACTION);
        LocalBroadcastManager.getInstance(thisActvity).registerReceiver((receiver),
                inf
        );
        super.onStart();
    }

    private void putCellOnMap(Location loc) {

        CameraUpdate center;
        if (loc != null && mMap != null) {

            if(CommonUtility.prevMarker==null) {

                //No marker yet so create
                MarkerOptions mopt = new MarkerOptions();
               // mopt.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker2));
                mopt.title("");
                mopt.position(new LatLng(loc.getLatitude(), loc.getLongitude()));
                Marker m = mMap.addMarker(mopt);
            }
            else if(new LatLng(loc.getLatitude(),loc.getLongitude())==CommonUtility.prevMarker.getPosition()) {

               CommonUtility.prevMarker.remove();
                MarkerOptions mopt = new MarkerOptions();
               //   mopt.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_map_marker2));
                mopt.title("");
                mopt.position(new LatLng(loc.getLatitude(), loc.getLongitude()));
                Marker m = mMap.addMarker(mopt);
                CommonUtility.prevMarker = m;
            }

            updateCameraByZoom(zoomed, loc);
            }
    }

    private void updateCameraByZoom(boolean isZoomed,Location loc) {

        if (!zoomed) {
            CameraPosition cameraPosition = new CameraPosition.Builder()
                    .target(new LatLng(loc.getLatitude(),
                            loc.getLongitude()))      // Sets the center of the map to Mountain View
                    .zoom(17)                   // Sets the zoom
                    .build();                   // Creates a CameraPosition from the builder
            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
            zoomed = true;

        } else {
            //Already Zoomed
            //Only move camera with out zoom

            CameraUpdate center =
                    CameraUpdateFactory.newLatLng(new LatLng(loc.getLatitude(),
                            loc.getLongitude()));
            mMap.moveCamera(center);
        }

    }

    private void getLastLocation() {
        try {

            if (permissionGranted) {
                mFusedLocationClient.getLastLocation()
                        .addOnSuccessListener(thisActvity, new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(Location location) {
                                // Got last known location. In some rare situations this can be null.
                                if (location != null) {
                                    currentLocation = location;
                                    plotPrimayServingCell(currentLocation);
                                }
                            }
                        });

                startLocationService();
            }
        } catch (SecurityException ex) {

        }
    }


     private void plotPrimayServingCell(Location loc){

        int dbm=0;
        if(loc!=null){
            try {

                if (permissionGranted) {
                    CellInfo cell=null;
                    List<CellInfo> cells = tm.getAllCellInfo();
                    for(CellInfo inf : cells){
                        if(inf.isRegistered()){

                            cell = inf;
                            break;
                        }
                    }
                    if(cell instanceof CellInfoGsm) {

                        dbm = ((CellInfoGsm) cell).getCellSignalStrength().getDbm();

                    }
                    else if(cell instanceof CellInfoLte){
                        dbm = ((CellInfoLte) cell).getCellSignalStrength().getDbm();
                    }
                    else if(cell instanceof CellInfoWcdma){
                        dbm = ((CellInfoWcdma) cell).getCellSignalStrength().getDbm();
                    }
                    else if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q){
                        if(cell instanceof CellInfoNr) {
                            dbm = ((CellInfoNr) cell).getCellSignalStrength().getDbm();
                        }
                    }

                    MarkerOptions mopt = new MarkerOptions();

                    if(dbm>= -140 && dbm <= -120){
                        mopt.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_red));
                    }
                    else if(dbm>=-120 && dbm <=-100){
                        mopt.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_organge));
                    }
                    else if(dbm>=-100 && dbm <=-40){
                        mopt.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_green));
                    }
/*                  if(CommonUtility.prevMarker==null) {
                        mopt.title("");
                        mopt.position(new LatLng(loc.getLatitude(), loc.getLongitude()));
                        Marker m = mMap.addMarker(mopt);
                        CommonUtility.prevMarker = m;
                        updateCameraByZoom(zoomed, loc);
                    }
                    else if(new LatLng(loc.getLatitude(),loc.getLongitude())==CommonUtility.prevMarker.getPosition()) {

                        CommonUtility.prevMarker.remove();
                        mopt.title("");
                        mopt.position(new LatLng(loc.getLatitude(), loc.getLongitude()));
                        Marker m = mMap.addMarker(mopt);
                        CommonUtility.prevMarker = m;
                        updateCameraByZoom(zoomed, loc);
                    }
                    else{
                        mopt.title("");
                        mopt.position(new LatLng(loc.getLatitude(), loc.getLongitude()));
                        Marker m = mMap.addMarker(mopt);
                        CommonUtility.prevMarker = m;
                        updateCameraByZoom(zoomed, loc);

                    }*/

                   mopt.title("");
                   mopt.position(new LatLng(loc.getLatitude(), loc.getLongitude()));
                    mMap.addMarker(mopt);
                   updateCameraByZoom(zoomed, loc);

                }

            }
            catch (SecurityException ex){

            }
            catch (Exception ex){

            }
        }

     }

    private  void  startLocationService(){

        if(!CommonUtility.isLocationServiceStarted) {


            Intent ii = new Intent(thisActvity, LocationServiceFG.class);
            ContextCompat.startForegroundService(getActivity(), ii);

        }

    }

    private  void stopLocationService(){

        Intent ii = new Intent(thisActvity,LocationServiceFG.class);
        thisActvity.stopService(ii);
    }

    @Override
    public void onStop() {

        try {

            stopLocationService();
            if (handler != null) {
                handler.removeCallbacks(runnable);
            }
            counter=0;
            threadStart =false;
        }
        catch (Exception ex){

        }
        super.onStop();
    }


    private void startRefreshThread() {


        handler = new Handler(Looper.myLooper());

        // Define the code block to be executed
        runnable = new Runnable() {
            @Override
            public void run() {

                // Do something here on the main threa
                threadStart = true;

                refreshThreadProc();

                Log.d("Handlers", "Called on main thread");
                handler.postDelayed(this, 2000);

            }
        };

        // Start the initial runnable task by posting through the handler
        handler.post(runnable);

    }

        private void refreshThreadProc(){

            try {
                if(isOnRefresh){
                    return;
                }


                getWifi_Battery_SimParams();



            }
            catch (SecurityException ex){

                isOnRefresh=false;
            }

        }//refreshThreadProc


}//Fragment Class
