package com.molamobile.mobileshark;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.telephony.CellIdentityGsm;
import android.telephony.CellIdentityLte;
import android.telephony.CellIdentityNr;
import android.telephony.CellIdentityWcdma;
import android.telephony.CellInfo;
import android.telephony.CellInfoGsm;
import android.telephony.CellInfoLte;
import android.telephony.CellInfoNr;
import android.telephony.CellInfoWcdma;
import android.telephony.CellSignalStrengthGsm;
import android.telephony.CellSignalStrengthLte;
import android.telephony.CellSignalStrengthNr;
import android.telephony.CellSignalStrengthWcdma;
import android.telephony.SubscriptionInfo;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;
import com.google.android.material.bottomnavigation.BottomNavigationMenuView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.molamobile.mobileshark.adapter.CellDisplayAdapter;
import com.molamobile.mobileshark.adapter.ListAdapter;
import com.molamobile.mobileshark.models.CellDisplayAdapterModel;
import com.molamobile.mobileshark.models.GsmCell;
import com.molamobile.mobileshark.models.WCdmaCell;
import com.molamobile.mobileshark.utils.CellUtility;
import com.molamobile.mobileshark.utils.CommonUtility;

import org.w3c.dom.Text;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MeasureActivity extends AppCompatActivity {
    Context thisActivity;
    TelephonyManager tm;
    TelephonyManager tmsub1;
    TelephonyManager tmsub2 = null;
    BottomNavigationView bottomNavigation;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    boolean permissionGranted = false;

    private Runnable runnableCode;
    Handler handler;

    boolean threadStart = false;

    TextView txt_battery;
    TextView txt_wifi_connec;
    TextView txt_ping;
    TextView txt_operator;
    TextView txt_roaming;
    TextView txt_mcc_mnc;
    TextView txt_sim_data_nw;
    TextView txt_cell_name;

    TextView txt_sim_state;
    TextView txt_sim_data_connected;



    /*TextView txt_operator_val;
    TextView txt_mcc_mnc_val;
    TextView txt_mcc_mnc_val1;
    TextView txt_roaming_val;
    TextView txt_sim_data_nw_val;*/



    ConnectivityManager cm;
    HashMap<String, String[][]> cellMap;
    List<CellDisplayAdapterModel> lstDisplay;
    RecyclerView rcvServingCells;

    List<CellInfoGsm> gsmCells;
    List<CellInfoLte> lteCells;
    List<CellInfoWcdma> wCdmaCells;
    List<CellInfoNr> cellInfoNrs;

    CellDisplayAdapter cellDisplayAdapter;
    TableLayout tbl_cell_params;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.measure_screen_content_layout);
        initialize();
    }


    private void initialize() {

        thisActivity = this;
        bottomNavigation = findViewById(R.id.bottom_navigation);
        tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        cm = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);


        if (checkAndRequestPermissionsForMesure(this)) {

            permissionGranted = true;
        } else {

            permissionGranted = false;
        }

        txt_battery = (TextView) findViewById(R.id.txt_battery);
        //txt_wifi_connec_stats = (TextView) findViewById(R.id.txt_wifi_connec_stats);
        txt_ping = (TextView) findViewById(R.id.txt_ping);
        txt_operator = (TextView) findViewById(R.id.txt_operator);
        txt_mcc_mnc = (TextView) findViewById(R.id.txt_mcc_mnc);
        //  txt_sim_state_val = (TextView) findViewById(R.id.txt_sim_state_val);
        txt_roaming = (TextView) findViewById(R.id.txt_roaming);
        txt_sim_data_nw = (TextView) findViewById(R.id.txt_sim_data_nw);
        //  txt_sim_data_connected_val = (TextView)findViewById(R.id.txt_sim_data_connected_val);
        txt_cell_name = (TextView) findViewById(R.id.txt_cell_name);
        tbl_cell_params = (TableLayout)findViewById(R.id.tbl_cell_params);
        rcvServingCells = (RecyclerView) findViewById(R.id.rcvServingCell) ;
        txt_wifi_connec = (TextView)findViewById(R.id.txt_wifi_connec) ;

        // txt_operator_val = (TextView) findViewById(R.id.txt_operator_val);
        //  txt_mcc_mnc_val1 = (TextView) findViewById(R.id.txt_mcc_mnc_val1);
        //  txt_roaming_val = (TextView) findViewById(R.id.txt_roaming_val);
        //   txt_sim_data_nw_val = (TextView)findViewById(R.id.txt_sim_data_nw_val);

        //  txt_mcc_mnc_val1.setText(" ");

        txt_sim_state = (TextView) findViewById(R.id.txt_sim_state);
        txt_sim_data_connected = (TextView)findViewById(R.id.txt_sim_data_connected);

        int itemViewType = 0;
        rcvServingCells.getRecycledViewPool().setMaxRecycledViews(itemViewType, 0);


        lstDisplay = new ArrayList<CellDisplayAdapterModel>();
        cellDisplayAdapter = new CellDisplayAdapter(lstDisplay,this);

        gsmCells = new ArrayList<CellInfoGsm>();
        lteCells = new ArrayList<CellInfoLte>();
        wCdmaCells = new ArrayList<CellInfoWcdma>();
        cellInfoNrs = new ArrayList<CellInfoNr>();

        //NeighBour
        RecyclerView.LayoutManager mLayoutManager2 = new LinearLayoutManager(getApplicationContext());
        ((LinearLayoutManager) mLayoutManager2).setOrientation(RecyclerView.VERTICAL);
        rcvServingCells.setAdapter(cellDisplayAdapter);
        rcvServingCells.setLayoutManager(mLayoutManager2);

// Find the bottom navigation view, (Use correct ID)
        // menu_item_1 is probably not a good ID for a navigation view


        // Find the menu item and then disable it
        bottomNavigation.getMenu().getItem(0).setChecked(true);
        bottomNavigation.getMenu().getItem(1).setCheckable(false);
        bottomNavigation.getMenu().getItem(2).setCheckable(false);
        bottomNavigation.getMenu().getItem(3).setCheckable(false);
        bottomNavigation.getMenu().getItem(4).setCheckable(false);

     /* try{
            if(Build.VERSION.SDK_INT >= 29) {

                getSimParamApi29();
                List<CellInfo> cellInfos = tm.getAllCellInfo();
                extractMeasuresForCells(cellInfos);
                collectNeighbouringCells(cellInfos);
                groupNeighbouringCells();
                cellDisplayAdapter.addCellInfoList(lstDisplay);
                 cellDisplayAdapter.notifyDataSetChanged();
            }
            else{
                getSIMParamOtherVersionApI();
                List<CellInfo> cellInfos = tm.getAllCellInfo();
                extractMeasuresForCells(cellInfos);
                collectNeighbouringCells(cellInfos);
                groupNeighbouringCells();
                cellDisplayAdapter.addCellInfoList(lstDisplay);
                cellDisplayAdapter.notifyDataSetChanged();

            }

        }
        catch(SecurityException ex){

        }*/

        if(permissionGranted) {
            if (Build.VERSION.SDK_INT >= 29) {

                getSimParamApi29();

            }
            else{
                getSIMParamOtherVersionApI();
            }
        }

    }

    private void startRefreshThread(){

        threadStart = true;

        handler = new Handler();

        // Define the code block to be executed
        runnableCode = new Runnable() {
            @Override
            public void run() {

                // Do something here on the main threa

                setUpCells();

                Log.d("Handlers", "Called on main thread");
                // Repeat this the same runnable code block again another 2 seconds
                // 'this' is referencing the Runnable object

                handler.postDelayed(this, 4000);

            }
        };

        // Start the initial runnable task by posting through the handler
        handler.post(runnableCode);

    }
    private void setUpCells(){
        try {
            List<CellInfo> cellInfos = tm.getAllCellInfo();
            extractMeasuresForCells(cellInfos);
            collectNeighbouringCells(cellInfos);
            groupNeighbouringCells();
            cellDisplayAdapter.addCellInfoList(lstDisplay);
            cellDisplayAdapter.notifyDataSetChanged();
        }
        catch (SecurityException ex){

        }
    }

    @Override
    protected void onResume() {


        super.onResume();

    }

    private void setUpCellList() {

        try {
            List<CellInfo> cellInfos = tm.getAllCellInfo();
            if (Build.VERSION.SDK_INT >= 29) {

                getSimParamApi29();

                extractMeasuresForCells(cellInfos);
                collectNeighbouringCells(cellInfos);
                groupNeighbouringCells();
                cellDisplayAdapter.addCellInfoList(lstDisplay);
                cellDisplayAdapter.notifyDataSetChanged();
            } else {
                getSIMParamOtherVersionApI();
                extractMeasuresForCells(cellInfos);
                collectNeighbouringCells(cellInfos);
                groupNeighbouringCells();
                cellDisplayAdapter.addCellInfoList(lstDisplay);
                cellDisplayAdapter.notifyDataSetChanged();

            }
        }
        catch(SecurityException ex) {

        }
    }

    // This function is called when user accept or decline the permission.
    // Request Code is used to check which permission called this function.
    // This request code is provided when user is prompt for permission.
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super
                .onRequestPermissionsResult(requestCode,
                        permissions,
                        grantResults);

        if (requestCode == REQUEST_ID_MULTIPLE_PERMISSIONS) {

            // Checking whether user granted the permission or not.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED
                    && grantResults[1] == PackageManager.PERMISSION_GRANTED
                    && grantResults[2] == PackageManager.PERMISSION_GRANTED) {

                permissionGranted = true;
                if (Build.VERSION.SDK_INT >= 29) {

                    getSimParamApi29();

                }
                else{
                    getSIMParamOtherVersionApI();
                }

            } else {

                permissionGranted = false;
            }
        }
    }//OnRequestPermissionResult


    @TargetApi(29)
    private void getSimParamApi29() throws SecurityException {

        if (permissionGranted) {

            SubscriptionManager subscriptionManager =
                    (SubscriptionManager) this.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);

            try {

                // hasdual = false;
                List<SubscriptionInfo> subscriptionInfoList = subscriptionManager.getActiveSubscriptionInfoList();

                if (subscriptionInfoList != null) {
                    if (subscriptionInfoList.size() > 0) {

                        int sz = subscriptionInfoList.size();
                        //subid1 = subscriptionInfoList.get(0).getSubscriptionId();
                        // tmsub1 = tm.createForSubscriptionId(subid1);
                        if (sz > 1) {
                            //Dual Sim get the Details for the First 1 - Later will take it from settings
                            // simSlot1.setText("SIM 1");
                            int subid1 = subscriptionInfoList.get(0).getSubscriptionId();
                            tmsub1 = tm.createForSubscriptionId(subid1);

                        } else {
                            int subid1 = subscriptionInfoList.get(0).getSubscriptionId();
                            tmsub1 = tm.createForSubscriptionId(subid1);

                        }
                        String simState = CellUtility.simState(tmsub1.getSimState()); //SimState

                        if (simState.equals("Ready")) {
                            //txt_sim_state_val.setText("Ready");
                            txt_sim_state.append(" "+getColoredString(thisActivity,"Ready","#14F22E"));
                            //txt_sim_state_val.setTextColor(Color.parseColor("#14F22E"));
                        } else {
                            // txt_sim_state_val.setText(simState);
                            // txt_sim_state_val.setTextColor(Color.parseColor("#14F22E"));
                            txt_sim_state.append(" "+getColoredString(thisActivity,simState,"#14F22E"));

                        }
                        String nw = CellUtility.getRatFromInt(tmsub1.getDataNetworkType());
                        txt_sim_data_nw.setText(txt_sim_data_nw.getText().toString()+ " "+nw);

                        // txt_mcc_mnc.setText(tmsub1.getNetworkOperator());//MCC+MNC
                        //txt_mcc_mnc_val.setText(tmsub1.getNetworkOperator());
                        txt_mcc_mnc.setText(txt_mcc_mnc.getText()+" "+tmsub1.getNetworkOperator());

                        String opname = tmsub1.getSimOperatorName();
                        if (opname.length() > 7) {
                            //txt_operator_val.setText(opname.substring(0, 10) + "...");
                            txt_operator.setText(txt_operator.getText()+" "+opname.substring(0, 7) + "...");
                        } else {
                            //  txt_operator_val.setText(opname);

                        }
                        boolean roaming = tmsub1.isNetworkRoaming();
                        // txt_roaming_val.setText(roaming == true ? "Yes" : "No");
                        if(roaming) {
                            txt_roaming.setText(txt_roaming.getText()+" "+"Yes");
                        }
                        else{
                            txt_roaming.setText(txt_roaming.getText()+" "+"No");
                        }

                        populateCellInfo();
                        //startRefreshThread();
                        setUpCells();

                    }
                }

            } catch (Exception ex) {

                String s = ex.getMessage();
                String s3 = "9939393";
            }//catch
        }
    }

    private void getSIMParamOtherVersionApI() {

        try {

            int sz = 0;
            if (permissionGranted) {


                SubscriptionManager subscriptionManager =
                        (SubscriptionManager) this.getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);


                try {

                    int subid1 = 0;
                    int simslot=0;
                    List<SubscriptionInfo> subscriptionInfoList = subscriptionManager.getActiveSubscriptionInfoList();
                    if (subscriptionInfoList != null) {


                        sz = subscriptionInfoList.size();
                        subid1 = subscriptionInfoList.get(0).getSubscriptionId();


                        if (sz > 1) {

                            subid1 = subscriptionInfoList.get(0).getSubscriptionId();


                        } else if (sz == 1)

                            subid1 = subscriptionInfoList.get(0).getSubscriptionId();
                        simslot =       subscriptionInfoList.get(0).getSimSlotIndex();
                    } else if (sz == 0) {

                    }
                    String nwt;



                    String simState = CellUtility.simState(CellUtility.getSIMStateBySlot(this, "getSimState", simslot));
                    nwt = CellUtility.getRatFromInt(CellUtility.getNetWork(this, "getNetworkType", subid1));

                    int nwtOp = CellUtility.getNetworkOperator(this, "getNetworkOperator", subid1);
                    String netOpname = CellUtility.getNetworkOperatorName(this, "getNetworkOperatorName", subid1);
                    boolean roaming = CellUtility.isNetworkRoaming(this, "isNetworkRoaming", 0);

                    if (netOpname.length() > 7) {
                        txt_operator.setText(txt_operator.getText().toString()+" "+netOpname.substring(0, 7) + "...");
                    } else {
                        txt_operator.setText(txt_operator.getText().toString()+" "+netOpname);

                    }
                    //txt_sim_state_val.setText(simState);
                    //  txt_roaming_val.setText(roaming == true ? "Yes" : "No");
                    if(roaming) {
                        txt_roaming.setText(txt_roaming.getText()+" "+"Yes");
                    }
                    else{
                        txt_roaming.setText(txt_roaming.getText()+" "+"No");
                    }

                    if (simState.equals("Ready")) {
                        //txt_sim_state_val.setText("Ready");
                        txt_sim_state.append(" "+getColoredString(thisActivity,"Ready","#14F22E"));
                        //txt_sim_state_val.setTextColor(Color.parseColor("#14F22E"));
                    } else {
                        // txt_sim_state_val.setText(simState);
                        // txt_sim_state_val.setTextColor(Color.parseColor("#14F22E"));
                        txt_sim_state.append(" "+getColoredString(thisActivity,simState,"#14F22E"));

                    }

                    txt_mcc_mnc.append(" "+nwtOp+"");
                    txt_sim_data_nw.append(" "+nwt);

                    populateCellInfo();
                    setUpCells();
                    //  startRefreshThread();
                } catch (SecurityException ex) {

                    String s = ex.getMessage();

                } catch (Exception ex) {

                    String s = ex.getMessage();

                }

            }//PermissionGranted
        } catch (Exception ex) {

            String s = ex.getMessage();
        }
    }//setSIMValuesOtherVersionApI


    private void populateCellInfo(){

        int sid;

        try {

            sid = CellUtility.getDefault(thisActivity);
            if (sid == 1) {

                txt_sim_data_connected.append(" "+CellUtility.getDataStateName(tm.getDataState()));
            } else if (sid == 2) {

                txt_sim_data_connected.append(" "+CellUtility.getDataStateName(tm.getDataState()));
            } else {
                sid = CellUtility.getDefault(thisActivity);
                if (sid == 1) {

                    txt_sim_data_connected.append(" "+
                            CellUtility.getDataStateName(tm.getDataState()));
                }
            }


        }
        catch(Exception ex){

            String s = ex.getMessage();
        }

        try {

            // this.registerReceiver(this.mBatInfoReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

            txt_battery.setText("Battery: "+
                    CellUtility.getBatteryPercentage(thisActivity)+"");
            String source = txt_battery.getText().toString();
            int start= source.indexOf(':') + 2;
            int ent = source.length();
            Spannable  wordtoSpan = new SpannableString(
                    txt_battery.getText().toString());
            wordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#3CB64A")),
                    start,ent, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            txt_battery.setText(wordtoSpan);

            if (CellUtility.isWifi(thisActivity)) {
                // txt_wifi_connec_stats.setText("Yes");
                txt_wifi_connec.setText("Wifi Connected: "+"Yes");
                source =  txt_wifi_connec.getText().toString().trim();
                start = source.indexOf(':') + 2;
                ent = source.length();
                wordtoSpan = new SpannableString(
                        txt_wifi_connec.getText().toString().trim());
                wordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#3CB64A")),
                        start,ent, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                txt_wifi_connec.setText(wordtoSpan);

            } else {
                txt_wifi_connec.setText("Wifi Connected: "+"No");
            }






            if(Build.VERSION.SDK_INT<30) {
                AsyncTaskExample asy = new AsyncTaskExample();
                asy.execute();
            }

        }
        catch (SecurityException ex){

            String s = ex.getLocalizedMessage();
        }
        catch(Exception ex){
            String s = ex.getMessage();
        }

    }

    private void extractMeasuresForCells(List<CellInfo> cellInfos){


        for(int i =0;i<cellInfos.size();i++) {
            CellInfo info = cellInfos.get(i);
            if (info != null) {

                // if(info.isRegistered()) { //check serving cell

                if (info instanceof CellInfoGsm) { //GSM

                    if(info.isRegistered()) {


                        String[]servCellParms = {"ARFCN","BSIC","TIMINGADV","ASSU","CID","LAC","MCC","MNC","DBM","LEVEL"};
                        int rowSize = 2;
                        int columnSize = servCellParms.length;
                        String key = "G_S";
                        String[][] gsmTable = new String[rowSize][columnSize];
                        for(int k =0;k<columnSize;k++){
                            gsmTable[0][k]  = servCellParms[k];
                        }//k

                        final CellSignalStrengthGsm gsmSignal = ((CellInfoGsm) info).getCellSignalStrength();
                        final CellIdentityGsm identityGsm = ((CellInfoGsm) info).getCellIdentity();

                        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                            //cellMap.put("ARFCN", identityGsm.getArfcn() + "");
                            // cellMap.put("BSIC", identityGsm.getBsic() + "");
                            gsmTable[1][0] = identityGsm.getArfcn() + "";
                            gsmTable[1][1] = identityGsm.getBsic() + "";
                        }
                        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {

                            //cellMap.put("TIMINGADV", gsmSignal.getTimingAdvance() + "");
                            gsmTable[1][2] =  gsmSignal.getTimingAdvance() + "";
                        }

                        gsmTable[1][3] =  gsmSignal.getAsuLevel() + "";
                        gsmTable[1][4] =  identityGsm.getCid() + "";
                        gsmTable[1][5] =  identityGsm.getLac() + "";
                        gsmTable[1][6] =  identityGsm.getMcc() + "";
                        gsmTable[1][7] =  identityGsm.getMnc() + "";
                        gsmTable[1][8] =  gsmSignal.getDbm() + "";
                        gsmTable[1][9] =  gsmSignal.getLevel() + "";

                            /*cellMap.put("ASSU", gsmSignal.getAsuLevel() + "");
                            cellMap.put("CID", identityGsm.getCid() + "");
                            cellMap.put("LAC", identityGsm.getLac() + "");
                            cellMap.put("MCC", identityGsm.getMcc() + "");
                            cellMap.put("MNC", identityGsm.getMnc() + "");
                            cellMap.put("DBM", gsmSignal.getDbm() + "");
                            cellMap.put("LEVEL", gsmSignal.getLevel() + "");*/
                        CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                        cellMap = new HashMap<String,String[][]>();
                        cellMap.put(key,gsmTable);
                        model.setCellType("G_S");
                        model.setCellTypeName("Primary Serving Cell");
                        model.setRows(rowSize);
                        model.setColumns(columnSize);
                        model.setCellMap(cellMap);
                        lstDisplay.add(model);
                    }
                    else{
                        //gsmCells.add((CellInfoGsm)info);
                    }

                }//if
                else if (info instanceof CellInfoLte) {

                    if(info.isRegistered()) {
                        //LTE
                        String[]servCellParms = {"CI","EARFCN","TIMINGADV","MCC","MNC","BANDWIDTH",
                                "PCI","TAC","ASU","CQI","DBM","LEVEL","RSRP","RSRQ","RSSNR"};
                        int rowSize = 2;
                        int columnSize = servCellParms.length;
                        String key ="L_S";
                        String[][] lteTable = new String[rowSize][columnSize];
                        for(int j =0;j<columnSize;j++){
                            lteTable[0][j]  = servCellParms[j];
                        }//k
                        final CellSignalStrengthLte lteSignal = ((CellInfoLte) info).getCellSignalStrength();
                        final CellIdentityLte lteIdentity = ((CellInfoLte) info).getCellIdentity();

                        //cellMap.put("CI", lteIdentity.getCi() + "");
                        lteTable[1][0]=lteIdentity.getCi() + "";
                        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                            // cellMap.put("EARFCN", lteIdentity.getEarfcn() + "");
                            lteTable[1][1]= lteIdentity.getEarfcn() + "";
                        }
                        lteTable[1][2]=   lteSignal.getTimingAdvance() + "";


                        if (Build.VERSION.SDK_INT >= 28) {
                            //  cellMap.put("MCC", lteIdentity.getMccString() + "");
                            // cellMap.put("MNC", lteIdentity.getMncString() + "");
                            lteTable[1][3]=lteIdentity.getMccString() + "";
                            lteTable[1][4]=lteIdentity.getMncString();

                        } else {
                            //cellMap.put("MCC", lteIdentity.getMcc() + "");
                            // cellMap.put("MNC", lteIdentity.getMnc() + "");
                            lteTable[1][3]=lteIdentity.getMcc() + "";
                            lteTable[1][4]=lteIdentity.getMnc()+"";
                        }

                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                            //  cellMap.put("BANDWIDTH", lteIdentity.getBandwidth() + "");
                            lteTable[1][5]=lteIdentity.getBandwidth()+"";
                        }

                        //  cellMap.put("PCI", lteIdentity.getPci() + "");
                        // cellMap.put("TAC", lteIdentity.getTac() + "");
                        //  cellMap.put("ASU", lteSignal.getAsuLevel() + "");

                        lteTable[1][6]= lteIdentity.getPci() + "";
                        lteTable[1][7]= lteIdentity.getTac() + "";
                        lteTable[1][8]= lteSignal.getAsuLevel() + "";
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            lteTable[1][9]= lteSignal.getCqi() + "";
                            // lteTable[1][3]= lteSignal.getCqi() + "");
                        }
                        //cellMap.put("DBM", lteSignal.getDbm() + "");
                        // cellMap.put("LEVEL", lteSignal.getLevel() + "");

                        lteTable[1][10]=  lteSignal.getDbm() + "";
                        lteTable[1][11]=  lteSignal.getLevel() + "";
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                            //cellMap.put("RSRP", lteSignal.getRsrp() + "");
                            // cellMap.put("RSRQ", lteSignal.getRsrq() + "");
                            //cellMap.put("RSSNR", lteSignal.getRssnr() + "");

                            lteTable[1][12]=   lteSignal.getRsrp() + "";
                            lteTable[1][13]=   lteSignal.getRsrq() + "";
                            lteTable[1][14]=   lteSignal.getRssnr() + "";
                        }
                        //cellMap.put("TIMINGADV", lteSignal.getTimingAdvance() + "");


                        CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                        cellMap = new HashMap<String,String[][]>();
                        cellMap.put(key,lteTable);
                        model.setCellType("L_S");
                        model.setCellTypeName("Primary Serving Cell");
                        model.setRows(rowSize);
                        model.setColumns(columnSize);
                        model.setCellMap(cellMap);
                        lstDisplay.add(model);
                    }
                    else{
                        //  lteCells.add((CellInfoLte)info);
                    }


                } else if (info instanceof CellInfoWcdma) {

                    //3G
                    if(info.isRegistered()) {

                        String[]servCellParms = {"CID","LAC","MCC","MNC","PSC",
                                "UARFCN"};
                        int rowSize = 2;
                        int columnSize = servCellParms.length;
                        String key = "W_S";
                        String[][] wcdmaTable = new String[rowSize][columnSize];
                        for(int j =0;j<columnSize;j++){
                            wcdmaTable[0][j]  = servCellParms[j];
                        }//k
                        final CellSignalStrengthWcdma wcdmaSignal = ((CellInfoWcdma) info).getCellSignalStrength();
                        final CellIdentityWcdma wcdmaIdentity = ((CellInfoWcdma) info).getCellIdentity();

                        //cellMap.put("CID", wcdmaIdentity.getCid() + "");
                        //cellMap.put("LAC", wcdmaIdentity.getLac() + "");
                        wcdmaTable[1][0]  = wcdmaIdentity.getCid() + "";
                        wcdmaTable[1][1]  = wcdmaIdentity.getLac() + "";

                        if (Build.VERSION.SDK_INT >= 28) {
                            //  cellMap.put("MCC", wcdmaIdentity.getMccString() + "");
                            // cellMap.put("MNC", wcdmaIdentity.getMncString() + "");
                            wcdmaTable[1][2]  =  wcdmaIdentity.getMccString() + "";
                            wcdmaTable[1][3]  =  wcdmaIdentity.getMncString() + "";
                        } else {
                            // cellMap.put("MCC", wcdmaIdentity.getMcc() + "");
                            //cellMap.put("MNC", wcdmaIdentity.getMnc() + "");
                            wcdmaTable[1][2]  =  wcdmaIdentity.getMcc() + "";
                            wcdmaTable[1][3]  =  wcdmaIdentity.getMnc() + "";
                        }
                        //cellMap.put("PSC", wcdmaIdentity.getPsc() + "");
                        wcdmaTable[1][4]  = wcdmaIdentity.getPsc() + "";
                        if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                            // cellMap.put("UARFCN", wcdmaIdentity.getUarfcn() + "");
                            wcdmaTable[1][5]  =  wcdmaIdentity.getUarfcn() + "";
                        }
                        CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                        cellMap = new HashMap<String,String[][]>();
                        cellMap.put(key,wcdmaTable);
                        model.setCellType("W_S");
                        model.setCellTypeName("Primary Serving Cell");
                        model.setRows(rowSize);
                        model.setColumns(columnSize);
                        model.setCellMap(cellMap);
                        lstDisplay.add(model);
                    }
                    else{

                        //wCdmaCells.add((CellInfoWcdma)info);
                    }


                }//cdma 3G
                else if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

                    if (info instanceof CellInfoNr) {

                        if(info.isRegistered()) {

                            String[]servCellParms = {"MCC","MNC","NCI","NRARFCN","PCI",
                                    "CSIRSRP","CSISINR","DBM","LEVEL","SSRSRP","SSRSRQ","SSSINR"};
                            int rowSize = 2;
                            int columnSize = servCellParms.length;
                            String key ="5_S";
                            String[][] fiveGTable = new String[rowSize][columnSize];
                            for(int j =0;j<columnSize;j++){
                                fiveGTable[0][j]  = servCellParms[j];
                            }//k

                            final CellIdentityNr fiveGIdentity = (CellIdentityNr) ((CellInfoNr) info).getCellIdentity();
                            final CellSignalStrengthNr fiveGISignal = (CellSignalStrengthNr) ((CellInfoNr) info).getCellSignalStrength();

/*
                                cellMap.put("MCC", fiveGIdentity.getMccString() + "");
                                cellMap.put("MNC", fiveGIdentity.getMncString() + "");
                                cellMap.put("NCI", fiveGIdentity.getNci() + "");
                                cellMap.put("NRARFCN", fiveGIdentity.getNrarfcn() + "");
                                cellMap.put("PCI", fiveGIdentity.getPci() + "");

                                cellMap.put("CSIRSRP", fiveGISignal.getCsiRsrp() + "");
                                cellMap.put("CSISINR", fiveGISignal.getCsiSinr() + "");
                                cellMap.put("DBM", fiveGISignal.getDbm() + "");
                                cellMap.put("LEVEL", fiveGISignal.getLevel() + "");

                                cellMap.put("SSRSRP", fiveGISignal.getSsRsrp() + "");
                                cellMap.put("SSRSRQ", fiveGISignal.getSsRsrq() + "");
                                cellMap.put("SSSINR", fiveGISignal.getSsSinr() + "");
*/

                            fiveGTable[1][0]  = fiveGIdentity.getMccString() + "";
                            fiveGTable[1][1]  = fiveGIdentity.getMncString() + "";
                            fiveGTable[1][2]  = fiveGIdentity.getNci() + "";
                            fiveGTable[1][3]  = fiveGIdentity.getNrarfcn() + "";
                            fiveGTable[1][4]  =fiveGIdentity.getPci() + "";

                            fiveGTable[1][5]  = fiveGISignal.getCsiRsrp() + "";
                            fiveGTable[1][6]  = fiveGISignal.getCsiSinr() + "";
                            fiveGTable[1][7]  = fiveGISignal.getDbm() + "";
                            fiveGTable[1][8]  =fiveGISignal.getLevel() + "";

                            fiveGTable[1][9]  = fiveGISignal.getSsRsrp() + "";
                            fiveGTable[1][10]  = fiveGISignal.getSsRsrq() + "";
                            fiveGTable[1][11]  = fiveGISignal.getSsSinr() + "";
                            CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                            cellMap = new HashMap<String,String[][]>();
                            cellMap.put(key,fiveGTable);
                            model.setCellType("5_S");
                            model.setCellTypeName("Primary Serving Cell");
                            model.setRows(rowSize);
                            model.setColumns(columnSize);
                            model.setCellMap(cellMap);
                            lstDisplay.add(model);
                        }
                        else{

                            // cellInfoNrs.add((CellInfoNr)info);
                        }
                    }
                }


                // groupNeighbouringCells();

                //     cellDisplayAdapter.addCellInfoList(lstDisplay);
                //   cellDisplayAdapter.notifyDataSetChanged();


            }//if null
        }//for
    }//extractMeasuresForCells

    /*collectNeighbouringCells(cellInfos);
    groupNeighbouringCells();

        cellDisplayAdapter.addCellInfoList(lstDisplay);
        cellDisplayAdapter.notifyDataSetChanged();*/




    private void collectNeighbouringCells(List<CellInfo> cellInfos){
        if(gsmCells.size()>0){
            gsmCells.clear();
        }
        if(cellInfoNrs.size()>0){
            cellInfoNrs.clear();
        }
        if(wCdmaCells.size()>0){
            wCdmaCells.clear();
        }
        if(lteCells.size()>0){
            lteCells.clear();
        }
        try {
            for (CellInfo cinfo : cellInfos) {

                if (!cinfo.isRegistered()) {

                    if (cinfo instanceof CellInfoGsm) {

                        gsmCells.add((CellInfoGsm) cinfo);
                        if (gsmCells.size() > 6) {
                            for (int i = 7; i < gsmCells.size(); i++) {
                                gsmCells.remove(i);
                            }
                        }
                    } else if (cinfo instanceof CellInfoLte) {

                        lteCells.add((CellInfoLte) cinfo);
                        if (lteCells.size() > 6) {
                            for (int i = 7; i < lteCells.size(); i++) {
                                lteCells.remove(i);
                            }
                        }
                    } else if (cinfo instanceof CellInfoWcdma) {

                        wCdmaCells.add((CellInfoWcdma) cinfo);
                        if (wCdmaCells.size() > 6) {
                            for (int i = 7; i < wCdmaCells.size(); i++) {
                                wCdmaCells.remove(i);
                            }
                        }
                    } else if (Build.VERSION.SDK_INT >= 29) {
                     if (cinfo instanceof CellInfoNr) {

                            cellInfoNrs.add((CellInfoNr) cinfo);
                            if (cellInfoNrs.size() > 6) {
                                for (int i = 7; i < cellInfoNrs.size(); i++) {
                                    cellInfoNrs.remove(i);
                                }
                            }
                        }//CellInfoNr
                    }
                }
            }
        }
        catch (Exception ex){

            String s = ex.getMessage();
        }
    }

    private void addDynamicCellTable() {

        try {
            CellDisplayAdapterModel displayAdptModel = lstDisplay.get(0);
            if (displayAdptModel != null) {
                txt_cell_name.setText(displayAdptModel.getCellTypeName());
                HashMap<String, String[][]> cellMap = displayAdptModel.getCellMap();
                if (cellMap != null) {

                    String key = displayAdptModel.getCellType();
                    String[][] dtable = cellMap.get(key);
                    int rows = displayAdptModel.getRows();
                    int columns = displayAdptModel.getColumns();
                    if (dtable != null) {

                        for (int row = 0; row < rows; row++) {

                            TableRow tr = new TableRow(this);
                            tr.setLayoutParams(new TableLayout.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT));

                            for (int column = 0; column < columns; column++) {

                                TextView lblhead = new TextView(this);
                                lblhead.setWidth(TableLayout.LayoutParams.WRAP_CONTENT);
                                final float scale = this.getResources().getDisplayMetrics().density;
                                int pixels = (int) (40 * scale + 0.5f);
                                lblhead.setHeight(pixels);
                                lblhead.setText(dtable[row][column]);
                                lblhead.setTextColor(Color.parseColor("#434655"));          // part2// part2
                                lblhead.setPadding(5, 5, 5, 5);
                                if (row == 0) {//header row
                                    if (column == 0) {
                                        lblhead.setBackgroundResource(R.drawable.cell_left_top_curve_bk);
                                    } else if (column == columns - 1) {
                                        lblhead.setBackgroundResource(R.drawable.cell_top_right_curve_bk);
                                    } else {
                                        lblhead.setBackgroundResource(R.drawable.cell_inter_bk);
                                    }
                                } else if (row == rows - 1) {

                                    if (column == 0) {
                                        lblhead.setBackgroundResource(R.drawable.cell_bottom_left_curve_bk_value);
                                    } else if (column == columns - 1) {
                                        lblhead.setBackgroundResource(R.drawable.cell_bottom_right_curve_value);
                                    } else {
                                        lblhead.setBackgroundResource(R.drawable.cell_inter_value);
                                    }
                                } else {
                                    if (column == 0) {
                                        //lblhead.setBackgroundResource(R.drawable.cell_inter_value_left);
                                    } else if (column == columns - 1) {
                                        lblhead.setBackgroundResource(R.drawable.cell_inter_value_right);
                                    } else {
                                        lblhead.setBackgroundResource(R.drawable.cell_inter_value);
                                    }
                                }
                                tr.addView(lblhead,new TableLayout.LayoutParams(
                                        TableLayout.LayoutParams.MATCH_PARENT,                    //part4
                                        TableLayout.LayoutParams.WRAP_CONTENT));
                            }//for inner
                            tbl_cell_params.addView(tr);
                        }//for

                        String s = "dkfkdkfdkf";
                    }
                }
            }

        } catch (Exception ex) {

            String s = ex.getMessage();
            String s1 = "hdkdfkjdkjf";
        }
    }


    private void groupNeighbouringCells(){
        try {

            String key = "";
            if (gsmCells != null && gsmCells.size() > 0) {
                String[] gmsParams = new String[]{"ARFCN","BSIC","TIMINGADV","ASSU","CID","LAC","MCC","MNC","DBM","LEVEL"};
                key="G_N";
                int rowSize = gsmCells.size()+1;
                int columnSize = gmsParams.length;
                String[][] gsmTable = new String[rowSize][columnSize];
                for(int i =0;i<columnSize;i++){
                    gsmTable[0][i]  = gmsParams[i];
                }//i

                for(int gsmIndex = 0; gsmIndex<gsmCells.size();gsmIndex++){
                    final CellSignalStrengthGsm gsmSignal = ((CellInfoGsm) gsmCells.get(gsmIndex)).getCellSignalStrength();
                    final CellIdentityGsm identityGsm = ((CellInfoGsm) gsmCells.get(gsmIndex)).getCellIdentity();
                    if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        gsmTable[gsmIndex + 1][0] = identityGsm.getArfcn() + "";
                        gsmTable[gsmIndex + 1][1] = identityGsm.getBsic() + "";
                    }
                    else {
                        gsmTable[gsmIndex + 1][0] = "";
                        gsmTable[gsmIndex + 1][1] = "";
                    }
                    if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {

                        gsmTable[gsmIndex + 1][2] = gsmSignal.getTimingAdvance()+"";
                    }
                    else {
                        gsmTable[gsmIndex + 1][2] = "";

                    }
                    gsmTable[gsmIndex + 1][3] = gsmSignal.getAsuLevel() + "";
                    gsmTable[gsmIndex + 1][4] = identityGsm.getCid() + "";
                    gsmTable[gsmIndex + 1][5] = identityGsm.getLac() + "";
                    gsmTable[gsmIndex + 1][6] = identityGsm.getMcc() + "";
                    gsmTable[gsmIndex + 1][7] = identityGsm.getMnc() + "";
                    gsmTable[gsmIndex + 1][8] = gsmSignal.getDbm() + "";
                    gsmTable[gsmIndex + 1][9] = gsmSignal.getLevel() + "";
                }//Iterate through gsm cells

                CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                cellMap = new HashMap<String,String[][]>();
                cellMap.put(key,gsmTable);
                model.setCellType(key);
                model.setCellTypeName("2G Measurements");
                model.setRows(rowSize);
                model.setColumns(columnSize);
                model.setCellMap(cellMap);
                lstDisplay.add(model);


            }//gsmFor
            if(lteCells!=null && lteCells.size() > 0){

                String[] lteParams = new String[]{"CI","EARFCN","MCC","MNC","BANDWIDTH",
                        "PCI","TAC","ASU","CQI","DBM","LEVEL","RSRP","RSRQ","RSSNR","TIMINGADV"};

                //LTE
                int rowSize = lteCells.size()+1;
                int columnSize = lteParams.length;
                key = "L_N";
                String[][] lteTable = new String[rowSize][columnSize];
                for(int i =0;i<columnSize;i++){
                    lteTable[0][i]  = lteParams[i];
                }//i
                for(int lteIndex =0;lteIndex<lteCells.size();lteIndex++) {
                    final CellSignalStrengthLte lteSignal = ((CellInfoLte) lteCells.get(lteIndex)).getCellSignalStrength();
                    final CellIdentityLte lteIdentity = ((CellInfoLte) lteCells.get(lteIndex)).getCellIdentity();

                    lteTable[lteIndex+1][0] =  lteIdentity.getCi()+"";

                    if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        lteTable[lteIndex + 1][1] = lteIdentity.getEarfcn() + "";
                    }
                    else {
                        lteTable[lteIndex + 1][1] = "";
                    }

                    if (Build.VERSION.SDK_INT >= 28) {
                        lteTable[lteIndex + 1][2] =lteIdentity.getMccString() + "";
                        lteTable[lteIndex + 1][2] =lteIdentity.getMncString() + "";
                    } else {

                        lteTable[lteIndex + 1][2] =lteIdentity.getMcc() + "";
                        lteTable[lteIndex + 1][2] =lteIdentity.getMnc() + "";
                    }
                    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                        lteTable[lteIndex + 1][3] =lteIdentity.getBandwidth() + "";
                    }
                    else{
                        lteTable[lteIndex + 1][3] ="";
                    }
                    lteTable[lteIndex + 1][4] =lteIdentity.getPci() + "";
                    lteTable[lteIndex + 1][5] =lteIdentity.getTac() + "";
                    lteTable[lteIndex + 1][6] =lteSignal.getAsuLevel() + "";

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        lteTable[lteIndex + 1][7] =lteSignal.getCqi() + "";
                    }
                    else{
                        lteTable[lteIndex + 1][7] ="";
                    }
                    lteTable[lteIndex + 1][8] =lteSignal.getDbm() + "";
                    lteTable[lteIndex + 1][9] =lteSignal.getLevel() + "";
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                        lteTable[lteIndex + 1][10] =lteSignal.getRsrp() + "";
                        lteTable[lteIndex + 1][11] =lteSignal.getRsrq() + "";
                        lteTable[lteIndex + 1][12] =lteSignal.getRsrq() + "";
                        lteTable[lteIndex + 1][13] =lteSignal.getRssnr() + "";
                    }
                    else{
                        lteTable[lteIndex + 1][10] ="";
                        lteTable[lteIndex + 1][11] ="";
                        lteTable[lteIndex + 1][12] ="";
                        lteTable[lteIndex + 1][13] ="";
                    }
                    lteTable[lteIndex + 1][14] =lteSignal.getTimingAdvance() + "";
                }//

                CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                cellMap = new HashMap<String,String[][]>();
                cellMap.put(key,lteTable);
                model.setCellType(key);
                model.setCellTypeName("4G Measurements");
                model.setRows(rowSize);
                model.setColumns(columnSize);
                model.setCellMap(cellMap);
                lstDisplay.add(model);
                lteCells.clear();
            }

            if(wCdmaCells!=null & wCdmaCells.size()!=0){
                String[] wCdmaParams = new String[]{"CID","LAC","MCC","MNC", "PSC","UARFCN"};
                int rowSize = wCdmaCells.size()+1;
                int columnSize =wCdmaParams.length;
                key = "W_N";
                String[][] wCdmaTable = new String[rowSize][columnSize];
                for(int i =0;i<columnSize;i++){
                    wCdmaTable[0][i]  = wCdmaParams[i];
                }//i

                for(int cdMaIndex =0;cdMaIndex<wCdmaCells.size();cdMaIndex++){
                    final CellSignalStrengthWcdma wcdmaSignal = ((CellInfoWcdma) wCdmaCells.get(cdMaIndex)).getCellSignalStrength();
                    final CellIdentityWcdma wcdmaIdentity = ((CellInfoWcdma)  wCdmaCells.get(cdMaIndex)).getCellIdentity();

                    wCdmaTable[cdMaIndex+1][0] = wcdmaIdentity.getCid()+"";
                    wCdmaTable[cdMaIndex+1][1] = wcdmaIdentity.getLac()+"";

                    if (Build.VERSION.SDK_INT >= 28) {
                        wCdmaTable[cdMaIndex+1][2] = wcdmaIdentity.getMccString()+"";
                        wCdmaTable[cdMaIndex+1][3] = wcdmaIdentity.getMncString()+"";
                    } else {
                        wCdmaTable[cdMaIndex+1][2] = wcdmaIdentity.getMcc()+"";
                        wCdmaTable[cdMaIndex+1][3] = wcdmaIdentity.getMnc()+"";
                    }
                    wCdmaTable[cdMaIndex+1][4] = wcdmaIdentity.getPsc()+"";
                    if (android.os.Build.VERSION.SDK_INT > Build.VERSION_CODES.M) {
                        wCdmaTable[cdMaIndex+1][5] = wcdmaIdentity.getUarfcn()+"";
                    }
                    else{
                        wCdmaTable[cdMaIndex+1][5] = "";
                    }
                }//for

                CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                cellMap = new HashMap<String,String[][]>();
                cellMap.put(key,wCdmaTable);
                model.setCellType(key);
                model.setCellTypeName("3G Measurements");
                model.setRows(rowSize);
                model.setColumns(columnSize);
                model.setCellMap(cellMap);
                lstDisplay.add(model);
            }
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                if (cellInfoNrs != null && cellInfoNrs.size() > 0) {
                    String[] FivGParams = new String[]{"MCC", "MNC", "NCI", "NRARFCN", "PCI", "CSIRSRP",
                            "CSISINR","DBM","LEVEL","SSRSRP","SSRSRQ","SSSINR"};
                    int rowSize = cellInfoNrs.size()+1;
                    int columnSize = 6;
                    key = "5_N";
                    String[][] fiveGTable = new String[rowSize][columnSize];
                    for(int i =0;i<columnSize;i++){
                        fiveGTable[0][i]  = FivGParams[i];
                    }//i
                    for(int fiveGIndex =0;fiveGIndex<cellInfoNrs.size();fiveGIndex++){
                        final CellIdentityNr fiveGIdentity =
                                (CellIdentityNr) ((CellInfoNr) cellInfoNrs.get(fiveGIndex)).getCellIdentity();
                        final CellSignalStrengthNr fiveGISignal =
                                (CellSignalStrengthNr) ((CellInfoNr) cellInfoNrs.get(fiveGIndex)).getCellSignalStrength();

                        fiveGTable[fiveGIndex+1][0] = fiveGIdentity.getMccString() + "";
                        fiveGTable[fiveGIndex+1][1] = fiveGIdentity.getMncString() + "";
                        fiveGTable[fiveGIndex+1][2] = fiveGIdentity.getNci() + "";
                        fiveGTable[fiveGIndex+1][3] = fiveGIdentity.getNrarfcn() + "";
                        fiveGTable[fiveGIndex+1][4] = fiveGIdentity.getPci() + "";

                        fiveGTable[fiveGIndex+1][5] = fiveGISignal.getCsiRsrp() + "";
                        fiveGTable[fiveGIndex+1][6] =  fiveGISignal.getCsiSinr() + "";
                        fiveGTable[fiveGIndex+1][7] = fiveGISignal.getDbm() + "";
                        fiveGTable[fiveGIndex+1][8] =  fiveGISignal.getLevel() + "";
                        fiveGTable[fiveGIndex+1][9] = fiveGISignal.getSsRsrp() + "";
                        fiveGTable[fiveGIndex+1][10] =fiveGISignal.getSsRsrq() + "";
                        fiveGTable[fiveGIndex+1][11] = fiveGISignal.getSsSinr() + "";
                    }

                    CellDisplayAdapterModel model = new CellDisplayAdapterModel();
                    cellMap = new HashMap<String,String[][]>();
                    cellMap.put(key,fiveGTable);
                    model.setCellType("5_N");
                    model.setCellTypeName("5G Measurements");
                    model.setRows(rowSize);
                    model.setColumns(columnSize);
                    model.setCellMap(cellMap);
                    lstDisplay.add(model);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }//groupNeighbouringCells


    public  boolean checkAndRequestPermissionsForMesure(Activity thisCtx) {
        int phone = ContextCompat.checkSelfPermission(thisCtx, Manifest.permission.READ_PHONE_STATE);
        int storage = ContextCompat.checkSelfPermission(thisCtx, android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int loc = ContextCompat.checkSelfPermission(thisCtx, android.Manifest.permission.ACCESS_COARSE_LOCATION);
        int loc2 = ContextCompat.checkSelfPermission(thisCtx, android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (phone != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (loc2 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (loc != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty())
        {
            ActivityCompat.requestPermissions(thisCtx,listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]),CommonUtility.REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    public static boolean isHostAvailable() {



        try {
            HttpURLConnection.setFollowRedirects(false);
            // note : you may also need
            // HttpURLConnection.setInstanceFollowRedirects(false)
            HttpURLConnection con = (HttpURLConnection) new URL("http://www.google.com")
                    .openConnection();
            con.setRequestMethod("HEAD");
            return (con.getResponseCode() == HttpURLConnection.HTTP_OK);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private class AsyncTaskExample extends AsyncTask<Void, Void, Boolean> {
        @Override
        protected Boolean doInBackground(Void... voids) {
            return(isHostAvailable());
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            super.onPostExecute(aBoolean);
            if(aBoolean) {
                txt_ping.setText("Ping: Success");
                //pingstatus.setTextColor(Color.GREEN);
                String source =  txt_ping.getText().toString();
                int start = source.indexOf(':') + 2;
                int ent = source.length();
                Spannable wordtoSpan = new SpannableString(
                        txt_ping.getText().toString());


                wordtoSpan.setSpan(new ForegroundColorSpan(Color.parseColor("#3CB64A")),
                        start,ent, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

                txt_ping.setText(wordtoSpan);
            }
            else{
                txt_ping.setText("Ping: Failed");
                //pingstatus.setTextColor(Color.RED);
            }
        }
    }

    public static final Spannable getColoredString(Context context, CharSequence text,String color) {
        Spannable spannable = new SpannableString(text);
        spannable.setSpan(new ForegroundColorSpan(Color.parseColor(color)), 0, spannable.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return spannable;
    }

    @Override
    protected void onStop() {
        if(handler!=null) {
            handler.removeCallbacks(runnableCode);
        }
        threadStart = false;
        super.onStop();
    }
}//Activity
