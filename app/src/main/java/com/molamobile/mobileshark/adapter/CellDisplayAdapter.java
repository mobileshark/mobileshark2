package com.molamobile.mobileshark.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.DrawableRes;
import androidx.annotation.NonNull;
import androidx.core.content.res.ResourcesCompat;
import androidx.recyclerview.widget.RecyclerView;
import androidx.transition.TransitionManager;

import com.molamobile.mobileshark.R;
import com.molamobile.mobileshark.models.CellDisplayAdapterModel;

import java.util.HashMap;
import java.util.List;


/*Created By Ajish Dharman on 10-May-2020
 *
 *
 */
public class CellDisplayAdapter extends RecyclerView.Adapter<CellDisplayAdapter.CellDisplayAdapterViewHolder>  {

     public  int mExpandedPosition = -1;
     public int prev_expanded=-1;
     public int previmage =1;
    List<CellDisplayAdapterModel> lstCellDisplayAdaterModel;
    private RecyclerView recyclerView = null;
    Context ctx;
    private boolean isExpanded=false;

    boolean tableRedrawRequired = true;
    boolean isOnrefresh=false;
    boolean expandpressed =false;



    public CellDisplayAdapter( List<CellDisplayAdapterModel> displayAdaterList, Context ctx) {

        this.lstCellDisplayAdaterModel = displayAdaterList;
        this.ctx = ctx;
        tableRedrawRequired = true;

    }
    public void updateCellMeasure(CellDisplayAdapterModel cdm){

       for(int i=0;i<lstCellDisplayAdaterModel.size();i++){

           if(cdm.getCellType().equals(lstCellDisplayAdaterModel.get(i).getCellType())){



               if(!cdm.isGroupEmpty()) {
                   lstCellDisplayAdaterModel.set(i, cdm);
                   if(tableRedrawRequired) {
                       notifyItemChanged(i);
                   }
               }

           }
       }
    }//updatePrimaryCellMeasure

    public void insertCellMeasure(CellDisplayAdapterModel cdm){

        tableRedrawRequired=true;
        if(cdm.getCellType().equals("G_S") ||
                cdm.getCellType().equals("L_S") ||cdm.getCellType().equals("W_S") || cdm.getCellType().equals("5_S")){
            lstCellDisplayAdaterModel.add(0,cdm);
            notifyItemInserted(0);
        }
        else if(cdm.getCellType().equals("5_N")){
            lstCellDisplayAdaterModel.add(1,cdm);
            notifyItemInserted(1);
        }  else if(cdm.getCellType().equals("L_N")){
            lstCellDisplayAdaterModel.add(2,cdm);
            notifyItemInserted(2);
        } else if(cdm.getCellType().equals("W_N")){
            lstCellDisplayAdaterModel.add(3,cdm);
            notifyItemInserted(3);
        } else if(cdm.getCellType().equals("G_N")){
        lstCellDisplayAdaterModel.add(4,cdm);
        notifyItemInserted(4);
    }

    }

    public void addCellInfoList(List<CellDisplayAdapterModel> displayAdaterList){

        this.lstCellDisplayAdaterModel = displayAdaterList;
    }

    @NonNull
    @Override
    public CellDisplayAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.serving_cell_row_item_dynamic, parent, false);


        return new CellDisplayAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final CellDisplayAdapterViewHolder holder, final int position) {


        try{


          //  ( (RecyclerView.ViewHolder)holder).setIsRecyclable(false);

            if(lstCellDisplayAdaterModel!=null){


                holder.txt_cell_name.setText(lstCellDisplayAdaterModel.get(position).getCellTypeName());
                if(lstCellDisplayAdaterModel.get(position).isGroupEmpty()){

                    holder.emptyview.setVisibility(View.VISIBLE);
                    holder.tbl_cell_params.setVisibility(View.GONE);
                    //holder.imgsignalstrength2.setVisibility(View.INVISIBLE);
                    //SignalStrength

                }
                else{
                    holder.tbl_cell_params.setVisibility(View.VISIBLE);
                    holder.emptyview.setVisibility(View.GONE);
                    //holder.imgsignalstrength2.setVisibility(View.VISIBLE);
                    //SignalStrength

                }



                if(tableRedrawRequired) {
                    if(!isOnrefresh) {
                        holder.tbl_cell_params.removeAllViews();
                        addDynamicCellTable(holder, position);
                    }
                }

                if (position == mExpandedPosition) {
                    holder.img_collapse_btn.setBackgroundResource(R.drawable.ic_collapse);
                } else {
                    holder.img_collapse_btn.setBackgroundResource(R.drawable.ic_expand_button);
                }

                final boolean isExpanded = position == mExpandedPosition;

                holder.rellay_details.setVisibility(isExpanded ? View.VISIBLE : View.GONE);

                if (lstCellDisplayAdaterModel.get(position).getCellType().contains("_S")) {
                    holder.rellay_details.setVisibility(View.VISIBLE);
                }


                holder.img_collapse_btn.setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {

                        mExpandedPosition = isExpanded ? -1 : holder.getAdapterPosition();

                        tableRedrawRequired = false;
                        notifyItemChanged(holder.getAdapterPosition());
                        tableRedrawRequired=true;


                        return true;
                    }
                });

                //notifyItemChanged(position);
            }


        }
        catch(Exception ex){


        }

    }

    @Override
    public void onAttachedToRecyclerView(@NonNull RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    private void addDynamic(CellDisplayAdapterViewHolder holder, int pos){

       // TableLayout t1;

       // TableLayout tl = holder.tbl_cell_params.addView(tr);

        CellDisplayAdapterModel displayAdptModel = lstCellDisplayAdaterModel.get(pos);
        TableRow tr_head = new TableRow(ctx);
        tr_head.setBackgroundColor(Color.parseColor("#F5F5F5"));        // part1
        tr_head.setLayoutParams(new TableLayout.LayoutParams(
                TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT));
        TextView label_hello = new TextView(ctx);
        label_hello.setText(displayAdptModel.getCellType());
        label_hello.setTextColor(Color.WHITE);          // part2
        label_hello.setPadding(5, 5, 5, 5);
        tr_head.addView(label_hello);// add the column to the table row here

        TextView label_android = new TextView(ctx);    // part3
        //  label_android.setId(21);// define id that must be unique
        label_android.setText(displayAdptModel.getCellTypeName()); // set the text for the header
        label_android.setTextColor(Color.WHITE); // set the color
        label_android.setPadding(5, 5, 5, 5); // set the padding (if required)
        tr_head.addView(label_android); // add the column to the table row here

        holder.tbl_cell_params.addView(tr_head, new TableLayout.LayoutParams(
                TableLayout.LayoutParams.MATCH_PARENT,                    //part4
                TableLayout.LayoutParams.WRAP_CONTENT));
    }

    private void addDynamicCellTable(final CellDisplayAdapterViewHolder holder, final int pos) {

        try {

            isOnrefresh=true;
                CellDisplayAdapterModel displayAdptModel = lstCellDisplayAdaterModel.get(pos);
                if (displayAdptModel != null) {
                  //  holder.txt_cell_name.setText(displayAdptModel.getCellTypeName());
                    if(displayAdptModel.isGroupEmpty()){
                        holder.imgsignalstrength2.setVisibility(View.INVISIBLE);
                    }

                    HashMap<String, String[][]> cellMap = displayAdptModel.getCellMap();
                    if (cellMap != null) {

                        String key = displayAdptModel.getCellType();


                        if(key.contains("_S")){
                            holder.imgsignalstrength2.setVisibility(View.GONE);
                            holder.img_collapse_btn.setVisibility(View.GONE);
                            holder.imgsignalstrength.setVisibility(View.VISIBLE);
                            if(displayAdptModel.getMaxlevel()==1){
                                holder.imgsignalstrength.setBackgroundResource(R.drawable.ic_signal_level_one);
                            }
                            else if(displayAdptModel.getMaxlevel()==2){
                                holder.imgsignalstrength.setBackgroundResource(R.drawable.ic_signal_two);
                            }
                            else if(displayAdptModel.getMaxlevel()==3){
                                holder.imgsignalstrength.setBackgroundResource(R.drawable.ic_ic_signal_three);
                            }
                            else if(displayAdptModel.getMaxlevel()==4){
                                holder.imgsignalstrength.setBackgroundResource(R.drawable.ic_signal_level_4);
                            }
                            else{
                                holder.imgsignalstrength.setBackgroundResource(R.drawable.ic_signal_level_one);
                            }
                            if(displayAdptModel.isGroupEmpty()){
                                holder.imgsignalstrength2.setVisibility(View.GONE);
                                holder.imgsignalstrength.setVisibility(View.INVISIBLE);
                            }
                        }
                        else if(key.contains("_N")){
                            holder.imgsignalstrength2.setVisibility(View.VISIBLE);
                            holder.img_collapse_btn.setVisibility(View.VISIBLE);
                            holder.imgsignalstrength.setVisibility(View.GONE);

                            if(displayAdptModel.getMaxlevel()==1){
                                holder.imgsignalstrength2.setBackgroundResource(R.drawable.ic_signal_level_one);
                            }
                            else if(displayAdptModel.getMaxlevel()==2){
                                holder.imgsignalstrength2.setBackgroundResource(R.drawable.ic_signal_two);
                            }
                            else if(displayAdptModel.getMaxlevel()==3){
                                holder.imgsignalstrength2.setBackgroundResource(R.drawable.ic_ic_signal_three);
                            }
                            else if(displayAdptModel.getMaxlevel()==4){
                                holder.imgsignalstrength2.setBackgroundResource(R.drawable.ic_signal_level_4);
                            }
                            else{
                                holder.imgsignalstrength2.setBackgroundResource(R.drawable.ic_signal_level_one);
                            }

                            if(displayAdptModel.isGroupEmpty()){
                                holder.imgsignalstrength2.setVisibility(View.INVISIBLE);
                                holder.imgsignalstrength.setVisibility(View.GONE);
                            }

                        }

                    /* holder.img_collapse_btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                final boolean visibility = holder.rellay_details.getVisibility()==View.VISIBLE;

                                if (!visibility)
                                {
                                    holder.itemView.setActivated(true);
                                    holder.rellay_details.setVisibility(View.VISIBLE);
                                    holder.img_collapse_btn.setBackgroundResource(R.drawable.ic_collapse);
                                   if(prev_expanded!=-1 && prev_expanded!=pos)
                                    {
                                        recyclerView.findViewHolderForLayoutPosition(prev_expanded).itemView.setActivated(false);
                                        recyclerView.findViewHolderForLayoutPosition(prev_expanded).
                                                itemView.findViewById(R.id.rellay_details).setVisibility(View.GONE);
                                    }
                                    prev_expanded = pos;
                                }
                                else
                                {
                                    holder.itemView.setActivated(false);
                                    holder.rellay_details.setVisibility(View.GONE);
                                    holder.img_collapse_btn.setBackgroundResource(R.drawable.ic_expand_button);
                                }
                                TransitionManager.beginDelayedTransition(recyclerView);
                            }
                        });*/


                        //Typeface typeface =ctx.getResources().getFont(R.font.jost_semi_bold);
                        //or to support all versions use
                        Typeface typeface = ResourcesCompat.getFont(ctx, R.font.jost_semi_bold);
                        Typeface typefacereg = ResourcesCompat.getFont(ctx, R.font.jost_regular);

                        String[][] dtable = cellMap.get(key);
                        int rows = displayAdptModel.getRows();
                        int columns = displayAdptModel.getColumns();
                        if (dtable != null) {

                            for (int row = 0; row < rows; row++) {

                                TableRow tr = new TableRow(ctx);
                                tr.setLayoutParams(new TableLayout.LayoutParams(
                                        ViewGroup.LayoutParams.MATCH_PARENT,
                                        ViewGroup.LayoutParams.WRAP_CONTENT));

                                for (int column = 0; column < columns; column++) {

                                    TextView lblhead = new TextView(ctx);
                                    lblhead.setText(dtable[row][column]);
                                    lblhead.setTextColor(Color.parseColor("#434655"));          // part2
                                    lblhead.setPadding(25, 25, 25, 25);
                                    lblhead.setGravity(Gravity.CENTER);


                                    if (row == 0) {//header row
                                       /* if (column == 0) {
                                            lblhead.setBackgroundResource(R.drawable.full_box_bk);
                                        } else if (column == columns - 1) {
                                            lblhead.setBackgroundResource(R.drawable.cell_bottom_right_curve_value);
                                        } else {
                                            lblhead.setBackgroundResource(R.drawable.cell_inter_bk);
                                        }*/
                                        lblhead.setTypeface(typeface);
                                        lblhead.setTextSize(13);
                                       if(column==0) {
                                           lblhead.setBackgroundResource(R.drawable.full_box_bk);
                                       }
                                       else{
                                           lblhead.setBackgroundResource(R.drawable.left_open_box);
                                       }
                                    } else  {

                                      /*  if (column == 0) {
                                            lblhead.setBackgroundResource(R.drawable.cell_bottom_left_curve_bk_value);
                                        } else if (column == columns - 1) {
                                            lblhead.setBackgroundResource(R.drawable.cell_bottom_right_curve_value);
                                        } else {
                                            lblhead.setBackgroundResource(R.drawable.cell_inter_value);
                                        }
                                    } else {
                                        if (column == 0) {
                                            lblhead.setBackgroundResource(R.drawable.cell_inter_value_left);
                                        } else if (column == columns - 1) {
                                            lblhead.setBackgroundResource(R.drawable.cell_inter_value_right);
                                        } else {
                                            lblhead.setBackgroundResource(R.drawable.cell_inter_value);
                                        }
                                    }*/
                                        lblhead.setTypeface(typefacereg);
                                        lblhead.setTextSize(14);
                                      if(column==0){
                                          lblhead.setBackgroundResource(R.drawable.full_box_bk_white);
                                      }
                                      else{
                                          lblhead.setBackgroundResource(R.drawable.left_open_box_bk_white);
                                      }
                                       // lblhead.setBackgroundResource(R.drawable.cell_inter_value);
                                    }
                                    tr.addView(lblhead);
                                }//for inner
                                holder.tbl_cell_params.addView(tr, new TableLayout.LayoutParams(
                                        TableLayout.LayoutParams.MATCH_PARENT,                    //part4
                                        TableLayout.LayoutParams.WRAP_CONTENT));
                            }//for

                            String s = "dkfkdkfdkf";
                            isOnrefresh=false;

                        }
                    }
                }




        } catch (Exception ex){

            String s = ex.getMessage();
            String s1="hdkdfkjdkjf";
        }

   /* private void addDynamicRows(CellDisplayAdapterViewHolder holder){
        TableRow tr_head = new TableRow(ctx);
        // part1
        tr_head.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT));


        int position =0;
        for (String key: cellMap.keySet()) {
            //System.out.println("key : " + key);
            //System.out.println("value : " + map.get(key));

            TextView lblhead = new TextView(ctx);
            lblhead.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
            final float scale = ctx.getResources().getDisplayMetrics().density;
            int pixels = (int) (40 * scale + 0.5f);
            lblhead.setHeight(pixels);
            lblhead.setText(key);
            lblhead.setTextColor(Color.parseColor("#434655"));          // part2
            lblhead.setTextSize(Color.WHITE);          // part2
            lblhead.setPadding(5, 5, 5, 5);
            if(position ==0) {
                lblhead.setBackgroundResource(R.drawable.cell_left_top_curve_bk);
            }
            else if(position == cellMap.size()-1){
                lblhead.setBackgroundResource(R.drawable.cell_top_right_curve_bk);
            }
            else{
                lblhead.setBackgroundResource(R.drawable.cell_inter_bk);
            }
            tr_head.addView(lblhead);// add the column to the table row here
            holder.tbl_cell_params.addView(tr_head);
            position++;
        }
        position=0;
        for (String key: cellMap.keySet()) {
            //System.out.println("key : " + key);
            //System.out.println("value : " + map.get(key));

            TextView lblvalue = new TextView(ctx);
            lblvalue.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
            final float scale = ctx.getResources().getDisplayMetrics().density;
            int pixels = (int) (40 * scale + 0.5f);
            lblvalue.setHeight(pixels);
            lblvalue.setText(key);
            lblvalue.setTextColor(Color.parseColor("#434655"));          // part2
            lblvalue.setTextSize(Color.WHITE);          // part2
            lblvalue.setPadding(5, 5, 5, 5);
            if(position ==0) {
                lblvalue.setBackgroundResource(R.drawable.cell_bottom_left_curve_bk_value);
            }
            else if(position == cellMap.size()-1){
                lblvalue.setBackgroundResource(R.drawable.cell_bottom_right_curve_value);
            }
            else{
                lblvalue.setBackgroundResource(R.drawable.cell_inter_value);
            }
            tr_head.addView(lblvalue);// add the column to the table row here
            holder.tbl_cell_params.addView(tr_head);
            position++;
        }

        position =0;
    }*/

    }

    @Override
    public int getItemCount() {

        return lstCellDisplayAdaterModel.size();
    }

    public class CellDisplayAdapterViewHolder extends RecyclerView.ViewHolder {

        private TextView txt_cell_name;
        private ImageView imgsignalstrength;
        private TableLayout tbl_cell_params;
       // private ImageView img_expand_btn;
        private ImageView img_collapse_btn;
        private RelativeLayout rellay_details;
        private ImageView imgsignalstrength2;
        private TextView emptyview;

        public CellDisplayAdapterViewHolder(@NonNull View itemView) {
            super(itemView);
            this.txt_cell_name = (TextView) itemView.findViewById(R.id.txt_cell_name);
            this.imgsignalstrength = (ImageView) itemView.findViewById(R.id.imgsignalstrength);
            this.tbl_cell_params = (TableLayout) itemView.findViewById(R.id.tbl_cell_params);
           // this.img_expand_btn = (ImageView)itemView.findViewById(R.id.img_expand_btn);
            this.img_collapse_btn = (ImageView)itemView.findViewById(R.id.img_collapse_btn);
            this.rellay_details = (RelativeLayout) itemView.findViewById(R.id.rellay_details);
            this.imgsignalstrength = (ImageView)itemView.findViewById(R.id.imgsignalstrength);
            this.imgsignalstrength2 = (ImageView)itemView.findViewById(R.id.imgsignalstrength2);
            this.emptyview = (TextView)itemView.findViewById(R.id.txt_empty_view);


        }
    }
}
