package com.molamobile.mobileshark.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.molamobile.mobileshark.R;
import com.molamobile.mobileshark.models.CellDisplayAdapterModel;

import java.util.HashMap;
import java.util.List;

public class ListAdapter extends BaseAdapter {

    Context mContext;
    List<CellDisplayAdapterModel> lstModel;

    public ListAdapter(Context context, List<CellDisplayAdapterModel> lstDisplayAdapterModel) {
        super();
        this.lstModel = lstDisplayAdapterModel;
        this.mContext = context;
    }

    @Override
    public int getCount() {
        return lstModel.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        if (view == null) {
            LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            view = layoutInflater.inflate(R.layout.serving_cell_row_item_dynamic, null);

            addDynamicCellTable(view, position);
        }
        return view;
    }

    private void addDynamicCellTable(View view, int pos) {

        try {
            CellDisplayAdapterModel displayAdptModel = lstModel.get(pos);
            if (displayAdptModel != null) {

                TextView txt_cell_name = (TextView) view.findViewById(R.id.txt_cell_name);
                TableLayout tbl_cell_params = (TableLayout) view.findViewById(R.id.tbl_cell_params);
                txt_cell_name.setText(displayAdptModel.getCellTypeName());
                HashMap<String, String[][]> cellMap = displayAdptModel.getCellMap();
                if (cellMap != null) {

                    String key = displayAdptModel.getCellType();
                    String[][] dtable = cellMap.get(key);
                    int rows = displayAdptModel.getRows();
                    int columns = displayAdptModel.getColumns();
                    if (dtable != null) {

                        for (int row = 0; row < rows; row++) {

                            TableRow tr = new TableRow(mContext);
                            tr.setLayoutParams(new TableLayout.LayoutParams(
                                    ViewGroup.LayoutParams.MATCH_PARENT,
                                    ViewGroup.LayoutParams.WRAP_CONTENT));

                            for (int column = 0; column < columns; column++) {

                                TextView lblhead = new TextView(mContext);
                                lblhead.setWidth(ViewGroup.LayoutParams.WRAP_CONTENT);
                                final float scale = mContext.getResources().getDisplayMetrics().density;
                                int pixels = (int) (40 * scale + 0.5f);
                                lblhead.setHeight(pixels);
                                lblhead.setText(dtable[row][column]);
                                lblhead.setTextColor(Color.parseColor("#434655"));          // part2
                                lblhead.setTextSize(Color.WHITE);          // part2
                                lblhead.setPadding(5, 5, 5, 5);
                                if (row == 0) {//header row
                                    if (column == 0) {
                                        lblhead.setBackgroundResource(R.drawable.cell_left_top_curve_bk);
                                    } else if (column == columns - 1) {
                                        lblhead.setBackgroundResource(R.drawable.cell_top_right_curve_bk);
                                    } else {
                                        lblhead.setBackgroundResource(R.drawable.cell_inter_bk);
                                    }
                                } else if (row == rows - 1) {

                                    if (column == 0) {
                                        lblhead.setBackgroundResource(R.drawable.cell_bottom_left_curve_bk_value);
                                    } else if (column == columns - 1) {
                                        lblhead.setBackgroundResource(R.drawable.cell_bottom_right_curve_value);
                                    } else {
                                        lblhead.setBackgroundResource(R.drawable.cell_inter_value);
                                    }
                                } else {
                                    if (column == 0) {
                                        //lblhead.setBackgroundResource(R.drawable.cell_inter_value_left);
                                    } else if (column == columns - 1) {
                                        lblhead.setBackgroundResource(R.drawable.cell_inter_value_right);
                                    } else {
                                        lblhead.setBackgroundResource(R.drawable.cell_inter_value);
                                    }
                                }
                                tr.addView(lblhead);
                            }//for inner
                            tbl_cell_params.addView(tr);
                        }//for

                        String s = "dkfkdkfdkf";
                    }
                }
            }

        } catch (Exception ex) {

            String s = ex.getMessage();
            String s1 = "hdkdfkjdkjf";
        }

    }
}
