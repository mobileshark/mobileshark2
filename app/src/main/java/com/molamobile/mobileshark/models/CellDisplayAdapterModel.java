package com.molamobile.mobileshark.models;

import java.util.HashMap;

public class CellDisplayAdapterModel {

    private String cellType;
    private String cellTypeName;
    private String signalStrength;
    private int rows;
    private int columns;
    private HashMap<String,String[][]> cellMap;
    private boolean isGroupEmpty;
    private int maxlevel;


    public String getCellType() {
        return cellType;
    }

    public void setCellType(String cellType) {
        this.cellType = cellType;
    }

    public String getCellTypeName() {
        return cellTypeName;
    }

    public void setCellTypeName(String cellTypeName) {
        this.cellTypeName = cellTypeName;
    }

    public String getSignalStrength() {
        return signalStrength;
    }

    public void setSignalStrength(String signalStrength) {
        this.signalStrength = signalStrength;
    }

    public HashMap<String, String[][]> getCellMap() {
        return cellMap;
    }

    public void setCellMap(HashMap<String, String[][]> cellMap) {
        this.cellMap = cellMap;
    }

    public int getRows() {
        return rows;
    }

    public void setRows(int rows) {
        this.rows = rows;
    }

    public int getColumns() {
        return columns;
    }

    public void setColumns(int columns) {
        this.columns = columns;
    }

    public boolean isGroupEmpty() {
        return isGroupEmpty;
    }

    public void setGroupEmpty(boolean groupEmpty) {
        isGroupEmpty = groupEmpty;
    }

    public int getMaxlevel() {
        return maxlevel;
    }

    public void setMaxlevel(int maxlevel) {
        this.maxlevel = maxlevel;
    }
}
