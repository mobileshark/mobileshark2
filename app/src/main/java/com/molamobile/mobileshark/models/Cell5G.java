package com.molamobile.mobileshark.models;

/*Created By Ajish Dharman on 05-May-2020
 *
 *
 */public class Cell5G {

     //Information to represent a unique NR(New Radio 5G) cell.
    //
    //Summary
    int[] bands; //Get bands of the cell Reference: TS 38.101-1 table 5.2-1 Reference: TS 38.101-2 table 5.2-1

    String mccString; //Mobile Country Code in string format, or null if unknown.

    String mncString;          //Mobile Network Code in string fomrat, or null if unknown.

    long nci; //Get the NR(New Radio 5G) Cell Identity.


    int nrarfcn;
   // Get the New Radio Absolute Radio Frequency Channel Number. Reference: 3GPP TS 38.101-1
   // section 5.4.2.1 NR-ARFCN and channel raster. Reference: 3GPP TS 38.101-2 section 5.4.2.1 NR-ARFCN and channel raster.

    int pci;
    //Get the physical cell id.
    //Integer value in range [0, 1007] or CellInfo#UNAVAILABLE if unknown. Value is between 0 and 1007 inclusive


    public int[] getBands() {
        return bands;
    }

    public void setBands(int[] bands) {
        this.bands = bands;
    }

    public String getMccString() {
        return mccString;
    }

    public void setMccString(String mccString) {
        this.mccString = mccString;
    }

    public String getMncString() {
        return mncString;
    }

    public void setMncString(String mncString) {
        this.mncString = mncString;
    }

    public long getNci() {
        return nci;
    }

    public void setNci(long nci) {
        this.nci = nci;
    }

    public int getNrarfcn() {
        return nrarfcn;
    }

    public void setNrarfcn(int nrarfcn) {
        this.nrarfcn = nrarfcn;
    }

    public int getPci() {
        return pci;
    }

    public void setPci(int pci) {
        this.pci = pci;
    }


    //SignalSTrength;

    int asu;
   // Get the RSRP in ASU. Asu is calculated based on 3GPP RSRP. Refer to 3GPP 27.007 (Ver 10.3.0) Sec 8.69
  // RSRP in ASU 0..97, 255, or UNAVAILABLE

    int csiRsrp;
   // Reference: 3GPP TS 38.215. Range: -140 dBm to -44 dBm.
   // CSI reference signal received power, CellInfo#UNAVAILABLE means unreported value.

    int csiSinr;
    //Reference: 3GPP TS 38.215. Range: -20 dB to -3 dB.
    //CSI reference signal received quality, CellInfo#UNAVAILABLE means unreported value.

    int dbm;
    //Get the SS-RSRP as dBm value -140..-44dBm or CellInfo#UNAVAILABLE.

    int level;
    //Retrieve an abstract level value for the overall signal quality.
    //Value is between SIGNAL_STRENGTH_NONE_OR_UNKNOWN and SIGNAL_STRENGTH_GREAT inclusive

    int ssRsrp;
    //SS reference signal received power, CellInfo#UNAVAILABLE means unreported value.
    //Reference: 3GPP TS 38.215. Range: -140 dBm to -44 dBm.

    int SsRsrq;
    //Reference: 3GPP TS 38.215. Range: -20 dB to -3 dB.
    //SS reference signal received quality, CellInfo#UNAVAILABLE means unreported value.

    int ssSinr;
    //Reference: 3GPP TS 38.215 Sec 5.1.*, 3GPP TS 38.133 10.1.16.1 Range: -23 dB to 40 dB
    //SS signal-to-noise and interference ratio, CellInfo#UNAVAILABLE means unreported value.


    public int getAsu() {
        return asu;
    }

    public void setAsu(int asu) {
        this.asu = asu;
    }

    public int getCsiRsrp() {
        return csiRsrp;
    }

    public void setCsiRsrp(int csiRsrp) {
        this.csiRsrp = csiRsrp;
    }

    public int getCsiSinr() {
        return csiSinr;
    }

    public void setCsiSinr(int csiSinr) {
        this.csiSinr = csiSinr;
    }

    public int getDbm() {
        return dbm;
    }

    public void setDbm(int dbm) {
        this.dbm = dbm;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getSsRsrp() {
        return ssRsrp;
    }

    public void setSsRsrp(int ssRsrp) {
        this.ssRsrp = ssRsrp;
    }

    public int getSsRsrq() {
        return SsRsrq;
    }

    public void setSsRsrq(int ssRsrq) {
        SsRsrq = ssRsrq;
    }

    public int getSsSinr() {
        return ssSinr;
    }

    public void setSsSinr(int ssSinr) {
        this.ssSinr = ssSinr;
    }


    public String converToString(){

        String s="";

        s = "mccString : "+this.mccString+"\n"+
                "mncString : "+this.mncString+"\n"+
                "nci : "+this.mncString+"\n"+
                "nrarfcn : "+this.nrarfcn+"\n"+
                "pci : "+this.pci+"\n"+
                "asu : "+this.asu+"\n"+
                "csiRsrp : "+this.csiRsrp+"\n"+
                "csiSinr : "+this.csiSinr+"\n"+
                "dbm : "+this.dbm+"\n"+
                "level : "+this.level+"\n"+
               "ssRsrp : "+this.ssRsrp+"\n"+
                "SsRsrq : "+this.SsRsrq+"\n"+

                "ssSinr : "+this.ssSinr+"\n";


        return s;

    }

    String netWork;
    String servingCell;

    public String getNetWork() {
        return netWork;
    }

    public void setNetWork(String netWork) {
        this.netWork = netWork;
    }

    public String getServingCell() {
        return servingCell;
    }

    public void setServingCell(String servingCell) {
        this.servingCell = servingCell;
    }

}
