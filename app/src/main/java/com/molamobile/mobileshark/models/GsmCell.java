package com.molamobile.mobileshark.models;

/*Created By Ajish Dharman on 04-May-2020
 *
 *
 */

//2G
public class GsmCell {

 //   CellIdentityGSM and CellSignalStrenghGSM

    String isRegistered;

    String timeStampInMills;

    int arfcn; 	//16-bit GSM Absolute RF Channel Number, UNAVAILABLE if unavailable. ==>Value: 2147483647 //Band Number

    int bsic;  // 6-bit Base Station Identity Code, UNAVAILABLE if unavailable.

    int cid;  //16-bit GSM Cell Identity described in TS 27.007, 0..65535, UNAVAILABLE if unavailable.

    int lac;  // 16-bit Location Area Code, 0..65535, UNAVAILABLE if unavailable.

    int mcc; // 3-digit Mobile Country Code, 0..999, UNAVAILABLE if unavailable.

    String mccString; //MCC String format null if not available

    int mnc; // 2 or 3-digit Mobile Network Code, 0..999, UNAVAILABLE if unavailable.

    String mncString; //Mobile Network Code in string format, null if unavailable.

    String mobileNetworkOperator; //a 5 or 6 character string (MCC+MNC), null if any field is unknown.

    int psc; // Primary Scrambling Code is not applicable to GSM.


    public String getIsRegistered() {
        return isRegistered;
    }

    public void setIsRegistered(String isRegistered) {
        this.isRegistered = isRegistered;
    }

    public String getTimeStampInMills() {
        return timeStampInMills;
    }

    public void setTimeStampInMills(String timeStampInMills) {
        this.timeStampInMills = timeStampInMills;
    }

    public int getArfcn() {
        return arfcn;
    }

    public void setArfcn(int arfcn) {
        this.arfcn = arfcn;
    }

    public int getBsic() {
        return bsic;
    }

    public void setBsic(int bsic) {
        this.bsic = bsic;
    }

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public int getLac() {
        return lac;
    }

    public void setLac(int lac) {
        this.lac = lac;
    }

    public int getMcc() {
        return mcc;
    }

    public void setMcc(int mcc) {
        this.mcc = mcc;
    }

    public String getMccString() {
        return mccString;
    }

    public void setMccString(String mccString) {
        this.mccString = mccString;
    }

    public int getMnc() {
        return mnc;
    }

    public void setMnc(int mnc) {
        this.mnc = mnc;
    }

    public String getMncString() {
        return mncString;
    }

    public void setMncString(String mncString) {
        this.mncString = mncString;
    }

    public String getMobileNetworkOperator() {
        return mobileNetworkOperator;
    }

    public void setMobileNetworkOperator(String mobileNetworkOperator) {
        if(mobileNetworkOperator==null){
            this.mobileNetworkOperator="";
        }
        else {
            this.mobileNetworkOperator = mobileNetworkOperator;
        }
    }

    public int getPsc() {
        return psc;
    }

    public void setPsc(int psc) {
        this.psc = psc;
    }



    //Signal Strengh Parameters Derived from SignalStrength Class

    int assuLevel; //Received Signal strength in asu

    int bitErrorRate; //Bit Error Rate

    int dbB; // Get Signal Strengh in DBM

    int level; // Overall Signal Quality abstract value

    int rssi; // Received signal strength

    int timingAdvance; // Timing Advance




    public int getAssuLevel() {
        return assuLevel;
    }

    public void setAssuLevel(int assuLevel) {
        this.assuLevel = assuLevel;
    }

    public int getBitErrorRate() {
        return bitErrorRate;
    }

    public void setBitErrorRate(int bitErrorRate) {
        this.bitErrorRate = bitErrorRate;
    }

    public int getDbB() {
        return dbB;
    }

    public void setDbB(int dbB) {
        this.dbB = dbB;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public int getTimingAdvance() {
        return timingAdvance;
    }

    public void setTimingAdvance(int timingAdvance) {
        this.timingAdvance = timingAdvance;
    }


    public String convertString(){



        String s = "arfcn = "+this.arfcn+ "\n"+
                "bsic = "+this.bsic+ "\n"+
                "cid = "+this.cid+ "\n"+
                "lac = "+this.lac+ "\n"+
                "mcc = "+this.mcc+ "\n"+
                "mnc = "+this.mnc+ "\n"+
                "mobileNetworkOperator = "+this.mobileNetworkOperator+ "\n"+
                "psc = NA"+ "\n"+
                "assuLevel = "+this.assuLevel+ "\n"+
                "bitErrorRate = "+this.bitErrorRate+ "\n"+
                "dbB = "+this.dbB+ "\n"+
                "level = "+this.level+ "\n"+
                "rssi = "+this.rssi+ "\n";


return  s;


    }

    String netWork;

    String servingCell;

    public String getNetWork() {
        return netWork;
    }

    public void setNetWork(String netWork) {
        this.netWork = netWork;
    }

    public String getServingCell() {
        return servingCell;
    }

    public void setServingCell(String servingCell) {
        this.servingCell = servingCell;
    }
}
