package com.molamobile.mobileshark.models;

/*Created By Ajish Dharman on 04-May-2020
 *
 *
 */
//3G
public class WCdmaCell {

    //WdcmaCellIdentity

    int cid; //CID 28-bit UMTS Cell Identity described in TS 25.331, 0..268435455, UNAVAILABLE if unavailable.

    int lac; //16-bit Location Area Code, 0..65535, UNAVAILABLE if unavailable.

    int mcc; //3-digit Mobile Country Code, 0..999, UNAVAILABLE if unavailable.

    int mnc; //	2 or 3-digit Mobile Network Code, 0..999, UNAVAILABLE if unavailable.

    String mccString; //Mobile Country Code in string version, null if unavailable.

    String mnsString; //Mobile Network Code in string version, null if unavailable.

    int psc;  //9-bit UMTS Primary Scrambling Code described in TS 25.331, 0..511, UNAVAILABLE if unavailable.

    int uarfcn; //16-bit UMTS Absolute RF Channel Number, UNAVAILABLE if unavailable. //Band Number

    String mobileNetworkOperator; //a 5 or 6 character string (MCC+MNC), null if any field is unknown

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public int getLac() {
        return lac;
    }

    public void setLac(int lac) {
        this.lac = lac;
    }

    public int getMcc() {
        return mcc;
    }

    public void setMcc(int mcc) {
        this.mcc = mcc;
    }

    public int getMnc() {
        return mnc;
    }

    public void setMnc(int mnc) {
        this.mnc = mnc;
    }

    public String getMccString() {
        return mccString;
    }

    public void setMccString(String mccString) {
        this.mccString = mccString;
    }

    public String getMnsString() {
        return mnsString;
    }

    public void setMnsString(String mnsString) {
        this.mnsString = mnsString;
    }

    public int getPsc() {
        return psc;
    }

    public void setPsc(int psc) {
        this.psc = psc;
    }

    public int getUarfcn() {
        return uarfcn;
    }

    public void setUarfcn(int uarfcn) {
        this.uarfcn = uarfcn;
    }

    public String getMobileNetworkOperator() {
        return mobileNetworkOperator;
    }

    public void setMobileNetworkOperator(String mobileNetworkOperator) {
        if(mobileNetworkOperator==null){
            this.mobileNetworkOperator="";
        }
        else {
            this.mobileNetworkOperator = mobileNetworkOperator;
        }
    }


    //SignalStrength;

    int asu; //Get the RSCP in ASU. Asu is calculated based on 3GPP RSCP. Refer to 3GPP 27.007 (Ver 10.3.0) Sec 8.69
            //Received Signal code power

    int dbm; //Get the RSCP as dBm value -120..-24dBm or CellInfo#UNAVAILABLE.

    int  ecno;    //Get the Ec/No (Energy per chip over the noise spectral density) as dB. Reference: TS 25.133 Section 9.1.2.3


    int level; //Retrieve an abstract level value for the overall signal quality.
                //Value is between SIGNAL_STRENGTH_NONE_OR_UNKNOWN and SIGNAL_STRENGTH_GREAT inclusive


    public int getAsu() {
        return asu;
    }

    public void setAsu(int asu) {
        this.asu = asu;
    }

    public int getDbm() {
        return dbm;
    }

    public void setDbm(int dbm) {
        this.dbm = dbm;
    }

    public int getEcno() {
        return ecno;
    }

    public void setEcno(int ecno) {
        this.ecno = ecno;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }



    public String convertToString(){


        String s = "cid = " +this.cid + "\n" +

                "lac = " +this.lac + "\n" +
                "mcc = " +this.mcc + "\n" +
                "mnc = " +this.mnc + "\n" +
                "psc = " +this.psc + "\n" +
                "mobileNetworkOperator = " +this.mobileNetworkOperator + "\n" +
                "asu = " +this.asu + "\n" +
                "dbM = " +this.dbm + "\n" +
                "ecno = " +this.ecno + "\n" +
                "level = " +this.level + "\n" ;



        return s;
    }


    String netWork;
    String servingCell;

    public String getNetWork() {
        return netWork;
    }

    public void setNetWork(String netWork) {
        this.netWork = netWork;
    }

    public String getServingCell() {
        return servingCell;
    }

    public void setServingCell(String servingCell) {
        this.servingCell = servingCell;
    }
}
