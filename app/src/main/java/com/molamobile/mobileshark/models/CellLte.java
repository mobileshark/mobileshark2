package com.molamobile.mobileshark.models;

/*Created By Ajish Dharman on 05-May-2020
 *
 *4G LTE
 */
public class CellLte {

    public CellLte(){

        bandWidth =0;
    }
    //Cell Identity

    int[] bands; //Array of band number or empty array if not available. This value will never be null.
    //Get bands of the cell Reference: 3GPP TS 36.101 section 5.5


    int bandWidth; //Cell bandwidth in kHz, UNAVAILABLE if unavailable.
    int ci; //28-bit Cell Identity, UNAVAILABLE if unavailable.
    int earfcn; //18-bit Absolute RF Channel Number, UNAVAILABLE if unavailable.

    int mcc;   //3-digit Mobile Country Code, 0..999, UNAVAILABLE if unavailable.

    String mccString; //3-digit Mobile Country Code, 0..999, UNAVAILABLE if unavailable.

    int mnc; //2 or 3-digit Mobile Network Code, 0..999, UNAVAILABLE if unavailable.

    String mncString;// 2 or 3-digit Mobile Network Code, 0..999, UNAVAILABLE if unavailable.

    String mobileNetworkOperator;//a 5 or 6 character string (MCC+MNC), null if any field is unknown.

    int pci; //Physical Cell Id 0..503, UNAVAILABLE if unavailable.

    int tac; //16-bit Tracking Area Code, UNAVAILABLE if unavailable

    public int[] getBands() {
        return bands;
    }

    public void setBands(int[] bands) {
        this.bands = bands;
    }

    public int getBandWidth() {
        return bandWidth;
    }

    public void setBandWidth(int bandWidth) {
        this.bandWidth = bandWidth;
    }

    public int getCi() {
        return ci;
    }

    public void setCi(int ci) {
        this.ci = ci;
    }

    public int getEarfcn() {
        return earfcn;
    }

    public void setEarfcn(int earfcn) {
        this.earfcn = earfcn;
    }

    public int getMcc() {
        return mcc;
    }

    public void setMcc(int mcc) {
        this.mcc = mcc;
    }

    public String getMccString() {
        return mccString;
    }

    public void setMccString(String mccString) {
        this.mccString = mccString;
    }

    public int getMnc() {
        return mnc;
    }

    public void setMnc(int mnc) {
        this.mnc = mnc;
    }

    public String getMncString() {
        return mncString;
    }

    public void setMncString(String mncString) {
        this.mncString = mncString;
    }

    public String getMobileNetworkOperator() {
        return mobileNetworkOperator;
    }

    public void setMobileNetworkOperator(String mobileNetworkOperator) {
        if(mobileNetworkOperator==null){
            this.mobileNetworkOperator="";
        }
        else {
            this.mobileNetworkOperator = mobileNetworkOperator;
        }
    }

    public int getPci() {
        return pci;
    }

    public void setPci(int pci) {
        this.pci = pci;
    }

    public int getTac() {
        return tac;
    }

    public void setTac(int tac) {
        this.tac = tac;
    }


    //Cell signal Strength;

    int asu;
    // Get the RSRP in ASU. Asu is calculated based on 3GPP RSRP. Refer to 3GPP 27.007 (Ver 10.3.0) Sec 8.69            v
    // Reference Signals Received Power (RSRP) and Reference Signal Received Quality (RSRQ) are key measures of signal level and quality for modern LTE networks. In cellular networks, when a mobile device moves from cell to cell and performs cell selection/reselection and handover, it has to measure the signal strength/quality of the neighbor cells.
    //  In the procedure of handover, the LTE specification provides the flexibility of using RSRP, RSRQ, or both.

    int cqi; // Get channel quality indicator

    // CQI stands for Channel Quality Indicator. As the name implies, it is an indicator carrying the information on
    // how good/bad the communication channel quality is. This CQI is for HSDPA. (LTE also has CQI for its own purpose).

    int dbm; //Get signal strength in dBm

    int level; //Retrieve an abstract level value for the overall signal quality.
    ///Value is between SIGNAL_STRENGTH_NONE_OR_UNKNOWN and SIGNAL_STRENGTH_GREAT inclusive

    int rsrp;
    //Get reference signal received power in dBm

    int rsrqp; //Get reference signal received quality;

    int rssi; //Get Received Signal Strength Indication (RSSI) in dBm The value range
    // is [-113, -51] inclusively or CellInfo#UNAVAILABLE if unavailable. Reference: TS 27.007 8.5 Signal quality +CSQ

    int rssnr; //Get reference signal signal-to-noise ratio

    int timingAdvance;

    //Get the timing advance value for LTE, as a value in range of 0..1282.
    // UNAVAILABLE is reported when there is no active RRC connection. Refer to 3GPP 36.213 Sec 4.2.3


    public int getAsu() {
        return asu;
    }

    public void setAsu(int asu) {
        this.asu = asu;
    }

    public int getCqi() {
        return cqi;
    }

    public void setCqi(int cqi) {
        this.cqi = cqi;
    }

    public int getDbm() {
        return dbm;
    }

    public void setDbm(int dbm) {
        this.dbm = dbm;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getRsrp() {
        return rsrp;
    }

    public void setRsrp(int rsrp) {
        this.rsrp = rsrp;
    }

    public int getRsrqp() {
        return rsrqp;
    }

    public void setRsrqp(int rsrqp) {
        this.rsrqp = rsrqp;
    }

    public int getRssi() {
        return rssi;
    }

    public void setRssi(int rssi) {
        this.rssi = rssi;
    }

    public int getRssnr() {
        return rssnr;
    }

    public void setRssnr(int rssnr) {
        this.rssnr = rssnr;
    }

    public int getTimingAdvance() {
        return timingAdvance;
    }

    public void setTimingAdvance(int timingAdvance) {
        this.timingAdvance = timingAdvance;
    }


    public String convertToString(){

       String s = "BandWidth = " +this.bandWidth + "\n" +
                  "ci = " +this.ci+"\n" +
               "earfcn = " +this.earfcn+"\n" +
               "mcc = " +this.mcc+"\n" +
               "mnc = " +this.mnc+"\n" +
               "pci = " +this.pci+"\n" +
               "tac = " +this.tac+"\n" +

               "asu = " +this.asu+"\n" +
               "cqi = " +this.cqi+"\n" +
               "dbm = " +this.dbm+"\n" +
               "level = " +this.level+"\n" +
               "rsrp = " +this.rsrp+"\n" +
               "rsrqp = " +this.rsrqp+"\n" +
               "rssi = " +this.rssi+"\n" +
               "rssnr = " +this.rssnr+"\n" +
               "timingAdvance = " +this.timingAdvance+"\n" ;


               return s;

    }

    String netWork;
    String servingCell;

    public String getNetWork() {
        return netWork;
    }

    public void setNetWork(String netWork) {
        this.netWork = netWork;
    }

    public String getServingCell() {
        return servingCell;
    }

    public void setServingCell(String servingCell) {
        this.servingCell = servingCell;
    }
}
