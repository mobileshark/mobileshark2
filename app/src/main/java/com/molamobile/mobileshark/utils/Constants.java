package com.molamobile.mobileshark.utils;

/*Created By Ajish Dharman on 06-August-2020
 *
 *
 */

public class Constants {

  public static final String INTENT_LOCATION_ACTION =  "com.molamobile.mobileshark.LOCATION_CHANGED";
  public static final String INTENT_LOCATION_ACTION_MSG =  "com.molamobile.mobileshark.LOC_MSG";
}
