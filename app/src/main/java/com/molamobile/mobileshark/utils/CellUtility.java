package com.molamobile.mobileshark.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Build;
import android.telephony.SubscriptionManager;
import android.telephony.TelephonyManager;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class CellUtility {

    /**
     * Get a human-readable string of RAT/Network Type
     *
     * Frustratingly it looks like the app uses RAT & Network Type interchangably with both either
     * being an integer representation (TelephonyManager's constants) or a human-readable string.
     *
     * @param netType The integer representation of the network type, via TelephonyManager
     * @return Human-readable representation of network type (e.g. "EDGE", "LTE")
     */
    public static String getRatFromInt(int netType) {
        switch (netType) {
            case TelephonyManager.NETWORK_TYPE_1xRTT:
                return "1xRTT";
            case TelephonyManager.NETWORK_TYPE_CDMA:
                return "CDMA";
            case TelephonyManager.NETWORK_TYPE_EDGE:
                return "EDGE";
            case TelephonyManager.NETWORK_TYPE_EHRPD:
                return "eHRPD";
            case TelephonyManager.NETWORK_TYPE_EVDO_0:
                return "EVDO rev. 0";
            case TelephonyManager.NETWORK_TYPE_EVDO_A:
                return "EVDO rev. A";
            case TelephonyManager.NETWORK_TYPE_EVDO_B:
                return "EVDO rev. B";
            case TelephonyManager.NETWORK_TYPE_GPRS:
                return "GPRS";
            case TelephonyManager.NETWORK_TYPE_HSDPA:
                return "HSDPA";
            case TelephonyManager.NETWORK_TYPE_HSPA:
                return "HSPA";
            case TelephonyManager.NETWORK_TYPE_HSPAP:
                return "HSPA+";
            case TelephonyManager.NETWORK_TYPE_HSUPA:
                return "HSUPA";
            case TelephonyManager.NETWORK_TYPE_IDEN:
                return "iDen";
            case TelephonyManager.NETWORK_TYPE_LTE:
                return "LTE";
            case TelephonyManager.NETWORK_TYPE_UMTS:
                return "UMTS";
            case TelephonyManager.NETWORK_TYPE_UNKNOWN:
                return "Unknown";

            case   TelephonyManager.NETWORK_TYPE_NR:
                return "5G";
            default:
                return String.valueOf(netType);
        }
    }

    public static String simState(int simState) {
        switch (simState) {
            case 0:
                return "Unknown";
            case 1:
                return "Absent";
            case 2:
                return "Required";
            case 3:
                return "PUK Required";
            case 4:
                return "Network Locked";
            case 5:
                return "Ready";
            case 6:
                return "Not Ready";
            case 7:
                return "PERM Disabled";
            case 8:
                return "CARD_IO_ERR";
        }
        return "??? " + simState;
    }

    public static int getSIMStateBySlot(Context context, String predictedMethodName, int slotID) throws Exception {
        boolean isReady = false;
        int simState=0;
        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class<?> telephonyClass = Class.forName(telephony.getClass().getName());
            Class<?>[] parameter = new Class[1];
            parameter[0] = int.class;
            Method getSimState = telephonyClass.getMethod(predictedMethodName, parameter);
            Object[] obParameter = new Object[1];
            obParameter[0] = slotID;
            Object ob_phone = getSimState.invoke(telephony, obParameter);

            if (ob_phone != null) {
                simState = Integer.parseInt(ob_phone.toString());

                // if ((simState != TelephonyManager.SIM_STATE_ABSENT) && (simState != TelephonyManager.SIM_STATE_UNKNOWN)) {
                //  isReady = true;
                // }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(predictedMethodName);
        }


        return simState;
    }


    public static int getNetWork(Context context, String predictedMethodName, int slotID) throws Exception {
        boolean isReady = false;
        int nwt=0;
        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class<?> telephonyClass = Class.forName(telephony.getClass().getName());
            Class<?>[] parameter = new Class[1];
            parameter[0] = int.class;
            Method getSimState = telephonyClass.getMethod(predictedMethodName, parameter);
            Object[] obParameter = new Object[1];
            obParameter[0] = slotID;
            Object ob_phone = getSimState.invoke(telephony, obParameter);

            if (ob_phone != null) {
                nwt = Integer.parseInt(ob_phone.toString());

                // if ((simState != TelephonyManager.SIM_STATE_ABSENT) && (simState != TelephonyManager.SIM_STATE_UNKNOWN)) {
                //  isReady = true;
                // }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(predictedMethodName);
        }

        return nwt;
    }


    public static int getDataNetwork(Context context, String predictedMethodName, int slotID) throws Exception {
        boolean isReady = false;
        int nwt=0;
        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class<?> telephonyClass = Class.forName(telephony.getClass().getName());
            Class<?>[] parameter = new Class[1];
            parameter[0] = int.class;
            Method getSimState = telephonyClass.getMethod(predictedMethodName, parameter);
            Object[] obParameter = new Object[1];
            obParameter[0] = slotID;
            Object ob_phone = getSimState.invoke(telephony, obParameter);

            if (ob_phone != null) {
                nwt = Integer.parseInt(ob_phone.toString());

                // if ((simState != TelephonyManager.SIM_STATE_ABSENT) && (simState != TelephonyManager.SIM_STATE_UNKNOWN)) {
                //  isReady = true;
                // }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(predictedMethodName);
        }

        return nwt;
    }


    public static int getNetWork29(Context context ,int subid) throws Exception {
        String method="getDataNetworkTypeForSubscriber";
        boolean isReady = false;
        int nwt=0;

        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class<?> telephonyClass = Class.forName(telephony.getClass().getName());
            Class<?>[] parameter = new Class[3];
            parameter[0] = int.class;
            parameter[1] = String.class;
            parameter[2] = String.class;

            Method getSimState = telephonyClass.getMethod(method, parameter);
            Object[] obParameter = new Object[3];
            obParameter[0] = subid;
            obParameter[1] = getOpPackageName(context);
            obParameter[2] =getFeatureId(context);
            Object ob_phone = getSimState.invoke(telephony, obParameter);

            if (ob_phone != null) {
                nwt = Integer.parseInt(ob_phone.toString());

                // if ((simState != TelephonyManager.SIM_STATE_ABSENT) && (simState != TelephonyManager.SIM_STATE_UNKNOWN)) {
                //  isReady = true;
                // }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(method);
        }

        return nwt;
    }



    public static int getDataState(Context context, String predictedMethodName, int slotID) throws Exception {
        boolean isReady = false;
        int nwt=0;
        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class<?> telephonyClass = Class.forName(telephony.getClass().getName());
            Class<?>[] parameter = new Class[1];
            parameter[0] = int.class;
            Method getSimState = telephonyClass.getMethod(predictedMethodName, parameter);
            Object[] obParameter = new Object[1];
            obParameter[0] = slotID;
            Object ob_phone = getSimState.invoke(telephony, obParameter);

            if (ob_phone != null) {
                nwt = Integer.parseInt(ob_phone.toString());

                // if ((simState != TelephonyManager.SIM_STATE_ABSENT) && (simState != TelephonyManager.SIM_STATE_UNKNOWN)) {
                //  isReady = true;
                // }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(predictedMethodName);
        }

        return nwt;
    }

    public static String getDataStateName(int state){

        String s ="";
        switch (state) {
            case 0:
                s="Disconnected";
                break;
            case 1:
                s="Connecting";
                break;
            case 2:
                s="Connected";
                break;
            default:
                s="UnKnown";
                break;

        }

        return s;
    }

    public static int getNetworkOperator(Context context, String predictedMethodName, int slotID) throws Exception {
        boolean isReady = false;
        int nwt=0;
        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class<?> telephonyClass = Class.forName(telephony.getClass().getName());
            Class<?>[] parameter = new Class[1];
            parameter[0] = int.class;
            Method getSimState = telephonyClass.getMethod(predictedMethodName, parameter);
            Object[] obParameter = new Object[1];
            obParameter[0] = slotID;
            Object ob_phone = getSimState.invoke(telephony, obParameter);

            if (ob_phone != null) {
                nwt = Integer.parseInt(ob_phone.toString());

                // if ((simState != TelephonyManager.SIM_STATE_ABSENT) && (simState != TelephonyManager.SIM_STATE_UNKNOWN)) {
                //  isReady = true;
                // }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(predictedMethodName);
        }

        return nwt;
    }


    public static String getNetworkOperatorName(Context context, String predictedMethodName, int slotID) throws Exception {

        boolean isReady = false;
        String nwt="";
        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class<?> telephonyClass = Class.forName(telephony.getClass().getName());
            Class<?>[] parameter = new Class[1];
            parameter[0] = int.class;
            Method getNetworkOperatorName = telephonyClass.getMethod(predictedMethodName, parameter);
            Object[] obParameter = new Object[1];
            obParameter[0] = slotID;
            Object ob_phone = getNetworkOperatorName.invoke(telephony, obParameter);

            if (ob_phone != null) {
                nwt= ob_phone.toString();

                // if ((simState != TelephonyManager.SIM_STATE_ABSENT) && (simState != TelephonyManager.SIM_STATE_UNKNOWN)) {
                //  isReady = true;
                // }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(predictedMethodName);
        }

        return nwt;
    }


    //  getOpPackageName


    public static String getOpPackageName(Context context) throws Exception {
        boolean isReady = false;
        String nwt="";
        String predictedMethodName="getOpPackageName";
        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class<?> telephonyClass = Class.forName(telephony.getClass().getName());
            Class<?>[] parameter = new Class[1];
            parameter[0] = int.class;
            Method getNetworkOperatorName = telephonyClass.getMethod(predictedMethodName, parameter);
            //  Object[] obParameter = new Object[1];
            //  obParameter[0] = slotID;
            Object ob_phone = getNetworkOperatorName.invoke(telephony);

            if (ob_phone != null) {
                nwt= ob_phone.toString();

                // if ((simState != TelephonyManager.SIM_STATE_ABSENT) && (simState != TelephonyManager.SIM_STATE_UNKNOWN)) {
                //  isReady = true;
                // }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(predictedMethodName);
        }

        return nwt;
    }


    public static String getFeatureId(Context context) throws Exception {
        boolean isReady = false;
        String nwt="";
        String predictedMethodName="getOpPackageName";
        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class<?> telephonyClass = Class.forName(telephony.getClass().getName());
            Class<?>[] parameter = new Class[1];
            parameter[0] = int.class;
            Method getNetworkOperatorName = telephonyClass.getMethod(predictedMethodName, parameter);
            //  Object[] obParameter = new Object[1];
            //  obParameter[0] = slotID;
            Object ob_phone = getNetworkOperatorName.invoke(telephony);

            if (ob_phone != null) {
                nwt= ob_phone.toString();

                // if ((simState != TelephonyManager.SIM_STATE_ABSENT) && (simState != TelephonyManager.SIM_STATE_UNKNOWN)) {
                //  isReady = true;
                // }
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(predictedMethodName);
        }

        return nwt;
    }

    public static boolean isNetworkRoaming(Context context, String predictedMethodName, int slotID) throws Exception {
        boolean roaming = false;

        TelephonyManager telephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        try {
            Class<?> telephonyClass = Class.forName(telephony.getClass().getName());
            Class<?>[] parameter = new Class[1];
            parameter[0] = int.class;
            Method getSimState = telephonyClass.getMethod(predictedMethodName, parameter);
            Object[] obParameter = new Object[1];
            obParameter[0] = slotID;
            Object ob_phone = getSimState.invoke(telephony, obParameter);

            if (ob_phone != null) {
                //  nwt= ob_phone.toString();

                // if ((simState != TelephonyManager.SIM_STATE_ABSENT) && (simState != TelephonyManager.SIM_STATE_UNKNOWN)) {
                //  isReady = true;
                // }
                roaming = (boolean)ob_phone;
            }
        } catch (Exception e) {
            e.printStackTrace();
            throw new Exception(predictedMethodName);
        }

        return roaming;
    }


    public  static int  getDefault(Context thisActivity){

        SubscriptionManager subscriptionManager =
                (SubscriptionManager) thisActivity.getApplicationContext().getSystemService(Context.TELEPHONY_SUBSCRIPTION_SERVICE);

        try  {
            Class<?> subscriptionClass = Class.forName(subscriptionManager.getClass().getName());
            try {
                Method getDefaultDataSubscriptionId = subscriptionClass.getMethod("getDefaultDataSubscriptionId");

                try {
                    return ((int) getDefaultDataSubscriptionId.invoke(subscriptionManager));
                }
                catch (IllegalAccessException e1) {
                    e1.printStackTrace();
                } catch (InvocationTargetException e1) {
                    e1.printStackTrace();
                }
            } catch (NoSuchMethodException e1) {
                e1.printStackTrace();
            }
        } catch (ClassNotFoundException e1) {
            e1.printStackTrace();
        }

        return -1;
    }

    public static int getBatteryPercentage(Context context) {

        if (Build.VERSION.SDK_INT >= 21) {

            BatteryManager bm = (BatteryManager) context.getSystemService(Context.BATTERY_SERVICE);
            int batl = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
            return batl;

        } else {

            IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            Intent batteryStatus = context.registerReceiver(null, iFilter);

            int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
            int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1) : -1;

            double batteryPct = level / (double) scale;

            return (int) (batteryPct * 100);
        }
    }

    public static boolean isWifi(Context ctx) {

        boolean isWiFi = false;
        boolean isConnected = false;
        boolean isMobile = false;
        boolean result = false;

        if (Build.VERSION.SDK_INT >= 29) {

            ConnectivityManager connectivityManager = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
            Network activeNetwork = connectivityManager.getActiveNetwork();
            NetworkCapabilities caps = connectivityManager.getNetworkCapabilities(activeNetwork);
            boolean wifi = caps.hasTransport(NetworkCapabilities.TRANSPORT_WIFI);
            if (wifi) {
                return true;
            } else {
                return false;
            }
        } else {
            ConnectivityManager cm =
                    (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

            // True if network is available.
            if (activeNetwork != null) {

                // True if using WiFI
                isWiFi = activeNetwork.getType() == ConnectivityManager.TYPE_WIFI;

                // True if using Mobile Data
                isMobile = activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE;

                isConnected = activeNetwork.isConnectedOrConnecting();
            }

            if (isConnected) {
                if (isWiFi) {

                    result = true;
                }


                if (isMobile) {

                    result = false;
                }
            } else {

                result = false;

            }

            return result;
        }
    }


}
