package com.molamobile.mobileshark.utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;

import com.google.android.gms.maps.model.Marker;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

public  class CommonUtility {

    public static int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;

    public  static boolean checkAndRequestPermissionsForMesure(Activity thisCtx) {
        int phone = ContextCompat.checkSelfPermission(thisCtx, Manifest.permission.READ_PHONE_STATE);
        int storage = ContextCompat.checkSelfPermission(thisCtx, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int loc = ContextCompat.checkSelfPermission(thisCtx, Manifest.permission.ACCESS_COARSE_LOCATION);
        int loc2 = ContextCompat.checkSelfPermission(thisCtx, Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();

        if (phone != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
        }
        if (loc2 != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
        }
        if (loc != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
        }
        if (!listPermissionsNeeded.isEmpty())
        {
            ActivityCompat.requestPermissions(thisCtx,listPermissionsNeeded.toArray
                    (new String[listPermissionsNeeded.size()]),CommonUtility.REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    public static Marker prevMarker=null;

    public static boolean isLocationServiceStarted = false;

    public static String currentFragment="";
}
